const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateLinkInput(data) {
  const errors = {};

  data.title = !isEmpty(data.title) ? data.title : '';
  data.url = !isEmpty(data.url) ? data.url : '';
  data.tagId = !isEmpty(data.tagId) ? data.tagId : '';

  if (Validator.isEmpty(data.title)) {
    errors.title = 'Title field is required';
  }

  if (!Validator.isURL(data.url)) {
    errors.url = 'URL is invalid';
  }

  if (Validator.isEmpty(data.url)) {
    errors.url = 'URL field is required';
  }

  if (Validator.isEmpty(data.tagId)) {
    errors.tagId = 'Tags field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
