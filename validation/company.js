const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateCompanyInput(data) {
  const errors = {};

  data.companyName = !isEmpty(data.companyName) ? data.companyName : '';

  if (Validator.isEmpty(data.companyName)) {
    errors.companyName = 'Company Name field is required';
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
