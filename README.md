# CustMetrics - Discover Your Customers

## Screenshots

| <img src="./screenshots/print1.png" width="440" height="247"> | <img src="./screenshots/print2.png" width="440" height="247"> |
| :---: | :---: |
| **Login Page** | **Companies Page** |

| <img src="./screenshots/print3.png" width="440" height="247"> | <img src="./screenshots/print4.png" width="440" height="247"> |
| :---: | :---: |
| **Tags Page** | **Workspace Page** |

| <img src="./screenshots/print5.png" width="440" height="247"> | <img src="./screenshots/print6.png" width="440" height="247"> |
| :---: | :---: |
| **Notes Template Page** | **Value Proposition Page** |

| <img src="./screenshots/print7.png" width="440" height="247"> | <img src="./screenshots/print8.png" width="440" height="247"> |
| :---: | :---: |
| **Mind Map Page** | **Charts Page** |

## Demo

:globe_with_meridians: https://custmetrics.herokuapp.com/

**Credentials:**

- :email: demo@demo.com
- :key: demo123

## Quick Start

```bash
# Install dependencies for server
$ npm install

# Install dependencies for client
$ npm run client-install

# Run the client & server with concurrently
$ npm run dev

# Run the Express server only
$ npm run server

# Run the React client only
$ npm run client

# Server runs on http://localhost:5000 and client on http://localhost:3000
```

### Environment Variables

Add these environment variables in `.env` file:

```bash
export NODE_ENV=production
export MONGO_URI=mongodb://127.0.0.1:27017/custmetrics
export SECRET_OR_KEY=secret
export CUSTMETRICS_APP_URL=https://example.com
export CUSTMETRICS_EMAIL_HOST=smtp.mailgun.org
export CUSTMETRICS_EMAIL_PORT=587
export CUSTMETRICS_EMAIL_USER=user@example.com
export CUSTMETRICS_EMAIL_PASSWORD=12345
```

## Contributing

If you want to help, please read the [Contributing](./CONTRIBUTING.md) guide.

## License

[Apache License Version 2](./LICENSE) © [Spicule Ltd](https://spicule.co.uk/)
