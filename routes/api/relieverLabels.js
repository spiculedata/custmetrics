const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Reliever Label model
const RelieverLabel = require('../../models/RelieverLabel');

// Validation
const validateLabelInput = require('../../validation/label');

// @route   GET api/label/relievers/test
// @desc    Tests reliever label route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Reliever Labels Works!' }));

// @route   GET api/label/relievers
// @desc    Get reliever labels
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    RelieverLabel.find()
      .sort({ name: 1 })
      .then(relieverLabels => res.json(relieverLabels))
      .catch(err =>
        res
          .status(404)
          .json({ noRelieverLabelsFound: 'No reliever labels found!' })
      );
  }
);

// @route   GET api/label/relievers/:id
// @desc    Get reliever label by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    RelieverLabel.findById(id)
      .then(relieverLabel => {
        if (relieverLabel) {
          res.json(relieverLabel);
        } else {
          res
            .status(404)
            .json({
              noRelieverLabelFound: 'No reliever label found with that ID!'
            });
        }
      })
      .catch(err =>
        res
          .status(404)
          .json({
            noRelieverLabelFound: 'No reliever label found with that ID!'
          })
      );
  }
);

// @route   POST api/label/relievers
// @desc    Create reliever label
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateLabelInput(req.body);
    const { name, value, label } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newRelieverLabel = new RelieverLabel({
      name,
      value,
      label
    });

    newRelieverLabel.save().then(relieverLabel => res.json(relieverLabel));
  }
);

// @route   PUT api/label/relievers/:id
// @desc    Edit reliever label
// @access  Private
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;
    const { errors, isValid } = validateLabelInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    RelieverLabel.findByIdAndUpdate({ _id: new ObjectId(id) }, req.body, {
      new: true
    })
      .then(relieverLabel => res.json(relieverLabel))
      .catch(err =>
        res
          .status(404)
          .json({
            noRelieverLabelFound: 'No reliever label found with that ID!'
          })
      );
  }
);

// @route   DELETE api/label/relievers/:id
// @desc    Delete reliever label
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    RelieverLabel.findById(id)
      .then(relieverLabel => {
        // Delete
        relieverLabel.remove().then(() => res.json({ success: true }));
      })
      .catch(err =>
        res
          .status(404)
          .json({ noRelieverLabelFound: 'No reliever label found!' })
      );
  }
);

module.exports = router;
