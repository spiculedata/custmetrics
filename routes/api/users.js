const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const nodemailer = require('nodemailer');
const keys = require('../../config/keys');

// Load Input Validation
const validateLoginInput = require('../../validation/login');
const validateRegisterInput = require('../../validation/register');
const validateResetPasswordInput = require('../../validation/resetPassword');
const validateResetPassword2Input = require('../../validation/resetPassword2');

// Load User model
const User = require('../../models/User');
const ResetPassword = require('../../models/ResetPassword');

// @route   GET api/users/test
// @desc    Tests users route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Users works!' }));

// @route   POST api/users/register
// @desc    Register user
// @access  Public
router.post('/register', (req, res) => {
  const { errors, isValid } = validateRegisterInput(req.body);
  const { name, email, password } = req.body;

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email }).then(user => {
    if (user) {
      errors.email = 'Email already exists';

      return res.status(400).json(errors);
    } else {
      const avatar = gravatar.url(email, {
        s: '200', // Size
        r: 'pg', // Rating
        d: 'mm' // Default
      });

      const newUser = new User({
        name,
        email,
        avatar,
        password
      });

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
          if (err) {
            throw err;
          }

          newUser.password = hash;
          newUser
            .save()
            .then(user => res.json(user))
            .catch(err => console.log(err));
        });
      });
    }
  });
});

// @route   GET api/users/current
// @desc    Return current user
// @access  Private
router.get(
  '/current',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email
    });
  }
);

// @route   GET api/users/:id
// @desc    Check if user exists by id
// @access  Public
router.get('/:userId', (req, res) => {
  const { userId } = req.params;

  User.findById(userId)
    .then(user => {
      if (user) {
        res.json({ status: 'ok' });
      } else {
        res.status(404).json({ noUserFound: 'No user found with that ID' });
      }
    })
    .catch(err =>
      res.status(404).json({ noUserFound: 'No user found with that ID' })
    );
});

// @route   GET api/users/login
// @desc    Login User / Returning JWT Token
// @access  Public
router.post('/login', (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);
  const { email, password } = req.body;

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  // Find user by email
  User.findOne({ email }).then(user => {
    // Check for user
    if (!user) {
      errors.email = 'User not found';

      return res.status(404).json(errors);
    }

    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
      // User matched
      if (isMatch) {
        // Create JWT payload
        const payload = {
          id: user.id,
          name: user.name,
          email: user.email,
          avatar: user.avatar
        };

        // Sign Token
        jwt.sign(
          payload,
          keys.secretOrKey,
          { expiresIn: '7 days' }, // See: https://github.com/zeit/ms
          (err, token) => {
            res.json({
              success: true,
              token: `Bearer ${token}`
            });
          }
        );
      } else {
        errors.password = 'Password incorrect';

        return res.status(400).json(errors);
      }
    });
  });
});

// @route   GET api/users/reset_password/:token
// @desc    Check token for reset password
// @access  Public
router.get('/reset_password/:token', (req, res) => {
  const { token } = req.params;

  ResetPassword.findOne({ token }).then(token => {
    if (token) {
      const { email } = token;

      User.findOne({ email }).then(user => res.json(user));
    } else {
      res.status(404).json({ noTokenFound: 'Invalid Token!' });
    }
  })
  .catch(err =>
    res.status(404).json({ noTokenFound: 'Invalid Token!' })
  );
});

// @route   POST api/users/reset_password/:token
// @desc    Send email for reset password
// @access  Public
router.post('/reset_password/:token', (req, res) => {
  const { errors, isValid } = validateResetPasswordInput(req.body);
  const { token } = req.params;
  const { email } = req.body;

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  const newResetPassword = new ResetPassword({
    email,
    token
  });

  User.findOne({ email }).then(user => {
    if (user) {
      newResetPassword.save().then(data => {
        const output = `
          <h1>Hi ${user.name},</h1>
          <p>You recently requested to reset your password for your CustMetrics account. Use the link below to reset it.</p>
          <a href="${process.env.CUSTMETRICS_APP_URL}/reset_password/${token}" target="_blank">${process.env.CUSTMETRICS_APP_URL}/reset_password/${token}</a>
        `;

        const transporter = nodemailer.createTransport({
          host: process.env.CUSTMETRICS_EMAIL_HOST, // eg.: 'smtp.gmail.com'
          port: process.env.CUSTMETRICS_EMAIL_PORT, // eg.: 587
          auth: {
            user: process.env.CUSTMETRICS_EMAIL_USER,
            pass: process.env.CUSTMETRICS_EMAIL_PASSWORD
          },
          tls:{
            rejectUnauthorized:false
          }
        });

        const mailOptions = {
          from: process.env.CUSTMETRICS_EMAIL_USER,
          to: email,
          subject: 'Set up a new password for CustMetrics',
          html: output
        };

        // send mail with defined transport object
        transporter.sendMail(mailOptions, (error, info) => {
          if (error) {
            return console.log(error);
          }

          console.log('Message sent: %s', info.messageId);
          console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));

          res.render('contact', { msg: 'Email has been sent' });
        });

        res.json(data);
      });
    }
  });
});

// @route   PUT api/users/reset_password/:token
// @desc    Reset password
// @access  Public
router.put('/reset_password/:token', (req, res) => {
  const { token } = req.params;
  const userId = req.body._id;
  const { errors, isValid } = validateResetPassword2Input(req.body);

  // Check validation
  if (!isValid) {
    // If any errors, send 400 with errors object
    return res.status(400).json(errors);
  }

  User.findOne({ email: req.body.email }).then(user => {
    if (user) {
      delete req.body.id;

      bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(req.body.password, salt, (err, hash) => {
          if (err) {
            throw err;
          }

          req.body.password = hash;

          User.findByIdAndUpdate({ _id: new ObjectId(userId) }, req.body, {
            new: true
          })
            .then(user => {
              ResetPassword.findOne({ token }).then(token => {
                // Delete token
                token.remove().then(() => res.json({ success: true }));
              })
              .catch(err =>
                res.status(404).json({ noTokenFound: 'Invalid Token!' })
              );

              return res.json(user);
            })
            .catch(err =>
              res.status(404).json({ noUserFound: 'No user found with that ID!' })
            );
        });
      });
    }
  });
});

module.exports = router;
