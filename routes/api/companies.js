const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Company model
const Company = require('../../models/Company');

// Validation
const validateCompanyInput = require('../../validation/company');

// @route   GET api/companies/test
// @desc    Tests company route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Companies Works!' }));

// @route   GET api/companies
// @desc    Get companies
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Company.find()
      .sort({ companyName: 1 })
      .then(companies => res.json(companies))
      .catch(err =>
        res.status(404).json({ noCompaniesFound: 'No companies found!' })
      );
  }
);

// @route   GET api/companies/:id
// @desc    Get company by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Company.findById(id)
      .then(company => {
        if (company) {
          res.json(company);
        } else {
          res
            .status(404)
            .json({ noCompanyFound: 'No company found with that ID!' });
        }
      })
      .catch(err =>
        res
          .status(404)
          .json({ noCompanyFound: 'No company found with that ID!' })
      );
  }
);

// @route   POST api/companies
// @desc    Create company
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateCompanyInput(req.body);
    const { companyName, contactName, address, phone, email } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newCompany = new Company({
      companyName,
      contactName,
      address,
      phone,
      email
    });

    newCompany.save().then(company => res.json(company));
  }
);

// @route   PUT api/companies/:id
// @desc    Edit company
// @access  Private
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;
    const { errors, isValid } = validateCompanyInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    Company.findByIdAndUpdate({ _id: new ObjectId(id) }, req.body, {
      new: true
    })
      .then(company => res.json(company))
      .catch(err =>
        res
          .status(404)
          .json({ noCompanyFound: 'No company found with that ID!' })
      );
  }
);

// @route   DELETE api/companies/:id
// @desc    Delete company
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Company.findById(id)
      .then(company => {
        // Delete
        company.remove().then(() => res.json({ success: true }));
      })
      .catch(err =>
        res.status(404).json({ noCompanyFound: 'No company found!' })
      );
  }
);

module.exports = router;
