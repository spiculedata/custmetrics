const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Link model
const Link = require('../../models/Link');

// Validation
const validateLinkInput = require('../../validation/link');

// @route   GET api/links/test
// @desc    Tests links route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Links Works!' }));

// @route   GET api/links/company/:companyId
// @desc    Get links by company id
// @access  Private
router.get(
  '/company/:companyId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { companyId } = req.params;

    Link.find({ companyId })
      .sort({ title: 1 })
      .then(links => res.json(links))
      .catch(err => res.status(404).json({ noLinksFound: 'No links found!' }));
  }
);

// @route   GET api/links/:id
// @desc    Get link by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Link.findById(id)
      .then(link => {
        if (link) {
          res.json(link);
        } else {
          res.status(404).json({ noLinkFound: 'No link found with that ID!' });
        }
      })
      .catch(err =>
        res.status(404).json({ noLinkFound: 'No link found with that ID!' })
      );
  }
);

// @route   GET api/links/tags/:tagId
// @desc    Get links by tag id
// @access  Private
router.get(
  '/tags/:tagId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { tagId } = req.params;

    Link.find({ tagId })
      .then(links => {
        if (links) {
          res.json(links);
        } else {
          res.status(404).json({ noLinksFound: 'No links found with that Tag ID!' });
        }
      })
      .catch(err =>
        res.status(404).json({ noLinksFound: 'No links found with that Tag ID!' })
      );
  }
);

// @route   POST api/links
// @desc    Create link
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateLinkInput(req.body);
    const { companyId, tagId, title, url, type } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newLink = new Link({
      companyId,
      tagId,
      title,
      url,
      type
    });

    newLink.save().then(link => res.json(link));
  }
);

// @route   PUT api/links/:id
// @desc    Edit link
// @access  Private
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;
    const { errors, isValid } = validateLinkInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    Link.findByIdAndUpdate({ _id: new ObjectId(id) }, req.body, {
      new: true
    })
      .then(link => res.json(link))
      .catch(err =>
        res.status(404).json({ noLinkFound: 'No link found with that ID!' })
      );
  }
);

// @route   DELETE api/links/:id
// @desc    Delete link
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Link.findById(id)
      .then(link => {
        // Delete
        link.remove().then(() => res.json({ success: true }));
      })
      .catch(err => res.status(404).json({ noLinkFound: 'No link found!' }));
  }
);

module.exports = router;
