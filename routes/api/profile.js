const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const gravatar = require('gravatar');
const bcrypt = require('bcryptjs');
const isEmpty = require('../../validation/is-empty');

// Load Input Validation
const validateProfileInput = require('../../validation/profile');

// Load User model
const User = require('../../models/User');

// @route   GET api/profile/test
// @desc    Tests profile route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Profile works!' }));

// @route   PUT api/profile/:userId/:userEmail
// @desc    Update profile user
// @access  Public
router.put('/:userId/:userEmail', (req, res) => {
  const { userId, userEmail } = req.params;
  const { errors, isValid } = validateProfileInput(req.body);
  const { email, password } = req.body;

  // Check validation
  if (!isValid) {
    return res.status(400).json(errors);
  }

  User.findOne({ email }).then(user => {
    if (user && (user.email !== userEmail)) {
      errors.email = 'Email already exists';

      return res.status(400).json(errors);
    } else {
      const avatar = gravatar.url(email, {
        s: '200', // Size
        r: 'pg', // Rating
        d: 'mm' // Default
      });

      delete req.body.id;

      req.body.avatar = avatar;

      if (isEmpty(password)) {
        delete req.body.password;

        User.findByIdAndUpdate({ _id: new ObjectId(userId) }, req.body, {
          new: true
        })
          .then(user => res.json(user))
          .catch(err =>
            res.status(404).json({ noUserFound: 'No user found with that ID!' })
          );
      } else {
        bcrypt.genSalt(10, (err, salt) => {
          bcrypt.hash(req.body.password, salt, (err, hash) => {
            if (err) {
              throw err;
            }

            req.body.password = hash;

            User.findByIdAndUpdate({ _id: new ObjectId(userId) }, req.body, {
              new: true
            })
              .then(user => res.json(user))
              .catch(err =>
                res.status(404).json({ noUserFound: 'No user found with that ID!' })
              );
          });
        });
      }
    }
  });
});

module.exports = router;
