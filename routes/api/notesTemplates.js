const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Notes model
const Notes = require('../../models/Notes');

// Validation
const validateNotesInput = require('../../validation/notes');

// @route   GET api/notestemplates/test
// @desc    Tests notes templates route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Notes Templates Works!' }));

// @route   GET api/notestemplates
// @desc    Get notes templates
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Notes.find({ template: true })
      .sort({ title: 1 })
      .then(notesTemplates => res.json(notesTemplates))
      .catch(err => res.status(404).json({ noNotesTemplatesFound: 'No notes templates found!' }));
  }
);

// @route   GET api/notestemplates/:id
// @desc    Get notes template by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Notes.findById(id)
      .sort({ title: 1 })
      .then(notesTemplate => res.json(notesTemplate))
      .catch(err => res.status(404).json({ noNotesTemplateFound: 'No notes template found with that ID!' }));
  }
);

// @route   POST api/notestemplates
// @desc    Create notes template
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateNotesInput(req.body);
    const { notesId, title, template, treeData } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newNotesTemplate = new Notes({
      notesId,
      title,
      template,
      treeData
    });

    newNotesTemplate.save().then(notesTemplate => res.json(notesTemplate));
  }
);

// @route   PUT api/notestemplates/:notesId
// @desc    Edit notes template
// @access  Private
router.put(
  '/:notesId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { notesId } = req.params;
    const { errors, isValid } = validateNotesInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    Notes.findOneAndUpdate({ notesId }, req.body, {
      new: true
    })
      .then(notesTemplate => res.json(notesTemplate))
      .catch(err =>
        res.status(404).json({ noNotesTemplateFound: 'No notes template found with that Notes ID!' })
      );
  }
);

// @route   DELETE api/notestemplates/:id
// @desc    Delete notes template
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Notes.findById(id)
      .then(notesTemplate => {
        // Delete
        notesTemplate.remove().then(() => res.json({ success: true }));
      })
      .catch(err => res.status(404).json({ noNotesTemplateFound: 'No notes template found!' }));
  }
);

module.exports = router;
