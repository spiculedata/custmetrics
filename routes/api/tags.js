const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Tag model
const Tag = require('../../models/Tag');

// Validation
const validateTagInput = require('../../validation/tag');

// @route   GET api/tags/test
// @desc    Tests tag route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Tags Works!' }));

// @route   GET api/tags
// @desc    Get tags
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    Tag.find()
      .sort({ name: 1 })
      .then(tags => res.json(tags))
      .catch(err => res.status(404).json({ noTagsFound: 'No tags found!' }));
  }
);

// @route   GET api/tags/:id
// @desc    Get tag by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Tag.findById(id)
      .then(tag => {
        if (tag) {
          res.json(tag);
        } else {
          res.status(404).json({ noTagFound: 'No tag found with that ID!' });
        }
      })
      .catch(err =>
        res.status(404).json({ noTagFound: 'No tag found with that ID!' })
      );
  }
);

// @route   POST api/tags
// @desc    Create tag
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateTagInput(req.body);
    const { name, color } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newTag = new Tag({
      name,
      color
    });

    newTag.save().then(tag => res.json(tag));
  }
);

// @route   PUT api/tags/:id
// @desc    Edit tag
// @access  Private
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;
    const { errors, isValid } = validateTagInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    Tag.findByIdAndUpdate({ _id: new ObjectId(id) }, req.body, {
      new: true
    })
      .then(tag => res.json(tag))
      .catch(err =>
        res.status(404).json({ noTagFound: 'No tag found with that ID!' })
      );
  }
);

// @route   DELETE api/tags/:id
// @desc    Delete tag
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Tag.findById(id)
      .then(tag => {
        // Delete
        tag.remove().then(() => res.json({ success: true }));
      })
      .catch(err => res.status(404).json({ noTagFound: 'No tag found!' }));
  }
);

module.exports = router;
