const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Notes model
const Notes = require('../../models/Notes');

// Validation
const validateNotesInput = require('../../validation/notes');

// @route   GET api/notes/test
// @desc    Tests notes route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Notes Works!' }));

// @route   GET api/notes/company/:companyId
// @desc    Get notes by company id
// @access  Private
router.get(
  '/company/:companyId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { companyId } = req.params;

    Notes.find({ companyId })
      .sort({ title: 1 })
      .then(notes => res.json(notes))
      .catch(err => res.status(404).json({ noNotesFound: 'No notes found!' }));
  }
);

// @route   GET api/notes/:id
// @desc    Get notes by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Notes.findById(id)
      .then(notes => {
        if (notes) {
          res.json(notes);
        } else {
          res.status(404).json({ noNotesFound: 'No notes found with that ID!' });
        }
      })
      .catch(err =>
        res.status(404).json({ noNotesFound: 'No notes found with that ID!' })
      );
  }
);

// @route   GET api/notes/tags/:tagId
// @desc    Get notes by tag id
// @access  Private
router.get(
  '/tags/:tagId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { tagId } = req.params;

    Notes.find({ tagId })
      .then(notes => {
        if (notes) {
          res.json(notes);
        } else {
          res.status(404).json({ noNotesFound: 'No notes found with that Tag ID!' });
        }
      })
      .catch(err =>
        res.status(404).json({ noNotesFound: 'No notes found with that Tag ID!' })
      );
  }
);

// @route   POST api/notes
// @desc    Create notes
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateNotesInput(req.body);
    const {
      companyId,
      notesId,
      tagId,
      title,
      template,
      type,
      treeData
    } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newNotes = new Notes({
      companyId,
      notesId,
      tagId,
      title,
      template,
      type,
      treeData
    });

    newNotes.save().then(notes => res.json(notes));
  }
);

// @route   PUT api/notes/:notesId
// @desc    Edit notes
// @access  Private
router.put(
  '/:notesId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { notesId } = req.params;
    const { errors, isValid } = validateNotesInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    Notes.findOneAndUpdate({ notesId }, req.body, {
      new: true
    })
      .then(notes => res.json(notes))
      .catch(err =>
        res.status(404).json({ noNotesFound: 'No notes found with that ID!' })
      );
  }
);

// @route   DELETE api/notes/:id
// @desc    Delete notes
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    Notes.findById(id)
      .then(notes => {
        // Delete
        notes.remove().then(() => res.json({ success: true }));
      })
      .catch(err => res.status(404).json({ noNotesFound: 'No notes found!' }));
  }
);

module.exports = router;
