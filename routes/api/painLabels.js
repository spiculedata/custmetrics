const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Pain Label model
const PainLabel = require('../../models/PainLabel');

// Validation
const validateLabelInput = require('../../validation/label');

// @route   GET api/label/pains/test
// @desc    Tests pain label route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Pain Labels Works!' }));

// @route   GET api/label/pains
// @desc    Get pain labels
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    PainLabel.find()
      .sort({ name: 1 })
      .then(painLabels => res.json(painLabels))
      .catch(err =>
        res.status(404).json({ noPainLabelsFound: 'No pain labels found!' })
      );
  }
);

// @route   GET api/label/pains/:id
// @desc    Get pain label by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    PainLabel.findById(id)
      .then(painLabel => {
        if (painLabel) {
          res.json(painLabel);
        } else {
          res
            .status(404)
            .json({ noPainLabelFound: 'No pain label found with that ID!' });
        }
      })
      .catch(err =>
        res
          .status(404)
          .json({ noPainLabelFound: 'No pain label found with that ID!' })
      );
  }
);

// @route   POST api/label/pains
// @desc    Create pain label
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateLabelInput(req.body);
    const { name, value, label } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newPainLabel = new PainLabel({
      name,
      value,
      label
    });

    newPainLabel.save().then(painLabel => res.json(painLabel));
  }
);

// @route   PUT api/label/pains/:id
// @desc    Edit pain label
// @access  Private
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;
    const { errors, isValid } = validateLabelInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    PainLabel.findByIdAndUpdate({ _id: new ObjectId(id) }, req.body, {
      new: true
    })
      .then(painLabel => res.json(painLabel))
      .catch(err =>
        res
          .status(404)
          .json({ noPainLabelFound: 'No pain label found with that ID!' })
      );
  }
);

// @route   DELETE api/label/pains/:id
// @desc    Delete pain label
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    PainLabel.findById(id)
      .then(painLabel => {
        // Delete
        painLabel.remove().then(() => res.json({ success: true }));
      })
      .catch(err =>
        res.status(404).json({ noPainLabelFound: 'No pain label found!' })
      );
  }
);

module.exports = router;
