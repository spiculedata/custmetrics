const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Value Proposition model
const ValueProposition = require('../../models/ValueProposition');

// Validation
const validateValuePropositionInput = require('../../validation/valueProposition');

// @route   GET api/valuepropositions/test
// @desc    Tests value propositions route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Links Works!' }));

// @route   GET api/valuepropositions/company/:companyId
// @desc    Get value propositions by company id
// @access  Private
router.get(
  '/company/:companyId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { companyId } = req.params;

    ValueProposition.find({ companyId })
      .sort({ title: 1 })
      .then(valuePropositions => res.json(valuePropositions))
      .catch(err => res.status(404).json({ noValuePropositionsFound: 'No value propositions found!' }));
  }
);

// @route   GET api/valuepropositions/:id
// @desc    Get value proposition by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    ValueProposition.findById(id)
      .then(valueProposition => {
        if (valueProposition) {
          res.json(valueProposition);
        } else {
          res.status(404).json({ noValuePropositionFound: 'No value proposition found with that ID!' });
        }
      })
      .catch(err =>
        res.status(404).json({ noValuePropositionFound: 'No value proposition found with that ID!' })
      );
  }
);

// @route   GET api/valuepropositions/tags/:tagId
// @desc    Get value propositions by tag id
// @access  Private
router.get(
  '/tags/:tagId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { tagId } = req.params;

    ValueProposition.find({ tagId })
      .then(valuePropositions => {
        if (valuePropositions) {
          res.json(valuePropositions);
        } else {
          res.status(404).json({ noValuePropositionsFound: 'No value propositions found with that Tag ID!' });
        }
      })
      .catch(err =>
        res.status(404).json({ noValuePropositionsFound: 'No value propositions found with that Tag ID!' })
      );
  }
);

// @route   POST api/valuepropositions
// @desc    Create value proposition
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateValuePropositionInput(req.body);
    const {
      companyId,
      valuePropositionId,
      tagId,
      title,
      type,
      wantsItems,
      wantsList,
      wantsMindmap,
      wantsChart,
      needsItems,
      needsList,
      needsMindmap,
      needsChart
    } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newValueProposition = new ValueProposition({
      companyId,
      valuePropositionId,
      tagId,
      title,
      type,
      wantsItems,
      wantsList,
      wantsMindmap,
      wantsChart,
      needsItems,
      needsList,
      needsMindmap,
      needsChart
    });

    newValueProposition.save().then(valueProposition => res.json(valueProposition));
  }
);

// @route   PUT api/valuepropositions/:valuePropositionId
// @desc    Edit value proposition
// @access  Private
router.put(
  '/:valuePropositionId',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { valuePropositionId } = req.params;
    const { errors, isValid } = validateValuePropositionInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    ValueProposition.findOneAndUpdate({ valuePropositionId }, req.body, {
      new: true
    })
      .then(valueProposition => res.json(valueProposition))
      .catch(err =>
        res.status(404).json({ noValuePropositionFound: 'No value proposition found with that ID!' })
      );
  }
);

// @route   DELETE api/valuepropositions/:id
// @desc    Delete value proposition
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    ValueProposition.findById(id)
      .then(valueProposition => {
        // Delete
        valueProposition.remove().then(() => res.json({ success: true }));
      })
      .catch(err => res.status(404).json({ noValuePropositionFound: 'No value proposition found!' }));
  }
);

module.exports = router;
