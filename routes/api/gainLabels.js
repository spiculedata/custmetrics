const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const ObjectId = mongoose.mongo.ObjectId;
const passport = require('passport');

// Gain Label model
const GainLabel = require('../../models/GainLabel');

// Validation
const validateLabelInput = require('../../validation/label');

// @route   GET api/label/gains/test
// @desc    Tests gain label route
// @access  Public
router.get('/test', (req, res) => res.json({ msg: 'Gain Labels Works!' }));

// @route   GET api/label/gains
// @desc    Get gain labels
// @access  Private
router.get(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    GainLabel.find()
      .sort({ name: 1 })
      .then(gainLabels => res.json(gainLabels))
      .catch(err =>
        res.status(404).json({ noGainLabelsFound: 'No gain labels found!' })
      );
  }
);

// @route   GET api/label/gains/:id
// @desc    Get gain label by id
// @access  Private
router.get(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    GainLabel.findById(id)
      .then(gainLabel => {
        if (gainLabel) {
          res.json(gainLabel);
        } else {
          res
            .status(404)
            .json({ noGainLabelFound: 'No gain label found with that ID!' });
        }
      })
      .catch(err =>
        res
          .status(404)
          .json({ noGainLabelFound: 'No gain label found with that ID!' })
      );
  }
);

// @route   POST api/label/gains
// @desc    Create gain label
// @access  Private
router.post(
  '/',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { errors, isValid } = validateLabelInput(req.body);
    const { name, value, label } = req.body;

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    const newGainLabel = new GainLabel({
      name,
      value,
      label
    });

    newGainLabel.save().then(gainLabel => res.json(gainLabel));
  }
);

// @route   PUT api/label/gains/:id
// @desc    Edit gain label
// @access  Private
router.put(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;
    const { errors, isValid } = validateLabelInput(req.body);

    // Check validation
    if (!isValid) {
      // If any errors, send 400 with errors object
      return res.status(400).json(errors);
    }

    delete req.body.id;

    GainLabel.findByIdAndUpdate({ _id: new ObjectId(id) }, req.body, {
      new: true
    })
      .then(gainLabel => res.json(gainLabel))
      .catch(err =>
        res
          .status(404)
          .json({ noGainLabelFound: 'No gain label found with that ID!' })
      );
  }
);

// @route   DELETE api/label/gains/:id
// @desc    Delete gain label
// @access  Private
router.delete(
  '/:id',
  passport.authenticate('jwt', { session: false }),
  (req, res) => {
    const { id } = req.params;

    GainLabel.findById(id)
      .then(gainLabel => {
        // Delete
        gainLabel.remove().then(() => res.json({ success: true }));
      })
      .catch(err =>
        res.status(404).json({ noGainLabelFound: 'No gain label found!' })
      );
  }
);

module.exports = router;
