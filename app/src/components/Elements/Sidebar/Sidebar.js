import React, { Component } from 'react';
import { withRouter, matchPath } from 'react-router-dom';

import { routes } from '../../../routes';

import MenuSwitch from '../MenuSwitch';

import './Sidebar.css';

class Sidebar extends Component {
  showSidebar() {
    for (let route of routes) {
      if (matchPath(this.props.location.pathname, route)) {
        return route.show_sidebar;
      }
    }

    return false;
  }

  render() {
    return this.showSidebar() ? (
      <div className="sidebar">
        <MenuSwitch />
      </div>
    ) : (
      ''
    );
  }
}

export default withRouter(Sidebar);
