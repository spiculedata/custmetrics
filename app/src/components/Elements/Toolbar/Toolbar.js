import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink, withRouter, matchPath } from 'react-router-dom';
import uuid from 'uuid';
import _ from 'lodash';
import {
  Icon,
  Menu,
  MenuDivider,
  MenuItem,
  Popover,
  Position,
  Tooltip
} from '@blueprintjs/core';

import { actionCreators } from '../../../actions';

import { routes } from '../../../routes';

import Logo from '../../UI/Logo';

import './Toolbar.css';

class Toolbar extends Component {
  state = {
    companyId: ''
  };

  componentWillMount() {
    const { companyId } = this.props;

    this.setState({ companyId });
  }

  componentWillReceiveProps(nextProps) {
    const companyIdState = this.state.companyId;
    const companyIdNextProps = nextProps.companyId;

    if (!_.isEqual(companyIdState, companyIdNextProps)) {
      this.setState({ companyId: companyIdNextProps });
    }
  }

  showBtnToolbarAdd() {
    for (let route of routes) {
      if (matchPath(this.props.location.pathname, route)) {
        return route.show_btn_toolbar_add;
      }
    }

    return false;
  }

  showBtnToolbarBack() {
    for (let route of routes) {
      if (matchPath(this.props.location.pathname, route)) {
        return route.show_btn_toolbar_back;
      }
    }

    return false;
  }

  getWorkspaceTab() {
    for (let route of routes) {
      if (matchPath(this.props.location.pathname, route)) {
        return route.workspace_tab;
      }
    }

    return false;
  }

  handleAddMenuClick(event) {
    const textContent = event.currentTarget.innerText.trim();
    const { companyId } = this.state;
    const { push } = this.props.history;

    switch (textContent) {
      case 'Company':
        push('/company/new');
        break;
      case 'Value Proposition':
        push(`/company/${companyId}/valueproposition/${uuid.v4()}/new`);
        break;
      case 'Notes Tree':
        push(`/company/${companyId}/notes/${uuid.v4()}/new`);
        break;
      case 'Link':
        push(`/company/${companyId}/link/new`);
        break;
      default:
        return;
    }
  }

  onBackClick(event) {
    event.preventDefault();

    const { companyId } = this.state;

    this.props.clearValueProposition();

    this.props.history.push(
      companyId ? `/workspace/${companyId}` : '/companies'
    );
  }

  onLogoutClick(event) {
    event.preventDefault();

    this.props.logoutUser();
  }

  render() {
    // const { companyId } = this.state;
    const addMenu = (
      <Menu>
        <MenuDivider title="Create New" />
        <MenuItem
          icon="office"
          text="Company"
          onClick={this.handleAddMenuClick.bind(this)}
        />
        <MenuDivider />
        <MenuItem icon="add" text="Add new...">
          <MenuItem
            icon="timeline-line-chart"
            text="Value Proposition"
            onClick={this.handleAddMenuClick.bind(this)}
          />
          <MenuItem
            icon="comment"
            text="Notes Tree"
            onClick={this.handleAddMenuClick.bind(this)}
          />
          <MenuItem
            icon="link"
            text="Link"
            onClick={this.handleAddMenuClick.bind(this)}
          />
        </MenuItem>
      </Menu>
    );

    return (
      <div className="toolbar">
        <div className="toolbar-inner">
          <div className="logo">
            <Logo width={50} height={50} />
          </div>

          <ul className="top">
            {this.showBtnToolbarBack() ? (
              <li>
                <Tooltip content={<span>Back</span>} position={Position.RIGHT}>
                  {/*
                  <NavLink
                    // to={`/workspace/${companyId}${this.getWorkspaceTab()}`}
                    to={`/workspace/${companyId}`}
                  >
                    <i className="md-icon">reply</i>
                  </NavLink>
                  */}
                  <a href="#back" onClick={this.onBackClick.bind(this)}>
                    <i className="md-icon">reply</i>
                  </a>
                </Tooltip>
              </li>
            ) : (
              ''
            )}
            <li>
              <Tooltip content={<span>Home</span>} position={Position.RIGHT}>
                {/*
                <NavLink
                  to={companyId ? `/workspace/${companyId}` : '/companies'}
                >
                  <i className="md-icon">home</i>
                </NavLink>
                */}
                <a href="#home" onClick={this.onBackClick.bind(this)}>
                  <i className="md-icon">home</i>
                </a>
              </Tooltip>
            </li>
            {this.showBtnToolbarAdd() ? (
              <li>
                <Popover content={addMenu} position={Position.RIGHT_TOP}>
                  <Tooltip content={<span>New</span>} position={Position.RIGHT}>
                    <a
                      href="#add"
                      onClick={e => {
                        e.preventDefault();
                      }}
                    >
                      <i className="md-icon">add</i>
                    </a>
                  </Tooltip>
                </Popover>
              </li>
            ) : (
              ''
            )}
            <li>
              <Tooltip content={<span>Tags</span>} position={Position.RIGHT}>
                <NavLink to="/tags">
                  <i className="md-icon">local_offer</i>
                </NavLink>
              </Tooltip>
            </li>
            <li>
              <Tooltip
                content={<span>Value Proposition Labels</span>}
                position={Position.RIGHT}
              >
                <NavLink to="/labels">
                  <Icon icon="label" iconSize={18} title={false} />
                </NavLink>
              </Tooltip>
            </li>
            <li>
              <Tooltip
                content={<span>Notes Tree Templates</span>}
                position={Position.RIGHT}
              >
                <NavLink to="/notes_templates">
                  <Icon icon="comment" iconSize={18} title={false} />
                </NavLink>
              </Tooltip>
            </li>
          </ul>

          <ul className="bottom">
            <li>
              <Tooltip
                content={<span>Settings</span>}
                position={Position.RIGHT}
              >
                <NavLink to="/settings">
                  <i className="md-icon">settings</i>
                </NavLink>
              </Tooltip>
            </li>
            <li>
              <Tooltip content={<span>Logout</span>} position={Position.RIGHT}>
                <a href="#logout" onClick={this.onLogoutClick.bind(this)}>
                  <i className="md-icon">power_settings_new</i>
                </a>
              </Tooltip>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

Toolbar.propTypes = {
  companyId: PropTypes.string,
  logoutUser: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(actionCreators.logoutUser()),
  clearValueProposition: () => dispatch(actionCreators.clearValueProposition())
});

export default connect(
  null,
  mapDispatchToProps
)(withRouter(Toolbar));
