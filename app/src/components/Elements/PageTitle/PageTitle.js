import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter, matchPath } from 'react-router-dom';

import { routes } from '../../../routes';

import Menu from '../../UI/Menu';

import './PageTitle.css';

const stylePosition = {
  position: 'none',
  left: '0'
};

class PageTitle extends Component {
  findTitle() {
    for (let route of routes) {
      if (matchPath(this.props.location.pathname, route)) {
        return route.title;
      }
    }

    return null;
  }

  showSidebar() {
    for (let route of routes) {
      if (matchPath(this.props.location.pathname, route)) {
        return route.show_sidebar;
      }
    }

    return false;
  }

  render() {
    return (
      <div className="page-title">
        <h1 style={this.showSidebar() ? {} : stylePosition}>
          {this.findTitle()}
        </h1>
        <Menu />
      </div>
    );
  }
}

PageTitle.propTypes = {
  location: PropTypes.object.isRequired
};

export default withRouter(PageTitle);
