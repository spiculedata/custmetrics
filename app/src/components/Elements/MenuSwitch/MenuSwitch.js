import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import onClickOutside from 'react-onclickoutside';

import { actionCreators } from '../../../actions';

import Loading from '../../UI/Loading';

import './MenuSwitch.css';

class MenuSwitch extends Component {
  state = {
    companies: [],
    company: {},
    filter: ''
  };

  componentWillMount() {
    const { companies, company } = this.props;

    this.setState({ companies, company });
  }

  componentDidMount() {
    const { companyId } = this.props.match.params;

    this.props.getCompanies();
    this.props.getCompany(companyId);
  }

  componentWillReceiveProps(nextProps) {
    const companiesState = this.state.companies;
    const companiesNextProps = nextProps.companies;

    const companyState = this.state.company;
    const companyNextProps = nextProps.company;

    if (!_.isEqual(companiesState, companiesNextProps)) {
      this.setState({ companies: companiesNextProps });
    }

    if (!_.isEqual(companyState, companyNextProps)) {
      this.setState({ company: companyNextProps });
    }
  }

  handleClickOutside = event => {
    const hasClassName = document.body.classList.contains(
      'sidebar-title-content-open'
    );

    if (hasClassName) {
      document.body.classList.toggle('sidebar-title-content-open');
    }
  };

  toggleBodyClass() {
    document.body.classList.toggle('sidebar-title-content-open');
  }

  onFilter() {
    const { companies } = this.props;
    const filter = this.filter.value;

    let companiesFilter = companies;

    companiesFilter = companies.filter(
      value =>
        value.companyName.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );

    this.setState({
      companies: companiesFilter,
      filter
    });
  }

  handleChooseCompany(companyId) {
    this.setState({ filter: '' });
    this.toggleBodyClass();
    this.props.getCompany(companyId);
    this.props.clearValueProposition();
  }

  render() {
    const { companies, filter } = this.state;
    const { companyName } = this.state.company;

    return (
      <div className="sidebar-title-wrapper">
        {this.props.loading ? (
          <div style={{ height: '50px' }}>
            <Loading small center />
          </div>
        ) : (
          <div
            className="sidebar-title-inner"
            onClick={this.toggleBodyClass.bind(this)}
          >
            <div className="sidebar-subtitle">Switch Company</div>
            <div className="sidebar-title truncate" style={{ width: '170px' }}>
              {companyName}
            </div>
            <div className="sidebar-action">
              <i className="md-icon">arrow_drop_down</i>
            </div>
          </div>
        )}

        <div className="sidebar-title-content">
          <div className="sidebar-title-content-label">Switch Company</div>

          <div className="sidebar-title-content-body">
            <div className="form-group">
              <div className="pt-input-group">
                <span className="pt-icon pt-icon-search" />
                <input
                  type="text"
                  ref={input => (this.filter = input)}
                  onChange={() => this.onFilter()}
                  className="pt-input"
                  value={filter}
                  placeholder="Search Company"
                />
              </div>
            </div>

            <ul>
              {companies.map(({ _id, companyName }, index) =>
                index <= 4 ? (
                  <li key={index}>
                    <Link
                      to={`/workspace/${_id}`}
                      onClick={() => this.handleChooseCompany(_id)}
                      className="p-0 m-0"
                    >
                      {companyName}
                    </Link>
                  </li>
                ) : (
                  ''
                )
              )}
            </ul>

            <div className="sidebar-title-content-body-more">
              <Link
                to="/companies"
                onClick={this.toggleBodyClass.bind(this)}
                className="p-0 m-0"
              >
                Show All Companies
              </Link>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

MenuSwitch.propTypes = {
  companies: PropTypes.array.isRequired,
  company: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  getCompanies: PropTypes.func.isRequired,
  getCompany: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.companies
});

const mapDispatchToProps = dispatch => ({
  getCompanies: () => dispatch(actionCreators.getCompanies()),
  getCompany: companyId => dispatch(actionCreators.getCompany(companyId)),
  clearValueProposition: () => dispatch(actionCreators.clearValueProposition())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(onClickOutside(MenuSwitch)));
