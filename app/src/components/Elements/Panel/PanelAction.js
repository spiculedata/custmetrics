import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';

export class PanelAction extends Component {
  render() {
    return (
      <div className="panel-action">
        <NavLink
          to={this.props.to ? this.props.to : ''}
          onClick={() => document.body.classList.toggle('content-side-open')}
        >
          <div className="panel-action-inner">
            {this.props.icon ? (
              <i className="md-icon">{this.props.icon}</i>
            ) : (
              ''
            )}

            {this.props.title ? <strong>{this.props.title}</strong> : ''}
          </div>
        </NavLink>
      </div>
    );
  }
}

PanelAction.propTypes = {
  title: PropTypes.string,
  icon: PropTypes.string,
  to: PropTypes.string
};

export default PanelAction;
