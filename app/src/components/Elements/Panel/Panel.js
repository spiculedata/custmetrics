import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import onClickOutside from 'react-onclickoutside';

import { actionCreators } from '../../../actions';

import PanelContainer from './PanelContainer';
import PanelUser from './PanelUser';
import PanelListWrapper from './PanelListWrapper';
// import PanelListTitle from './PanelListTitle';
// import PanelList from './PanelList';
// import PanelListItem from './PanelListItem';
import PanelActionWrapper from './PanelActionWrapper';
import PanelAction from './PanelAction';

import './Panel.css';
// import user from '../../../images/user.jpeg';

class Panel extends Component {
  handleClickOutside = event => {
    const hasClassName = document.body.classList.contains('content-side-open');

    if (hasClassName) {
      document.body.classList.toggle('content-side-open');
    }
  };

  onLogoutClick(event) {
    event.preventDefault();

    document.body.classList.toggle('content-side-open');

    this.props.logoutUser();
  }

  render() {
    const { user } = this.props.auth;

    return (
      <PanelContainer>
        {/*<PanelUser name="Breno Polanski" subtitle="Meteorite BI" image={user} />*/}
        <PanelUser name={user.name} image={user.avatar} />

        <PanelListWrapper>
          {/*
          <PanelListTitle>Help</PanelListTitle>

          <PanelList>
            <PanelListItem>Lorem ipsum dolor sit amet</PanelListItem>
            <PanelListItem>Lorem ipsum dolor sit amet</PanelListItem>
            <PanelListItem>Lorem ipsum dolor sit amet</PanelListItem>
          </PanelList>
          */}
        </PanelListWrapper>

        <PanelActionWrapper>
          <PanelAction title="Profile" icon="person" to="/profile" />
          <PanelAction title="Settings" icon="build" to="/settings" />
          <div className="panel-action">
            <a
              to={this.props.to ? this.props.to : ''}
              onClick={this.onLogoutClick.bind(this)}
            >
              <div className="panel-action-inner">
                <i className="md-icon">power_settings_new</i>
                <strong>Logout</strong>
              </div>
            </a>
          </div>
        </PanelActionWrapper>
      </PanelContainer>
    );
  }
}

Panel.propTypes = {
  auth: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

const mapDispatchToProps = dispatch => ({
  logoutUser: () => dispatch(actionCreators.logoutUser())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(onClickOutside(Panel));
