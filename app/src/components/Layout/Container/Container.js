import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import _ from 'lodash';

import Toolbar from '../../Elements/Toolbar';
import Sidebar from '../../Elements/Sidebar';
import PageTitle from '../../Elements/PageTitle';
import Panel from '../../Elements/Panel';

import './Container.css';

class Container extends Component {
  state = {
    companyId: ''
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.match.params && nextProps.match.params.companyId) {
      const companyIdState = this.state.companyId;
      const companyIdNextProps = nextProps.match.params.companyId;

      if (!_.isEqual(companyIdState, companyIdNextProps)) {
        this.setState({ companyId: companyIdNextProps });
      }
    }
  }

  render() {
    const { companyId } = this.state;

    return (
      <div className="page-inner">
        <Toolbar companyId={companyId} />
        <Sidebar />

        <div className="main">
          <PageTitle />
          <div className="content">{this.props.children}</div>
        </div>

        <div className="content-side-wrapper">
          <div className="content-side-overlay" />
          <div className="content-side">
            <Panel />
          </div>
        </div>
      </div>
    );
  }
}

Container.propTypes = {
  children: PropTypes.node.isRequired
};

export default withRouter(Container);
