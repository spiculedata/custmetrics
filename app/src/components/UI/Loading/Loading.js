import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Spinner } from '@blueprintjs/core';
import classnames from 'classnames';

class Loading extends Component {
  render() {
    return (
      <div
        className={classnames({
          'center-block': this.props.center
        })}
      >
        <Spinner {...this.props} /> &nbsp; {this.props.text}
      </div>
    );
  }
}

Loading.propTypes = {
  className: PropTypes.string,
  intent: PropTypes.oneOf(['danger', 'none', 'primary', 'success', 'warning']),
  text: PropTypes.string,
  large: PropTypes.bool,
  small: PropTypes.bool,
  center: PropTypes.bool
};

Loading.defaultProps = {
  intent: 'none'
};

export default Loading;
