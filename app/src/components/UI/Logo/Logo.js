import React, { Component } from 'react';
import PropTypes from 'prop-types';

import logo from '../../../images/custmetrics-logo.png';

class Logo extends Component {
  render() {
    const { className } = this.props;

    return (
      <img
        {...this.props}
        src={logo}
        className={className}
        alt="CustMetrics logo"
      />
    );
  }
}

Logo.propTypes = {
  className: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number
};

Logo.defaultProps = {
  width: 150,
  height: 150
};

export default Logo;
