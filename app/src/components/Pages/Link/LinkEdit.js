import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';
import LinkForm from './LinkForm';

class LinkEdit extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  componentDidMount() {
    const { linkId } = this.props.match.params;

    this.props.getLink(linkId);
  }

  handleSubmit(values) {
    const { companyId, linkId } = this.props.match.params;

    values.companyId = companyId;
    values.type = 'link';

    this.setState({ isBtnLoading: true });
    this.props.onSubmit(linkId, values, this.props.history);
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          <LinkForm
            onSubmit={this.handleSubmit.bind(this)}
            initialValues={this.props.link}
            isBtnLoading={isBtnLoading}
            errors={errors}
          />
        </div>
      </Container>
    );
  }
}

LinkEdit.propTypes = {
  link: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getLink: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.links,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getLink: linkId => dispatch(actionCreators.getLink(linkId)),
  onSubmit: (linkId, linkData, history) =>
    dispatch(actionCreators.editLink(linkId, linkData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(LinkEdit));
