import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import tinycolor from 'tinycolor2';
import { Button, Intent } from '@blueprintjs/core';
import classnames from 'classnames';
import _ from 'lodash';

import { actionCreators } from '../../../actions';

import './Link.css';

class LinkForm extends Component {
  state = {
    companyId: '',
    tags: []
  };

  componentWillMount() {
    const { companyId } = this.props.match.params;
    const { tags } = this.props;

    this.setState({ companyId, tags });
  }

  componentDidMount() {
    this.props.getTags();
  }

  componentWillReceiveProps(nextProps) {
    const tagsState = this.state.tags;
    const tagsNextProps = nextProps.tags;

    if (!_.isEqual(tagsState, tagsNextProps)) {
      this.setState({ tags: tagsNextProps });
    }
  }

  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      reset,
      isBtnLoading,
      errors
    } = this.props;
    const { companyId, tags } = this.state;

    return (
      <form className="link-form" onSubmit={handleSubmit}>
        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.title
          })}
        >
          <label className="pt-label" htmlFor="title">
            Link Name
          </label>
          <Field
            name="title"
            component="input"
            type="text"
            tabIndex="1"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.title
              })
            }}
            autoFocus
          />
          <div className="pt-form-helper-text">{errors.title}</div>
        </div>

        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.url
          })}
        >
          <label className="pt-label" htmlFor="url">
            URL
          </label>
          <Field
            name="url"
            component="input"
            type="url"
            tabIndex="2"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.url
              })
            }}
          />
          <div className="pt-form-helper-text">{errors.url}</div>
        </div>

        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.tagId
          })}
        >
          <label className="pt-label" htmlFor="tagId">
            Tags
          </label>
          <div className="pt-select pt-fill">
            <Field
              name="tagId"
              component="select"
              tabIndex="3"
              props={{
                className: classnames({
                  'pt-intent-danger': errors.tagId
                })
              }}
            >
              <option />
              {tags.map((tag, index) => (
                <option
                  style={{
                    backgroundColor: tag.color ? tag.color : '#333333',
                    color: tinycolor(tag.color).isLight()
                      ? '#000000'
                      : '#ffffff'
                  }}
                  key={index}
                  value={tag._id}
                >
                  {tag.name}
                </option>
              ))}
            </Field>
            <div className="pt-form-helper-text">{errors.tagId}</div>
          </div>
        </div>

        <Field type="hidden" component="input" name="id" />

        <div className="pt-form-group pt-inline">
          <Button
            type="submit"
            intent={Intent.PRIMARY}
            text="Submit"
            tabIndex="4"
            loading={isBtnLoading}
            disabled={pristine || submitting}
          />
          <Button
            onClick={reset}
            text="Reset"
            tabIndex="5"
            disabled={pristine || submitting}
          />
          <Link
            to={`/workspace/${companyId}`}
            className="pt-button pt-minimal pt-icon-chevron-left"
            tabIndex="6"
          >
            Back
          </Link>
        </div>
      </form>
    );
  }
}

LinkForm.propTypes = {
  tags: PropTypes.array.isRequired,
  getTags: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.tags
});

const mapDispatchToProps = dispatch => ({
  getTags: () => dispatch(actionCreators.getTags())
});

export default reduxForm({
  // This is the form's name and must be unique across the app
  form: 'linkForm'
})(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(withRouter(LinkForm))
);
