import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import TagForm from './TagForm';

class TagCreate extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  handleSubmit(values) {
    this.setState({ isBtnLoading: true });
    this.props.onSubmit(values, this.props.history);
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return (
      <Container>
        <div className="content-inner">
          <TagForm
            onSubmit={this.handleSubmit.bind(this)}
            isBtnLoading={isBtnLoading}
            errors={errors}
            showBackButton
          />
        </div>
      </Container>
    );
  }
}

TagCreate.propTypes = {
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  onSubmit: (tagData, history) =>
    dispatch(actionCreators.addTag(tagData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(TagCreate));
