import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actionCreators } from '../../../actions';

import TagForm from './TagForm';

class TagCreateDialog extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  handleSubmit(values) {
    this.setState({ isBtnLoading: true });
    this.props.onSubmit(values, null);

    if (values && values.name) {
      this.props.dialogClose();
    }
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return (
      <div className="p-t-20 p-l-20">
        <TagForm
          onSubmit={this.handleSubmit.bind(this)}
          isBtnLoading={isBtnLoading}
          showBackButton={false}
          errors={errors}
        />
      </div>
    );
  }
}

TagCreateDialog.propTypes = {
  dialogClose: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  onSubmit: (tagData, history) =>
    dispatch(actionCreators.addTag(tagData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagCreateDialog);
