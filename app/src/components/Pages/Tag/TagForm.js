import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { Button, Intent } from '@blueprintjs/core';
import classnames from 'classnames';

class TagForm extends Component {
  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      reset,
      isBtnLoading,
      showBackButton,
      errors
    } = this.props;

    return (
      <form className="tag-form" onSubmit={handleSubmit}>
        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.name
          })}
        >
          <label className="pt-label" htmlFor="name">
            Tag Name
          </label>
          <Field
            name="name"
            component="input"
            type="text"
            tabIndex="1"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.name
              })
            }}
            autoFocus
          />
          <div className="pt-form-helper-text">{errors.name}</div>
        </div>

        <div className="pt-form-group">
          <label className="pt-label" htmlFor="color">
            Tag Color
          </label>

          <Field name="color" component="input" type="color" tabIndex="2" />
        </div>

        <Field type="hidden" component="input" name="id" />

        <div className="pt-form-group pt-inline">
          <Button
            type="submit"
            intent={Intent.PRIMARY}
            text="Submit"
            tabIndex="3"
            loading={isBtnLoading}
            disabled={pristine || submitting}
          />
          <Button
            onClick={reset}
            text="Reset"
            tabIndex="4"
            disabled={pristine || submitting}
          />
          {showBackButton ? (
            <Link
              to="/tags"
              className="pt-button pt-minimal pt-icon-chevron-left"
              tabIndex="5"
            >
              Back
            </Link>
          ) : (
            ''
          )}
        </div>
      </form>
    );
  }
}

export default reduxForm({
  // This is the form's name and must be unique across the app
  form: 'tagForm'
})(TagForm);
