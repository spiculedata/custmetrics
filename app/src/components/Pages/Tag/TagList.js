import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Alert, Button, Intent, NonIdealState, Tag } from '@blueprintjs/core';
import ReactTable from 'react-table';
import tinycolor from 'tinycolor2';
import BlockUi from 'react-block-ui';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loader from '../../UI/Loader';

import './Tag.css';

class TagList extends Component {
  state = {
    tagId: '',
    tags: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false,
    isOpenDeleteAlertError: false,
    isBlockingUi: false,
    errors: {}
  };

  componentWillMount() {
    const { tags } = this.props;

    this.setState({ tags });
  }

  componentDidMount() {
    this.props.getTags();
  }

  componentWillReceiveProps(nextProps) {
    const tagsState = this.state.tags;
    const tagsNextProps = nextProps.tags;
    const { errors } = nextProps;

    if (!_.isEqual(tagsState, tagsNextProps)) {
      this.setState({ tags: tagsNextProps });
    }

    if (!_.isEmpty(errors) && this.state.isBlockingUi) {
      this.setState({
        isOpenDeleteAlertError: true,
        isBlockingUi: false,
        errors
      });
    }

    if (!nextProps.loading && this.state.isBlockingUi) {
      this.setState({ isBlockingUi: false });
    }
  }

  onFilter() {
    const { tags } = this.props;
    const filter = this.filter.value;

    let tagsFilter = tags;

    tagsFilter = tags.filter(
      value => value.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );

    this.setState({
      tags: tagsFilter
    });
  }

  handleCancelAlertDelete() {
    this.setState({
      tagId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: false
    });
  }

  handleConfirmAlertDelete() {
    const { tagId } = this.state;

    this.props.onClickDelete(tagId);

    this.setState({
      tagId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: true
    });
  }

  handleErrorClose() {
    this.props.clearErrors();
    this.props.getTags();

    this.setState({
      isOpenDeleteAlertError: false,
      isBlockingUi: false,
      errors: {}
    });
  }

  render() {
    const {
      tags,
      itemNameToDelete,
      isOpenAlertDelete,
      isOpenDeleteAlertError,
      isBlockingUi,
      errors
    } = this.state;
    const btnCreateTag = (
      <Link
        to="/tag/new"
        className="pt-button pt-minimal pt-icon-add pt-intent-primary"
      >
        Create Tag
      </Link>
    );

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loader />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          {this.props.tags.length === 0 ? (
            <NonIdealState
              visual="tag"
              title="No tags created"
              description="Create a new tag:"
              action={btnCreateTag}
            />
          ) : (
            <div>
              <div className="pt-form-group form-group-search">
                <div className="pt-input-group m-r-10" style={{ width: '50%' }}>
                  <span className="pt-icon pt-icon-search" />
                  <input
                    type="text"
                    ref={input => (this.filter = input)}
                    onChange={() => this.onFilter()}
                    className="pt-input"
                    placeholder="Search Tag"
                  />
                </div>

                <div className="m-r-30">{btnCreateTag}</div>
              </div>

              <BlockUi tag="div" blocking={isBlockingUi}>
                <ReactTable
                  data={tags}
                  columns={[
                    {
                      Header: 'Info',
                      columns: [
                        {
                          Header: 'Tag Name',
                          accessor: 'name',
                          headerClassName: ['text-left', 'bold'],
                          Cell: d => (
                            <Tag
                              large
                              style={{
                                backgroundColor: d.original.color
                                  ? d.original.color
                                  : '#333333',
                                color: tinycolor(d.original.color).isLight()
                                  ? '#000000'
                                  : '#ffffff'
                              }}
                            >
                              {d.original.name}
                            </Tag>
                          )
                        }
                      ]
                    },
                    {
                      Header: 'Actions',
                      columns: [
                        {
                          Header: 'Edit',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Link
                              to={`/tag/${d.original._id}`}
                              className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                              title="Edit"
                            />
                          )
                        },
                        {
                          Header: 'Delete',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Button
                              icon="trash"
                              intent={Intent.DANGER}
                              title="Delete"
                              minimal
                              onClick={() =>
                                this.setState({
                                  tagId: d.original._id,
                                  itemNameToDelete: d.original.name,
                                  isOpenAlertDelete: true
                                })
                              }
                            />
                          )
                        }
                      ]
                    }
                  ]}
                  defaultPageSize={20}
                  style={{
                    height: '90vh'
                  }}
                  className="-striped -highlight"
                />
              </BlockUi>

              <Alert
                canEscapeKeyCancel={true}
                canOutsideClickCancel={true}
                confirmButtonText="Close"
                icon="error"
                intent={Intent.DANGER}
                isOpen={isOpenDeleteAlertError}
                onClose={this.handleErrorClose.bind(this)}
              >
                <p>{errors.noTagFound}</p>
              </Alert>

              <Alert
                canEscapeKeyCancel={true}
                canOutsideClickCancel={true}
                cancelButtonText="Cancel"
                confirmButtonText="Delete"
                icon="trash"
                intent={Intent.DANGER}
                isOpen={isOpenAlertDelete}
                onCancel={this.handleCancelAlertDelete.bind(this)}
                onConfirm={this.handleConfirmAlertDelete.bind(this)}
              >
                <p>
                  Are you sure that you want to delete the{' '}
                  <b>{itemNameToDelete}</b> tag?
                </p>
              </Alert>
            </div>
          )}
        </div>
      </Container>
    );
  }
}

TagList.propTypes = {
  tags: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getTags: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.tags,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getTags: () => dispatch(actionCreators.getTags()),
  onClickDelete: id => dispatch(actionCreators.deleteTag(id)),
  clearErrors: () => dispatch(actionCreators.clearErrors())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TagList);
