import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';
import TagForm from './TagForm';

class TagEdit extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  componentDidMount() {
    const { tagId } = this.props.match.params;

    this.props.getTag(tagId);
  }

  handleSubmit(values) {
    const { tagId } = this.props.match.params;

    this.setState({ isBtnLoading: true });
    this.props.onSubmit(tagId, values, this.props.history);
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          <TagForm
            onSubmit={this.handleSubmit.bind(this)}
            initialValues={this.props.tag}
            isBtnLoading={isBtnLoading}
            errors={errors}
            showBackButton
          />
        </div>
      </Container>
    );
  }
}

TagEdit.propTypes = {
  tag: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getTag: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.tags,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getTag: tagId => dispatch(actionCreators.getTag(tagId)),
  onSubmit: (tagId, tagData, history) =>
    dispatch(actionCreators.editTag(tagId, tagData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(TagEdit));
