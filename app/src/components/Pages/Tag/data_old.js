export default [
  {
    id: '0',
    name: 'Difficulty: Complex',
    color: '#d93f0b'
  },
  {
    id: '1',
    name: 'Difficulty: Impossible',
    color: '#bfdadc'
  },
  {
    id: '2',
    name: 'Difficulty: Medium',
    color: '#b60205'
  },
  {
    id: '3',
    name: 'Difficulty: Starter',
    color: '#bfd4f2'
  },
  {
    id: '4',
    name: 'Priority: Critical',
    color: '#1d76db'
  },
  {
    id: '5',
    name: 'Priority: High',
    color: '#bfdadc'
  },
  {
    id: '6',
    name: 'Priority: Low',
    color: '#c2e0c6'
  },
  {
    id: '7',
    name: 'Priority: Medium',
    color: '#f9d0c4'
  },
  {
    id: '8',
    name: 'Status: Abandoned',
    color: '#fef2c0'
  },
  {
    id: '9',
    name: 'Status: Accepted',
    color: '#006b75'
  },
  {
    id: '10',
    name: 'Status: Available',
    color: '#0e8a16'
  },
  {
    id: '11',
    name: 'Status: Blocked',
    color: '#b60205'
  },
  {
    id: '12',
    name: 'Type: Question',
    color: '#0e8a16'
  }
];
