export default [
  {
    id: '0',
    name: 'Identity',
    color: '#d93f0b'
  },
  {
    id: '1',
    name: 'Context',
    color: '#bfdadc'
  },
  {
    id: '2',
    name: 'Messaging',
    color: '#b60205'
  },
  {
    id: '3',
    name: 'Points of Contact',
    color: '#bfd4f2'
  },
  {
    id: '4',
    name: 'Notes',
    color: '#1d76db'
  }
];
