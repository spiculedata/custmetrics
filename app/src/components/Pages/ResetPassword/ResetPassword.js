import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import {
  Button,
  Callout,
  FormGroup,
  Intent,
  InputGroup
} from '@blueprintjs/core';
import _ from 'lodash';

import { actionCreators } from '../../../actions';

import Empty from '../../Layout/Empty';
import Logo from '../../UI/Logo';

class ResetPassword extends Component {
  constructor() {
    super();

    this.state = {
      email: '',
      isBtnLoading: false,
      isResetPassword: false,
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { auth } = this.props;

    if (auth.isAuthenticated) {
      this.handleAuthenticate();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { auth, errors } = nextProps;

    if (auth.isAuthenticated) {
      this.handleAuthenticate();
    }

    if (errors) {
      this.setState({ isBtnLoading: false, isResetPassword: false, errors });
    }

    if (
      !_.isEmpty(nextProps.location.search) &&
      nextProps.location.search === '?reset=true'
    ) {
      this.setState({
        isResetPassword: true
      });
    }
  }

  authenticated() {
    const $el = document.getElementById('ipl-progress-indicator');

    $el.removeAttribute('hidden');

    return new Promise(resolve => setTimeout(resolve, 2000));
  }

  handleAuthenticate() {
    const { push } = this.props.history;

    this.authenticated().then(() => {
      const $el = document.getElementById('ipl-progress-indicator');

      push('/companies');

      if ($el) {
        $el.classList.add('available');

        setTimeout(() => {
          $el.setAttribute('hidden', 'hidden');
          $el.classList.remove('available');
        }, 2000);
      }
    });
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const { email } = this.state;

    this.setState({ isBtnLoading: true });

    this.props.resetPassword({ email }, this.props.history);
  }

  render() {
    const { email, isBtnLoading, isResetPassword, errors } = this.state;

    return (
      <Empty>
        <div className="login">
          <div className="logo">
            <Logo />
          </div>

          <form onSubmit={this.handleSubmit}>
            {isResetPassword ? (
              <div>
                <Callout
                  icon="tick"
                  intent={Intent.SUCCESS}
                  title="Success!"
                  style={{ marginBottom: '20px' }}
                >
                  <div>
                    <p>
                      We have sent you an email with reset instructions.
                      <br />
                    </p>
                    <p>
                      If the email does not arrive soon, check your{' '}
                      <b>spam folder</b>.
                    </p>
                  </div>
                </Callout>

                <div className="form-group form-group-button">
                  <div className="form-group-button-description">
                    <NavLink to="/">← Back to login</NavLink>
                  </div>
                </div>
              </div>
            ) : (
              <div>
                <FormGroup
                  label="Email"
                  labelFor="email"
                  intent={errors.email ? Intent.DANGER : Intent.NONE}
                  helperText={errors.email ? errors.email : ''}
                >
                  <InputGroup
                    type="email"
                    id="email"
                    name="email"
                    value={email}
                    intent={errors.email ? Intent.DANGER : Intent.NONE}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <div className="form-group form-group-button">
                  <div className="form-group-button-description">
                    <NavLink to="/">← Back to login</NavLink>
                  </div>

                  <Button
                    type="submit"
                    className="button-right"
                    intent={Intent.PRIMARY}
                    text="Reset Password"
                    loading={isBtnLoading}
                    large
                  />
                </div>
              </div>
            )}
          </form>
        </div>
      </Empty>
    );
  }
}

ResetPassword.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  resetPassword: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  resetPassword: (email, history) =>
    dispatch(actionCreators.resetPassword(email, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ResetPassword));
