import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import { Button, FormGroup, Intent, InputGroup } from '@blueprintjs/core';
import PasswordMask from 'react-password-mask';
import classnames from 'classnames';

import { actionCreators } from '../../../actions';

import Empty from '../../Layout/Empty';
import Logo from '../../UI/Logo';

class Register extends Component {
  constructor() {
    super();

    this.state = {
      name: '',
      email: '',
      password: '',
      password2: '',
      isBtnLoading: false,
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { auth } = this.props;

    if (auth.isAuthenticated) {
      this.handleAuthenticate();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  authenticated() {
    const $el = document.getElementById('ipl-progress-indicator');

    $el.removeAttribute('hidden');

    return new Promise(resolve => setTimeout(resolve, 2000));
  }

  handleAuthenticate() {
    const { push } = this.props.history;

    this.authenticated().then(() => {
      const $el = document.getElementById('ipl-progress-indicator');

      push('/companies');

      if ($el) {
        $el.classList.add('available');

        setTimeout(() => {
          $el.setAttribute('hidden', 'hidden');
          $el.classList.remove('available');
        }, 2000);
      }
    });
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const { name, email, password, password2 } = this.state;

    const newUser = {
      name,
      email,
      password,
      password2
    };

    this.setState({ isBtnLoading: true });

    this.props.registerUser(newUser, this.props.history);
  }

  render() {
    const {
      name,
      email,
      password,
      password2,
      isBtnLoading,
      errors
    } = this.state;

    return (
      <Empty>
        <div className="login">
          <div className="logo">
            <Logo />
          </div>

          <form onSubmit={this.handleSubmit}>
            <FormGroup
              label="Name"
              labelFor="name"
              intent={errors.name ? Intent.DANGER : Intent.NONE}
              helperText={errors.name ? errors.name : ''}
            >
              <InputGroup
                type="text"
                id="name"
                name="name"
                value={name}
                intent={errors.name ? Intent.DANGER : Intent.NONE}
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup
              label="Email"
              labelFor="email"
              intent={errors.email ? Intent.DANGER : Intent.NONE}
              helperText={errors.email ? errors.email : ''}
            >
              <InputGroup
                type="email"
                id="email"
                name="email"
                value={email}
                intent={errors.email ? Intent.DANGER : Intent.NONE}
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup
              label="Password"
              labelFor="password"
              intent={errors.password ? Intent.DANGER : Intent.NONE}
              helperText={errors.password ? errors.password : ''}
            >
              <PasswordMask
                id="password"
                name="password"
                buttonClassName="btn-password-mask"
                inputClassName={classnames('pt-input', {
                  'pt-intent-danger': errors.password
                })}
                buttonStyles={{ marginTop: '-12px' }}
                value={password}
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup
              label="Confirm Password"
              labelFor="password2"
              intent={errors.password2 ? Intent.DANGER : Intent.NONE}
              helperText={errors.password2 ? errors.password2 : ''}
            >
              <PasswordMask
                id="password2"
                name="password2"
                buttonClassName="btn-password-mask"
                inputClassName={classnames('pt-input', {
                  'pt-intent-danger': errors.password2
                })}
                buttonStyles={{ marginTop: '-12px' }}
                value={password2}
                onChange={this.handleChange}
              />
            </FormGroup>

            <div className="form-group form-group-button">
              <div className="form-group-button-description">
                Already have an account? <Link to="/">Log in</Link>
              </div>

              <Button
                type="submit"
                className="button-right"
                intent={Intent.PRIMARY}
                text="Register"
                loading={isBtnLoading}
                large
              />
            </div>
          </form>
        </div>
      </Empty>
    );
  }
}

Register.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  registerUser: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  registerUser: (newUser, history) =>
    dispatch(actionCreators.registerUser(newUser, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Register));
