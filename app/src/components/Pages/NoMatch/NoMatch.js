import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Button, NonIdealState } from '@blueprintjs/core';

import Empty from '../../Layout/Empty';

class NoMatch extends Component {
  render() {
    const btnGoBack = (
      <Button
        icon="chevron-left"
        intent="primary"
        text="Go Back"
        minimal
        onClick={() => this.props.history.goBack()}
      />
    );

    return (
      <Empty>
        <div className="page-empty-container">
          <NonIdealState
            visual="error"
            title="404 ERROR"
            description="Page not found!"
            action={btnGoBack}
          />
        </div>
      </Empty>
    );
  }
}

export default withRouter(NoMatch);
