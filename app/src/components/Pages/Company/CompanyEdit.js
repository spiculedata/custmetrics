import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';
import CompanyForm from './CompanyForm';

class CompanyEdit extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  componentDidMount() {
    const { companyId } = this.props.match.params;

    this.props.getCompany(companyId);
  }

  handleSubmit(values) {
    const { companyId } = this.props.match.params;

    this.setState({ isBtnLoading: true });
    this.props.onSubmit(companyId, values, this.props.history);
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          <CompanyForm
            onSubmit={this.handleSubmit.bind(this)}
            initialValues={this.props.company}
            isBtnLoading={isBtnLoading}
            errors={errors}
          />
        </div>
      </Container>
    );
  }
}

CompanyEdit.propTypes = {
  company: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getCompany: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.companies,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getCompany: companyId => dispatch(actionCreators.getCompany(companyId)),
  onSubmit: (companyId, companyData, history) =>
    dispatch(actionCreators.editCompany(companyId, companyData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CompanyEdit));
