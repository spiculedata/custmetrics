import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { Button, Intent } from '@blueprintjs/core';
import classnames from 'classnames';

class CompanyForm extends Component {
  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      reset,
      isBtnLoading,
      errors
    } = this.props;

    return (
      <form className="company-form" onSubmit={handleSubmit}>
        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.companyName
          })}
        >
          <label className="pt-label" htmlFor="companyName">
            Company Name
          </label>
          <Field
            name="companyName"
            component="input"
            type="text"
            tabIndex="1"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.companyName
              })
            }}
            autoFocus
          />
          <div className="pt-form-helper-text">{errors.companyName}</div>
        </div>

        <div className="pt-form-group">
          <label className="pt-label" htmlFor="contactName">
            Contact Name
          </label>
          <Field
            name="contactName"
            component="input"
            type="text"
            tabIndex="2"
            props={{ className: 'pt-input' }}
          />
        </div>

        <div className="pt-form-group">
          <label className="pt-label" htmlFor="address">
            Address
          </label>
          <Field
            name="address"
            component="input"
            type="text"
            tabIndex="3"
            props={{ className: 'pt-input' }}
          />
        </div>

        <div className="pt-form-group">
          <label className="pt-label" htmlFor="phone">
            Phone
          </label>
          <Field
            name="phone"
            component="input"
            type="tel"
            tabIndex="4"
            props={{ className: 'pt-input' }}
          />
        </div>

        <div className="pt-form-group">
          <label className="pt-label" htmlFor="email">
            E-mail
          </label>
          <Field
            name="email"
            component="input"
            type="email"
            tabIndex="5"
            props={{ className: 'pt-input' }}
          />
        </div>

        <Field type="hidden" component="input" name="id" />

        <div className="pt-form-group pt-inline">
          <Button
            type="submit"
            intent={Intent.PRIMARY}
            text="Submit"
            tabIndex="6"
            loading={isBtnLoading}
            disabled={pristine || submitting}
          />
          <Button
            onClick={reset}
            text="Reset"
            tabIndex="7"
            disabled={pristine || submitting}
          />
          <Link
            to="/companies"
            className="pt-button pt-minimal pt-icon-chevron-left"
            tabIndex="8"
          >
            Back
          </Link>
        </div>
      </form>
    );
  }
}

export default reduxForm({
  // This is the form's name and must be unique across the app
  form: 'companyForm'
})(CompanyForm);
