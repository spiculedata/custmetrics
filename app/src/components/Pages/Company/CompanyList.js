import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Alert, Button, Intent, NonIdealState } from '@blueprintjs/core';
import ReactTable from 'react-table';
import BlockUi from 'react-block-ui';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loader from '../../UI/Loader';

import './Company.css';

class CompanyList extends Component {
  state = {
    companyId: '',
    companies: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false,
    isOpenDeleteAlertError: false,
    isBlockingUi: false,
    errors: {}
  };

  componentWillMount() {
    const { companies } = this.props;

    this.setState({ companies });
  }

  componentDidMount() {
    this.props.getCompanies();
  }

  componentWillReceiveProps(nextProps) {
    const companiesState = this.state.companies;
    const companiesNextProps = nextProps.companies;
    const { errors } = nextProps;

    if (!_.isEqual(companiesState, companiesNextProps)) {
      this.setState({ companies: companiesNextProps });
    }

    if (!_.isEmpty(errors) && this.state.isBlockingUi) {
      this.setState({
        isOpenDeleteAlertError: true,
        isBlockingUi: false,
        errors
      });
    }

    if (!nextProps.loading && this.state.isBlockingUi) {
      this.setState({ isBlockingUi: false });
    }
  }

  onFilter() {
    const { companies } = this.props;
    const filter = this.filter.value;

    let companiesFilter = companies;

    companiesFilter = companies.filter(value => {
      return (
        value.companyName.toLowerCase().indexOf(filter.toLowerCase()) !== -1 ||
        (value.contactName &&
          value.contactName.toLowerCase().indexOf(filter.toLowerCase()) !== -1)
      );
    });

    this.setState({
      companies: companiesFilter
    });
  }

  handleCancelAlertDelete() {
    this.setState({
      companyId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: false
    });
  }

  handleConfirmAlertDelete() {
    const { companyId } = this.state;

    this.props.onClickDelete(companyId);

    this.setState({
      companyId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: true
    });
  }

  handleErrorClose() {
    this.props.clearErrors();
    this.props.getCompanies();

    this.setState({
      isOpenDeleteAlertError: false,
      isBlockingUi: false,
      errors: {}
    });
  }

  render() {
    const {
      companies,
      itemNameToDelete,
      isOpenAlertDelete,
      isOpenDeleteAlertError,
      isBlockingUi,
      errors
    } = this.state;
    const btnCreateCompany = (
      <Link
        to="/company/new"
        className="pt-button pt-minimal pt-icon-add pt-intent-primary"
      >
        Create Company
      </Link>
    );

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loader />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          {this.props.companies.length === 0 ? (
            <NonIdealState
              visual="office"
              title="No company created"
              description="Create a new company to start:"
              action={btnCreateCompany}
            />
          ) : (
            <div>
              <div className="pt-form-group form-group-search">
                <div className="pt-input-group m-r-10" style={{ width: '50%' }}>
                  <span className="pt-icon pt-icon-search" />
                  <input
                    type="text"
                    ref={input => (this.filter = input)}
                    onChange={() => this.onFilter()}
                    className="pt-input"
                    placeholder="Search Company"
                  />
                </div>

                <div className="m-r-30">{btnCreateCompany}</div>
              </div>

              <BlockUi tag="div" blocking={isBlockingUi}>
                <ReactTable
                  data={companies}
                  columns={[
                    {
                      Header: 'Info',
                      columns: [
                        {
                          Header: 'Company Name',
                          accessor: 'companyName',
                          headerClassName: ['text-left', 'bold']
                        },
                        {
                          Header: 'Contact Name',
                          accessor: 'contactName',
                          headerClassName: ['text-left', 'bold']
                        },
                        {
                          Header: 'Address',
                          accessor: 'address',
                          headerClassName: ['text-left', 'bold']
                        },
                        {
                          Header: 'Phone',
                          accessor: 'phone',
                          headerClassName: ['text-left', 'bold']
                        },
                        {
                          Header: 'Email',
                          accessor: 'email',
                          headerClassName: ['text-left', 'bold']
                        }
                      ]
                    },
                    {
                      Header: 'Actions',
                      columns: [
                        {
                          Header: 'Start',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Link
                              // to={`/workspace/${d.original._id}?tab=0`}
                              to={`/workspace/${d.original._id}`}
                              className="pt-button pt-minimal pt-icon-play pt-intent-primary"
                              title="Start"
                            />
                          )
                        },
                        {
                          Header: 'Edit',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Link
                              to={`/company/${d.original._id}`}
                              className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                              title="Edit"
                            />
                          )
                        },
                        {
                          Header: 'Delete',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Button
                              icon="trash"
                              intent={Intent.DANGER}
                              title="Delete"
                              minimal
                              onClick={() =>
                                this.setState({
                                  companyId: d.original._id,
                                  itemNameToDelete: d.original.companyName,
                                  isOpenAlertDelete: true
                                })
                              }
                            />
                          )
                        }
                      ]
                    }
                  ]}
                  defaultPageSize={20}
                  style={{
                    height: '90vh'
                  }}
                  className="-striped -highlight"
                />
              </BlockUi>

              <Alert
                canEscapeKeyCancel={true}
                canOutsideClickCancel={true}
                confirmButtonText="Close"
                icon="error"
                intent={Intent.DANGER}
                isOpen={isOpenDeleteAlertError}
                onClose={this.handleErrorClose.bind(this)}
              >
                <p>{errors.noCompanyFound}</p>
              </Alert>

              <Alert
                canEscapeKeyCancel={true}
                canOutsideClickCancel={true}
                cancelButtonText="Cancel"
                confirmButtonText="Delete"
                icon="trash"
                intent={Intent.DANGER}
                isOpen={isOpenAlertDelete}
                onCancel={this.handleCancelAlertDelete.bind(this)}
                onConfirm={this.handleConfirmAlertDelete.bind(this)}
              >
                <p>
                  Are you sure that you want to delete the{' '}
                  <b>{itemNameToDelete}</b> company?
                </p>
              </Alert>
            </div>
          )}
        </div>
      </Container>
    );
  }
}

CompanyList.propTypes = {
  companies: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getCompanies: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.companies,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getCompanies: () => dispatch(actionCreators.getCompanies()),
  onClickDelete: id => dispatch(actionCreators.deleteCompany(id)),
  clearErrors: () => dispatch(actionCreators.clearErrors())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CompanyList);
