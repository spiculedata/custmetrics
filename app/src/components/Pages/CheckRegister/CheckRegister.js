import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { Button, NonIdealState } from '@blueprintjs/core';
import _ from 'lodash';
import Loading from '../../UI/Loading';

import { actionCreators } from '../../../actions';

import Empty from '../../Layout/Empty';

class CheckRegister extends Component {
  componentDidMount() {
    this.props.checkUser(this.props.match.params.userId);
  }

  render() {
    const { errors, history } = this.props;
    const { user, loading } = this.props.auth;
    let CheckRegisterContent;

    const btnGoRegister = (
      <Button
        icon="chevron-left"
        intent="primary"
        text="Go to Register Page"
        minimal
        onClick={() => history.push('/register')}
      />
    );

    const btnGoLogin = (
      <Button
        icon="chevron-left"
        intent="primary"
        text="Go to Login Page"
        minimal
        onClick={() => history.push('/')}
      />
    );

    if (!_.isEmpty(errors)) {
      CheckRegisterContent = (
        <NonIdealState
          visual="error"
          title={errors.noUserFound}
          description="Try again!"
          action={btnGoRegister}
        />
      );
    } else if (user === null || loading) {
      CheckRegisterContent = <Loading center />;
    } else {
      CheckRegisterContent = (
        <NonIdealState
          visual="tick-circle"
          title="Awesome :)"
          description="User created successfully!"
          action={btnGoLogin}
        />
      );
    }

    return (
      <Empty>
        <div className="page-empty-container">{CheckRegisterContent}</div>
      </Empty>
    );
  }
}

CheckRegister.propTypes = {
  checkUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  checkUser: userId => dispatch(actionCreators.checkUser(userId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(CheckRegister));
