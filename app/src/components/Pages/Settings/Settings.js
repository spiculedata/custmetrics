import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Grid, Row, Col } from 'react-flexbox-grid';
import { Card, Elevation, NonIdealState } from '@blueprintjs/core';

import Container from '../../Layout/Container';

class Settings extends Component {
  render() {
    return (
      <Container>
        <div className="content-inner">
          <Grid fluid>
            <Row>
              <Col md={2}>
                <Card
                  interactive={true}
                  elevation={Elevation.TWO}
                  onClick={() => this.props.history.push('/profile')}
                >
                  <NonIdealState visual="user" title="Profile" />
                </Card>
              </Col>
            </Row>
          </Grid>
        </div>
      </Container>
    );
  }
}

export default withRouter(Settings);
