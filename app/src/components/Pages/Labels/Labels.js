import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';

import Container from '../../Layout/Container';
import { PainLabelList } from './PainLabel';
import { RelieverLabelList } from './RelieverLabel';
import { GainLabelList } from './GainLabel';

import './Labels.css';

class Labels extends Component {
  state = {
    tabIndex: 0
  };

  componentWillMount() {
    const { search } = this.props.location;
    const tabIndex = Number(search.split('=')[1]) || 0;

    this.setState({ tabIndex });
  }

  render() {
    const { tabIndex } = this.state;

    return (
      <Container>
        <div className="content-inner">
          <Tabs
            selectedIndex={tabIndex}
            onSelect={tabIndex => {
              this.setState({ tabIndex });
              this.props.history.push(`?tab=${tabIndex}`);
            }}
          >
            <TabList>
              <Tab>Pains Labels</Tab>
              <Tab>Relievers Labels</Tab>
              <Tab>Gains Labels</Tab>
            </TabList>

            <TabPanel>
              <PainLabelList />
            </TabPanel>
            <TabPanel>
              <RelieverLabelList />
            </TabPanel>
            <TabPanel>
              <GainLabelList />
            </TabPanel>
          </Tabs>
        </div>
      </Container>
    );
  }
}

export default withRouter(Labels);
