import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { actionCreators } from '../../../../actions';

import Container from '../../../Layout/Container';
import Loading from '../../../UI/Loading';
import PainLabelForm from './PainLabelForm';

class PainLabelEdit extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  componentDidMount() {
    const { labelId } = this.props.match.params;

    this.props.getPainLabel(labelId);
  }

  handleSubmit(values) {
    const { labelId } = this.props.match.params;
    const { id, name } = values;
    const label = {
      id,
      name,
      label: name,
      value: name
    };

    this.setState({ isBtnLoading: true });
    this.props.onSubmit(labelId, label, this.props.history);
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          <PainLabelForm
            onSubmit={this.handleSubmit.bind(this)}
            initialValues={this.props.painLabel}
            isBtnLoading={isBtnLoading}
            errors={errors}
            showBackButton
          />
        </div>
      </Container>
    );
  }
}

PainLabelEdit.propTypes = {
  painLabel: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getPainLabel: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.painsLabels,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getPainLabel: labelId => dispatch(actionCreators.getPainLabel(labelId)),
  onSubmit: (labelId, labelData, history) =>
    dispatch(actionCreators.editPainLabel(labelId, labelData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(PainLabelEdit));
