import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import { actionCreators } from '../../../../actions';

import Container from '../../../Layout/Container';
import PainLabelForm from './PainLabelForm';

class PainLabelCreate extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  handleSubmit(values) {
    const { name } = values;
    const label = {
      name,
      label: name,
      value: name
    };

    this.setState({ isBtnLoading: true });
    this.props.onSubmit(label, this.props.history);
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return (
      <Container>
        <div className="content-inner">
          <PainLabelForm
            onSubmit={this.handleSubmit.bind(this)}
            isBtnLoading={isBtnLoading}
            errors={errors}
            showBackButton
          />
        </div>
      </Container>
    );
  }
}

PainLabelCreate.propTypes = {
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  onSubmit: (labelData, history) =>
    dispatch(actionCreators.addPainLabel(labelData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(PainLabelCreate));
