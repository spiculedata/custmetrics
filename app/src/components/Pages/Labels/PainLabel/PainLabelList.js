import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Alert, Button, Intent, NonIdealState } from '@blueprintjs/core';
import ReactTable from 'react-table';
import BlockUi from 'react-block-ui';

import { actionCreators } from '../../../../actions';

import Loader from '../../../UI/Loader';

class PainLabelList extends Component {
  state = {
    labelId: '',
    painsLabels: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false,
    isOpenDeleteAlertError: false,
    isBlockingUi: false,
    errors: {}
  };

  componentWillMount() {
    const { painsLabels } = this.props;

    this.setState({ painsLabels });
  }

  componentDidMount() {
    this.props.getPainsLabels();
  }

  componentWillReceiveProps(nextProps) {
    const painsLabelsState = this.state.painsLabels;
    const painsLabelsNextProps = nextProps.painsLabels;
    const { errors } = nextProps;

    if (!_.isEqual(painsLabelsState, painsLabelsNextProps)) {
      this.setState({ painsLabels: painsLabelsNextProps });
    }

    if (!_.isEmpty(errors) && this.state.isBlockingUi) {
      this.setState({
        isOpenDeleteAlertError: true,
        isBlockingUi: false,
        errors
      });
    }

    if (!nextProps.loading && this.state.isBlockingUi) {
      this.setState({ isBlockingUi: false });
    }
  }

  onFilter() {
    const { painsLabels } = this.props;
    const filter = this.filter.value;

    let painsLabelsFilter = painsLabels;

    painsLabelsFilter = painsLabels.filter(
      value => value.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );

    this.setState({
      painsLabels: painsLabelsFilter
    });
  }

  handleCancelAlertDelete() {
    this.setState({
      labelId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: false
    });
  }

  handleConfirmAlertDelete() {
    const { labelId } = this.state;

    this.props.onClickDelete(labelId);

    this.setState({
      labelId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: true
    });
  }

  handleErrorClose() {
    this.props.clearErrors();
    this.props.getPainsLabels();

    this.setState({
      isOpenDeleteAlertError: false,
      isBlockingUi: false,
      errors: {}
    });
  }

  render() {
    const {
      painsLabels,
      itemNameToDelete,
      isOpenAlertDelete,
      isOpenDeleteAlertError,
      isBlockingUi,
      errors
    } = this.state;
    const btnCreatePainLabel = (
      <Link
        to="/label/pain/new"
        className="pt-button pt-minimal pt-icon-add pt-intent-primary"
      >
        Create Pain Label
      </Link>
    );

    return this.props.loading ? (
      <Loader />
    ) : (
      <div>
        {this.props.painsLabels.length === 0 ? (
          <NonIdealState
            visual="label"
            title="No pain label created"
            description="Create a new pain label:"
            className="m-t-20"
            action={btnCreatePainLabel}
          />
        ) : (
          <div>
            <div className="pt-form-group form-group-search">
              <div className="pt-input-group m-r-10" style={{ width: '50%' }}>
                <span className="pt-icon pt-icon-search" />
                <input
                  type="text"
                  ref={input => (this.filter = input)}
                  onChange={() => this.onFilter()}
                  className="pt-input"
                  placeholder="Search Pain Label"
                />
              </div>

              <div className="m-r-30">{btnCreatePainLabel}</div>
            </div>

            <BlockUi tag="div" blocking={isBlockingUi}>
              <ReactTable
                data={painsLabels}
                columns={[
                  {
                    Header: 'Info',
                    columns: [
                      {
                        Header: 'Label Name',
                        accessor: 'name',
                        headerClassName: ['text-left', 'bold']
                      }
                    ]
                  },
                  {
                    Header: 'Actions',
                    columns: [
                      {
                        Header: 'Edit',
                        headerClassName: 'bold',
                        width: 80,
                        className: 'text-center',
                        sortable: false,
                        resizable: false,
                        Cell: d => (
                          <Link
                            to={`/label/pain/${d.original._id}`}
                            className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                            title="Edit"
                          />
                        )
                      },
                      {
                        Header: 'Delete',
                        headerClassName: 'bold',
                        width: 80,
                        className: 'text-center',
                        sortable: false,
                        resizable: false,
                        Cell: d => (
                          <Button
                            icon="trash"
                            intent={Intent.DANGER}
                            title="Delete"
                            minimal
                            onClick={() =>
                              this.setState({
                                labelId: d.original._id,
                                itemNameToDelete: d.original.name,
                                isOpenAlertDelete: true
                              })
                            }
                          />
                        )
                      }
                    ]
                  }
                ]}
                defaultPageSize={20}
                style={{
                  height: '90vh'
                }}
                className="-striped -highlight"
              />
            </BlockUi>

            <Alert
              canEscapeKeyCancel={true}
              canOutsideClickCancel={true}
              confirmButtonText="Close"
              icon="error"
              intent={Intent.DANGER}
              isOpen={isOpenDeleteAlertError}
              onClose={this.handleErrorClose.bind(this)}
            >
              <p>{errors.noPainLabelFound}</p>
            </Alert>

            <Alert
              canEscapeKeyCancel={true}
              canOutsideClickCancel={true}
              cancelButtonText="Cancel"
              confirmButtonText="Delete"
              icon="trash"
              intent={Intent.DANGER}
              isOpen={isOpenAlertDelete}
              onCancel={this.handleCancelAlertDelete.bind(this)}
              onConfirm={this.handleConfirmAlertDelete.bind(this)}
            >
              <p>
                Are you sure that you want to delete the{' '}
                <b>{itemNameToDelete}</b> label?
              </p>
            </Alert>
          </div>
        )}
      </div>
    );
  }
}

PainLabelList.propTypes = {
  painsLabels: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getPainsLabels: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.painsLabels,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getPainsLabels: () => dispatch(actionCreators.getPainsLabels()),
  onClickDelete: id => dispatch(actionCreators.deletePainLabel(id)),
  clearErrors: () => dispatch(actionCreators.clearErrors())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PainLabelList);
