import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { reduxForm, Field } from 'redux-form';
import { Button, Intent } from '@blueprintjs/core';
import classnames from 'classnames';

class PainLabelForm extends Component {
  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      reset,
      isBtnLoading,
      showBackButton,
      errors
    } = this.props;

    return (
      <form className="label-form" onSubmit={handleSubmit}>
        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.name
          })}
        >
          <label className="pt-label" htmlFor="name">
            Label Name
          </label>
          <Field
            name="name"
            component="input"
            type="text"
            tabIndex="1"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.name
              })
            }}
            autoFocus
          />
          <div className="pt-form-helper-text">{errors.name}</div>
        </div>

        <Field type="hidden" component="input" name="id" />

        <div className="pt-form-group pt-inline">
          <Button
            type="submit"
            intent={Intent.PRIMARY}
            text="Submit"
            tabIndex="2"
            loading={isBtnLoading}
            disabled={pristine || submitting}
          />
          <Button
            onClick={reset}
            text="Reset"
            tabIndex="3"
            disabled={pristine || submitting}
          />
          {showBackButton ? (
            <Link
              to="/labels/?tab=0"
              className="pt-button pt-minimal pt-icon-chevron-left"
              tabIndex="4"
            >
              Back
            </Link>
          ) : (
            ''
          )}
        </div>
      </form>
    );
  }
}

export default reduxForm({
  // This is the form's name and must be unique across the app
  form: 'painLabelForm'
})(PainLabelForm);
