import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actionCreators } from '../../../../actions';

import PainLabelForm from './PainLabelForm';

class PainLabelCreateDialog extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  handleSubmit(values) {
    const { name } = values;
    const label = {
      name,
      label: name,
      value: name
    };

    this.setState({ isBtnLoading: true });
    this.props.onSubmit(label, null);

    if (values && values.name) {
      this.props.dialogClose();
    }
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return (
      <div className="p-t-20 p-l-20">
        <PainLabelForm
          onSubmit={this.handleSubmit.bind(this)}
          isBtnLoading={isBtnLoading}
          showBackButton={false}
          errors={errors}
        />
      </div>
    );
  }
}

PainLabelCreateDialog.propTypes = {
  dialogClose: PropTypes.func.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  onSubmit: (labelData, history) =>
    dispatch(actionCreators.addPainLabel(labelData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PainLabelCreateDialog);
