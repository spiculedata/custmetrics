export default [
  {
    id: '0',
    name: 'Worry',
    value: 'Worry',
    label: 'Worry'
  },
  {
    id: '1',
    name: 'Stress',
    value: 'Stress',
    label: 'Stress'
  },
  {
    id: '2',
    name: 'Legal implications',
    value: 'Legal implications',
    label: 'Legal implications'
  },
  {
    id: '3',
    name: 'Business Not Growing',
    value: 'Business Not Growing',
    label: 'Business Not Growing'
  },
  {
    id: '4',
    name: 'Loss of Sales',
    value: 'Loss of Sales',
    label: 'Loss of Sales'
  },
  {
    id: '5',
    name: 'Poor ROI',
    value: 'Poor ROI',
    label: 'Poor ROI'
  },
  {
    id: '6',
    name: 'Waste Time',
    value: 'Waste Time',
    label: 'Waste Time'
  },
  {
    id: '7',
    name: 'Waste Money',
    value: 'Waste Money',
    label: 'Waste Money'
  },
  {
    id: '8',
    name: 'Low Staff Motivation',
    value: 'Low Staff Motivation',
    label: 'Low Staff Motivation'
  },
  {
    id: '9',
    name: 'Expensive',
    value: 'Expensive',
    label: 'Expensive'
  },
  {
    id: '10',
    name: 'Poor Quality',
    value: 'Poor Quality',
    label: 'Poor Quality'
  },
  {
    id: '11',
    name: 'Inconsistant Product',
    value: 'Inconsistant Product',
    label: 'Inconsistant Product'
  },
  {
    id: '12',
    name: 'Pressure From Above',
    value: 'Pressure From Above',
    label: 'Pressure From Above'
  },
  {
    id: '13',
    name: 'Missed Opitunity',
    value: 'Missed Opitunity',
    label: 'Missed Opitunity'
  },
  {
    id: '14',
    name: 'Lose Credibility',
    value: 'Lose Credibility',
    label: 'Lose Credibility'
  },
  {
    id: '15',
    name: 'Reputational damage',
    value: 'Reputational damage',
    label: 'Reputational damage'
  },
  {
    id: '16',
    name: 'Confusion',
    value: 'Confusion',
    label: 'Confusion'
  },
  {
    id: '17',
    name: 'Due diligence',
    value: 'Due diligence',
    label: 'Due diligence'
  },
  {
    id: '18',
    name: 'Cost',
    value: 'Cost',
    label: 'Cost'
  },
  {
    id: '19',
    name: 'Fines/Penilties',
    value: 'Fines/Penilties',
    label: 'Fines/Penilties'
  },
  {
    id: '20',
    name: 'Legal costs',
    value: 'Legal costs',
    label: 'Legal costs'
  },
  {
    id: '21',
    name: 'Higher Risk',
    value: 'Higher Risk',
    label: 'Higher Risk'
  },
  {
    id: '22',
    name: 'Personal Reputational Damage',
    value: 'Personal Reputational Damage',
    label: 'Personal Reputational Damage'
  },
  {
    id: '23',
    name: 'Escalating cost',
    value: 'Escalating cost',
    label: 'Escalating cost'
  },
  {
    id: '24',
    name: 'Value for money',
    value: 'Value for money',
    label: 'Value for money'
  }
];
