export default [
  {
    id: '0',
    name: 'Save Time',
    value: 'Save Time',
    label: 'Save Time'
  },
  {
    id: '1',
    name: 'Save Money',
    value: 'Save Money',
    label: 'Save Money'
  },
  {
    id: '2',
    name: 'Efficiency',
    value: 'Efficiency',
    label: 'Efficiency'
  },
  {
    id: '3',
    name: 'Positive Relationship',
    value: 'Positive Relationship',
    label: 'Positive Relationship'
  },
  {
    id: '4',
    name: 'More Profit',
    value: 'More Profit',
    label: 'More Profit'
  },
  {
    id: '5',
    name: 'Business Growth',
    value: 'Business Growth',
    label: 'Business Growth'
  },
  {
    id: '6',
    name: 'More time',
    value: 'More time',
    label: 'More time'
  },
  {
    id: '7',
    name: 'More Money',
    value: 'More Money',
    label: 'More Money'
  },
  {
    id: '8',
    name: 'Motivated staff',
    value: 'Motivated staff',
    label: 'Motivated staff'
  },
  {
    id: '9',
    name: 'Better ROI',
    value: 'Better ROI',
    label: 'Better ROI'
  },
  {
    id: '10',
    name: 'Oppitunities',
    value: 'Oppitunities',
    label: 'Oppitunities'
  },
  {
    id: '11',
    name: 'Promotion',
    value: 'Promotion',
    label: 'Promotion'
  },
  {
    id: '12',
    name: 'Credibiltiy',
    value: 'Credibiltiy',
    label: 'Credibiltiy'
  },
  {
    id: '13',
    name: 'Better Reputation',
    value: 'Better Reputation',
    label: 'Better Reputation'
  },
  {
    id: '14',
    name: 'Reduced Stress',
    value: 'Reduced Stress',
    label: 'Reduced Stress'
  },
  {
    id: '15',
    name: 'Business Growth',
    value: 'Business Growth',
    label: 'Business Growth'
  },
  {
    id: '16',
    name: 'Confidence',
    value: 'Confidence',
    label: 'Confidence'
  },
  {
    id: '17',
    name: 'Clarity',
    value: 'Clarity',
    label: 'Clarity'
  },
  {
    id: '18',
    name: 'Insight',
    value: 'Insight',
    label: 'Insight'
  },
  {
    id: '19',
    name: 'Make informed decisions',
    value: 'Make informed decisions',
    label: 'Make informed decisions'
  }
];
