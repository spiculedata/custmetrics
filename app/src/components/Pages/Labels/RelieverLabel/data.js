export default [
  {
    id: '0',
    name: 'Experience',
    value: 'Experience',
    label: 'Experience'
  },
  {
    id: '1',
    name: 'Authority within industry',
    value: 'Authority within industry',
    label: 'Authority within industry'
  },
  {
    id: '2',
    name: 'Case Studies',
    value: 'Case Studies',
    label: 'Case Studies'
  },
  {
    id: '3',
    name: 'Testimionals',
    value: 'Testimionals',
    label: 'Testimionals'
  },
  {
    id: '4',
    name: 'Stats',
    value: 'Stats',
    label: 'Stats'
  },
  {
    id: '5',
    name: 'Indepth discovery',
    value: 'Indepth discovery',
    label: 'Indepth discovery'
  },
  {
    id: '6',
    name: 'Audit and report',
    value: 'Audit and report',
    label: 'Audit and report'
  },
  {
    id: '7',
    name: 'Knowledge transfer',
    value: 'Knowledge transfer',
    label: 'Knowledge transfer'
  },
  {
    id: '8',
    name: 'Value for money',
    value: 'Value for money',
    label: 'Value for money'
  },
  {
    id: '9',
    name: 'Competitively priced',
    value: 'Competitively priced',
    label: 'Competitively priced'
  },
  {
    id: '10',
    name: 'Demo',
    value: 'Demo',
    label: 'Demo'
  },
  {
    id: '11',
    name: 'Clarity on costs',
    value: 'Clarity on costs',
    label: 'Clarity on costs'
  }
];
