import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Alert, Button, Intent, NonIdealState } from '@blueprintjs/core';
import ReactTable from 'react-table';
import BlockUi from 'react-block-ui';

import { actionCreators } from '../../../../actions';

import Loader from '../../../UI/Loader';

class RelieverLabelList extends Component {
  state = {
    labelId: '',
    relieversLabels: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false,
    isOpenDeleteAlertError: false,
    isBlockingUi: false,
    errors: {}
  };

  componentWillMount() {
    const { relieversLabels } = this.props;

    this.setState({ relieversLabels });
  }

  componentDidMount() {
    this.props.getRelieversLabels();
  }

  componentWillReceiveProps(nextProps) {
    const relieversLabelsState = this.state.relieversLabels;
    const relieversLabelsNextProps = nextProps.relieversLabels;
    const { errors } = nextProps;

    if (!_.isEqual(relieversLabelsState, relieversLabelsNextProps)) {
      this.setState({ relieversLabels: relieversLabelsNextProps });
    }

    if (!_.isEmpty(errors) && this.state.isBlockingUi) {
      this.setState({
        isOpenDeleteAlertError: true,
        isBlockingUi: false,
        errors
      });
    }

    if (!nextProps.loading && this.state.isBlockingUi) {
      this.setState({ isBlockingUi: false });
    }
  }

  onFilter() {
    const { relieversLabels } = this.props;
    const filter = this.filter.value;

    let relieversLabelsFilter = relieversLabels;

    relieversLabelsFilter = relieversLabels.filter(
      value => value.name.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );

    this.setState({
      relieversLabels: relieversLabelsFilter
    });
  }

  handleCancelAlertDelete() {
    this.setState({
      labelId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: false
    });
  }

  handleConfirmAlertDelete() {
    const { labelId } = this.state;

    this.props.onClickDelete(labelId);

    this.setState({
      labelId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: true
    });
  }

  handleErrorClose() {
    this.props.clearErrors();
    this.props.getRelieversLabels();

    this.setState({
      isOpenDeleteAlertError: false,
      isBlockingUi: false,
      errors: {}
    });
  }

  render() {
    const {
      relieversLabels,
      itemNameToDelete,
      isOpenAlertDelete,
      isOpenDeleteAlertError,
      isBlockingUi,
      errors
    } = this.state;
    const btnCreateRelieverLabel = (
      <Link
        to="/label/reliever/new"
        className="pt-button pt-minimal pt-icon-add pt-intent-primary"
      >
        Create Reliever Label
      </Link>
    );

    return this.props.loading ? (
      <Loader />
    ) : (
      <div>
        {this.props.relieversLabels.length === 0 ? (
          <NonIdealState
            visual="label"
            title="No reliever label created"
            description="Create a new reliever label:"
            className="m-t-20"
            action={btnCreateRelieverLabel}
          />
        ) : (
          <div>
            <div className="pt-form-group form-group-search">
              <div className="pt-input-group m-r-10" style={{ width: '50%' }}>
                <span className="pt-icon pt-icon-search" />
                <input
                  type="text"
                  ref={input => (this.filter = input)}
                  onChange={() => this.onFilter()}
                  className="pt-input"
                  placeholder="Search Reliever Label"
                />
              </div>

              <div className="m-r-30">{btnCreateRelieverLabel}</div>
            </div>

            <BlockUi tag="div" blocking={isBlockingUi}>
              <ReactTable
                data={relieversLabels}
                columns={[
                  {
                    Header: 'Info',
                    columns: [
                      {
                        Header: 'Label Name',
                        accessor: 'name',
                        headerClassName: ['text-left', 'bold']
                      }
                    ]
                  },
                  {
                    Header: 'Actions',
                    columns: [
                      {
                        Header: 'Edit',
                        headerClassName: 'bold',
                        width: 80,
                        className: 'text-center',
                        sortable: false,
                        resizable: false,
                        Cell: d => (
                          <Link
                            to={`/label/reliever/${d.original._id}`}
                            className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                            title="Edit"
                          />
                        )
                      },
                      {
                        Header: 'Delete',
                        headerClassName: 'bold',
                        width: 80,
                        className: 'text-center',
                        sortable: false,
                        resizable: false,
                        Cell: d => (
                          <Button
                            icon="trash"
                            intent={Intent.DANGER}
                            title="Delete"
                            minimal
                            onClick={() =>
                              this.setState({
                                labelId: d.original._id,
                                itemNameToDelete: d.original.name,
                                isOpenAlertDelete: true
                              })
                            }
                          />
                        )
                      }
                    ]
                  }
                ]}
                defaultPageSize={20}
                style={{
                  height: '90vh'
                }}
                className="-striped -highlight"
              />
            </BlockUi>

            <Alert
              canEscapeKeyCancel={true}
              canOutsideClickCancel={true}
              confirmButtonText="Close"
              icon="error"
              intent={Intent.DANGER}
              isOpen={isOpenDeleteAlertError}
              onClose={this.handleErrorClose.bind(this)}
            >
              <p>{errors.noRelieverLabelFound}</p>
            </Alert>

            <Alert
              canEscapeKeyCancel={true}
              canOutsideClickCancel={true}
              cancelButtonText="Cancel"
              confirmButtonText="Delete"
              icon="trash"
              intent={Intent.DANGER}
              isOpen={isOpenAlertDelete}
              onCancel={this.handleCancelAlertDelete.bind(this)}
              onConfirm={this.handleConfirmAlertDelete.bind(this)}
            >
              <p>
                Are you sure that you want to delete the{' '}
                <b>{itemNameToDelete}</b> label?
              </p>
            </Alert>
          </div>
        )}
      </div>
    );
  }
}

RelieverLabelList.propTypes = {
  relieversLabels: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getRelieversLabels: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.relieversLabels,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getRelieversLabels: () => dispatch(actionCreators.getRelieversLabels()),
  onClickDelete: id => dispatch(actionCreators.deleteRelieverLabel(id)),
  clearErrors: () => dispatch(actionCreators.clearErrors())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RelieverLabelList);
