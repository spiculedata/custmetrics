import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import uuid from 'uuid';
import _ from 'lodash';
import {
  Alert,
  Button,
  Intent,
  NonIdealState,
  Position,
  Toaster
} from '@blueprintjs/core';
import ReactTable from 'react-table';

import { actionCreators } from '../../../actions';

import Loading from '../../UI/Loading';

class NotesList extends Component {
  state = {
    companyId: '',
    notesId: '',
    notes: [],
    notesPerCompany: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false
  };

  componentWillMount() {
    const { companyId, notes } = this.props;
    const notesFilter = notes.filter(
      value => value.companyId === companyId && !value.template
    );
    const notesPerCompany = notesFilter;

    this.setState({ companyId, notes: notesFilter, notesPerCompany });
  }

  componentWillReceiveProps(nextProps) {
    const companyIdState = this.state.companyId;
    const companyIdNextProps = nextProps.companyId;

    const notesState = this.state.notes;
    const notesNextProps = nextProps.notes;

    if (
      !_.isEqual(companyIdState, companyIdNextProps) ||
      !_.isEqual(notesState, notesNextProps)
    ) {
      const notesFilter = notesNextProps.filter(
        value => value.companyId === companyIdNextProps && !value.template
      );
      const notesPerCompany = notesFilter;

      this.setState({
        companyId: companyIdNextProps,
        notes: notesFilter,
        notesPerCompany
      });
    }
  }

  onFilter() {
    const { notesPerCompany } = this.state;
    const filter = this.filter.value;

    let notesFilter = notesPerCompany;

    notesFilter = notesPerCompany.filter(
      value => value.title.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );

    this.setState({
      notes: notesFilter
    });
  }

  handleCancelAlertDelete() {
    this.setState({
      notesId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false
    });
  }

  handleConfirmAlertDelete() {
    const { notesId } = this.state;
    const toaster = Toaster.create({
      position: Position.TOP_RIGHT
    });
    const toasterConfig = {
      icon: 'trash',
      intent: Intent.DANGER,
      message: 'Deleted!',
      timeout: 2000
    };

    this.props.onClickDelete(notesId);

    toaster.show(toasterConfig);

    this.setState({
      notesId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false
    });
  }

  render() {
    const {
      companyId,
      notes,
      notesPerCompany,
      itemNameToDelete,
      isOpenAlertDelete
    } = this.state;
    const btnCreateNotesTree = (
      <Link
        to={`/company/${companyId}/notes/${uuid.v4()}/new`}
        className="pt-button pt-minimal pt-icon-add pt-intent-primary"
      >
        Create Notes Tree
      </Link>
    );

    return this.props.isFetching ? (
      <div>
        <Loading small />
      </div>
    ) : (
      <div>
        {notesPerCompany.length === 0 ? (
          <NonIdealState
            visual="comment"
            title="No notes tree created"
            description="Create a new notes tree:"
            className="m-t-20"
            action={btnCreateNotesTree}
          />
        ) : (
          <div>
            <div className="pt-form-group form-group-search m-t-20">
              <div className="pt-input-group m-r-10" style={{ width: '50%' }}>
                <span className="pt-icon pt-icon-search" />
                <input
                  type="text"
                  ref={input => (this.filter = input)}
                  onChange={() => this.onFilter()}
                  className="pt-input"
                  placeholder="Search Notes Tree"
                />
              </div>

              <div className="m-r-30">{btnCreateNotesTree}</div>
            </div>

            <ReactTable
              data={notes}
              columns={[
                {
                  Header: 'Info',
                  columns: [
                    {
                      Header: 'Title',
                      accessor: 'title',
                      headerClassName: ['text-left', 'bold']
                    }
                  ]
                },
                {
                  Header: 'Actions',
                  columns: [
                    {
                      Header: 'View',
                      headerClassName: 'bold',
                      width: 80,
                      className: 'text-center',
                      sortable: false,
                      resizable: false,
                      Cell: d => (
                        <Link
                          to={`/company/${d.original.companyId}/notes/view/${
                            d.original.id
                          }`}
                          className="pt-button pt-minimal pt-icon-eye-open pt-intent-primary"
                          title="View"
                        />
                      )
                    },
                    {
                      Header: 'Edit',
                      headerClassName: 'bold',
                      width: 80,
                      className: 'text-center',
                      sortable: false,
                      resizable: false,
                      Cell: d => (
                        <Link
                          to={`/company/${d.original.companyId}/notes/${
                            d.original.id
                          }`}
                          className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                          title="Edit"
                        />
                      )
                    },
                    {
                      Header: 'Delete',
                      headerClassName: 'bold',
                      width: 80,
                      className: 'text-center',
                      sortable: false,
                      resizable: false,
                      Cell: d => (
                        <Button
                          icon="trash"
                          intent={Intent.DANGER}
                          title="Delete"
                          minimal
                          onClick={() =>
                            this.setState({
                              notesId: d.original.id,
                              itemNameToDelete: d.original.title,
                              isOpenAlertDelete: true
                            })
                          }
                        />
                      )
                    }
                  ]
                }
              ]}
              defaultPageSize={20}
              style={{
                height: '90vh'
              }}
              className="-striped -highlight"
            />

            <Alert
              canEscapeKeyCancel={true}
              canOutsideClickCancel={true}
              cancelButtonText="Cancel"
              confirmButtonText="Delete"
              icon="trash"
              intent={Intent.DANGER}
              isOpen={isOpenAlertDelete}
              onCancel={this.handleCancelAlertDelete.bind(this)}
              onConfirm={this.handleConfirmAlertDelete.bind(this)}
            >
              <p>
                Are you sure that you want to delete the{' '}
                <b>{itemNameToDelete}</b> notes tree?
              </p>
            </Alert>
          </div>
        )}
      </div>
    );
  }
}

NotesList.propTypes = {
  companyId: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  ...state.notes
});

const mapDispatchToProps = dispatch => ({
  onClickDelete: id => dispatch(actionCreators.deleteNotes(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NotesList));
