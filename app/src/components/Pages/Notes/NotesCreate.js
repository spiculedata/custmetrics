import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import SortableTree, {
  addNodeUnderParent,
  changeNodeAtPath,
  removeNodeAtPath
} from 'react-sortable-tree';
import {
  Alert,
  Button,
  ButtonGroup,
  Dialog,
  EditableText,
  Icon,
  Menu,
  MenuDivider,
  MenuItem,
  Intent,
  Popover,
  Position
} from '@blueprintjs/core';
import Select from 'react-select';
import Mousetrap from 'mousetrap';
import mouseTrap from 'react-mousetrap';
import Picky from 'react-picky';
import tinycolor from 'tinycolor2';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';

import { TagCreateDialog } from '../Tag';

const styleBtnGroup = {
  position: 'absolute',
  right: '20px',
  bottom: '20px'
};

class NotesCreate extends Component {
  state = {
    companyId: '',
    notesId: '',
    tagId: '',
    tag: [],
    tags: [],
    title: '',
    template: false,
    treeData: [],
    notesTemplates: [],
    selectedNotesTemplate: '',
    createdNotes: false,
    isOpenDialogCreateTag: false,
    isOpenTitleAlertError: false,
    isOpenTagAlertError: false
  };

  componentWillMount() {
    const { companyId, notesId } = this.props.match.params;
    const { tags } = this.props;

    this.setState({ companyId, notesId, tags });

    // Disable the shortcuts when the focus is on input fields
    Mousetrap.prototype.stopCallback = (e, element, combo) => {
      // If call button Save & Preview
      if (combo === 'mod+s') {
        return false;
      }

      // stop for input, select, and textarea
      return (
        element.tagName === 'INPUT' ||
        element.tagName === 'SELECT' ||
        element.tagName === 'TEXTAREA' ||
        (element.contentEditable && element.contentEditable === 'true')
      );
    };

    this.props.bindShortcut('mod+s', () => {
      // 'mod+s' is a helper to ['command+s', 'ctrl+s']

      this.handleSave();

      // return false to prevent default browser behavior
      // and stop event from bubbling
      return false;
    });
  }

  componentDidMount() {
    this.props.getNotesTemplates();
    this.props.getTags();
  }

  componentWillReceiveProps(nextProps) {
    const notesIdState = this.state.notesId;
    const notesIdNextProps = nextProps.match.params.notesId;

    const notesTemplatesState = this.state.notesTemplates;
    const notesTemplatesNextProps = nextProps.notesTemplates;

    const tagsState = this.state.tags;
    const tagsNextProps = nextProps.tags;

    if (!_.isEqual(notesIdState, notesIdNextProps)) {
      this.setState({
        notesId: notesIdNextProps,
        tagId: '',
        tag: [],
        title: '',
        template: false,
        treeData: [],
        selectedNotesTemplate: '',
        createdNotes: false,
        isOpenDialogCreateTag: false,
        isOpenTitleAlertError: false,
        isOpenTagAlertError: false
      });
    }

    if (!_.isEqual(notesTemplatesState, notesTemplatesNextProps)) {
      this.setState({ notesTemplates: notesTemplatesNextProps });
    }

    if (!_.isEqual(tagsState, tagsNextProps)) {
      this.setState({ tags: tagsNextProps });
    }
  }

  handleSave(template = false, type = 'notes') {
    const {
      companyId,
      notesId,
      tagId,
      title,
      treeData,
      createdNotes
    } = this.state;

    if (title === '' || title.trim() === '') {
      this.setState({ isOpenTitleAlertError: true });
    } else if (tagId === '' && !template) {
      this.setState({ isOpenTagAlertError: true });
    } else {
      if (createdNotes) {
        this.props.onClickEdit(notesId, {
          companyId,
          notesId,
          tagId,
          title,
          template,
          type,
          treeData
        });

        if (template) {
          this.props.history.push(`/notes_template/${notesId}`);
        }
      } else {
        this.setState({ createdNotes: true });
        this.props.onClickCreate({
          companyId,
          notesId,
          tagId,
          title,
          template,
          type,
          treeData
        });

        if (template) {
          this.props.history.push(`/notes_template/${notesId}`);
        }
      }
    }
  }

  handleAddMenuClick(event) {
    const textContent = event.target.innerText;

    switch (textContent) {
      case 'Save Notes':
        this.handleSave();
        break;
      case 'Save as Template':
        this.handleSave(true);
        break;
      default:
        return false;
    }
  }

  handleSelectNotesTemplate(template) {
    if (template) {
      this.setState({
        treeData: template.treeData,
        selectedNotesTemplate: template
      });
    } else {
      this.setState({ selectedNotesTemplate: template });
    }
  }

  handleSelectTag(tag) {
    this.setState({ tagId: tag._id, tag });
  }

  handleCloseDialogCreateTag() {
    this.setState({
      isOpenDialogCreateTag: false
    });
  }

  handleErrorClose() {
    this.setState({
      isOpenTitleAlertError: false,
      isOpenTagAlertError: false
    });
  }

  renderDialogCreateTag() {
    const { isOpenDialogCreateTag } = this.state;

    return (
      <Dialog
        autoFocus={true}
        canEscapeKeyClose={true}
        canOutsideClickClose={true}
        enforceFocus={true}
        icon="tag"
        isOpen={isOpenDialogCreateTag}
        onClose={this.handleCloseDialogCreateTag.bind(this)}
        title="Create Tag"
        usePortal={true}
      >
        <TagCreateDialog
          dialogClose={this.handleCloseDialogCreateTag.bind(this)}
        />
      </Dialog>
    );
  }

  render() {
    const {
      tag,
      tags,
      title,
      notesTemplates,
      selectedNotesTemplate,
      isOpenTitleAlertError,
      isOpenTagAlertError
    } = this.state;
    const defaultNodeName = _.uniqueId('new_node_');
    const getNodeKey = ({ treeIndex }) => treeIndex;

    const saveMenu = (
      <Menu>
        <MenuItem
          text="Save Notes"
          label="Ctrl+S"
          onClick={this.handleAddMenuClick.bind(this)}
          style={{ fontWeight: '600' }}
        />
        <MenuItem
          text="Save as Template"
          onClick={this.handleAddMenuClick.bind(this)}
        />
        <MenuDivider />
        <MenuItem text="Exit" onClick={this.handleAddMenuClick.bind(this)} />
      </Menu>
    );

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading text="Loading..." center small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner no-padding-right">
          <div className="group-title-tags">
            <h4>
              <EditableText
                maxLength={50}
                placeholder="Click to edit title..."
                onChange={title => this.setState({ title })}
                value={title}
              />
            </h4>

            <label className="m-r-50" style={{ width: '300px' }}>
              <span style={{ color: '#182026', fontWeight: '600' }}>
                Notes Templates
              </span>
              <Select
                name="form-field-notes-template"
                valueKey="_id"
                labelKey="title"
                value={selectedNotesTemplate}
                options={notesTemplates}
                placeholder="Select a template"
                onChange={option => this.handleSelectNotesTemplate(option)}
                style={{ marginTop: '10px' }}
              />
            </label>

            <label className="m-r-50" style={{ width: '300px' }}>
              <a
                onClick={event => {
                  event.preventDefault();

                  this.setState({ isOpenDialogCreateTag: true });
                }}
                style={{ color: '#182026', fontWeight: '600' }}
              >
                Tags
                <Icon
                  icon="small-plus"
                  intent={Intent.PRIMARY}
                  title="Create Tag"
                />
              </a>
              <Picky
                options={tags}
                value={tag}
                valueKey="_id"
                labelKey="name"
                numberDisplayed={0}
                dropdownHeight={300}
                multiple={false}
                onChange={this.handleSelectTag.bind(this)}
                includeFilter
                render={({
                  style,
                  isSelected,
                  item,
                  selectValue,
                  labelKey,
                  valueKey,
                  multiple
                }) => {
                  return (
                    <li
                      style={{
                        backgroundColor: item.color ? item.color : '#333333'
                      }}
                      className={isSelected ? 'selected' : ''}
                      key={item._id}
                      onClick={() => selectValue(item)}
                    >
                      <input type="checkbox" checked={isSelected} readOnly />
                      <span
                        style={{
                          color: tinycolor(item.color).isLight()
                            ? '#000000'
                            : '#ffffff'
                        }}
                      >
                        {item.name}
                      </span>
                    </li>
                  );
                }}
              />
            </label>
          </div>

          <div style={{ height: '100%' }}>
            <SortableTree
              treeData={this.state.treeData}
              onChange={treeData => this.setState({ treeData })}
              generateNodeProps={({ node, path }) => ({
                title: (
                  <input
                    className="pt-input"
                    style={{ width: '300px' }}
                    value={node.name}
                    onChange={event => {
                      const name = event.target.value;

                      this.setState(state => ({
                        treeData: changeNodeAtPath({
                          treeData: state.treeData,
                          path,
                          getNodeKey,
                          newNode: { ...node, name, title: name }
                        })
                      }));
                    }}
                  />
                ),
                buttons: [
                  <Button
                    icon="add"
                    intent={Intent.PRIMARY}
                    minimal
                    onClick={() =>
                      this.setState(state => ({
                        treeData: addNodeUnderParent({
                          treeData: state.treeData,
                          parentKey: path[path.length - 1],
                          expandParent: true,
                          getNodeKey,
                          newNode: {
                            name: `${defaultNodeName}`,
                            title: `${defaultNodeName}`
                          }
                        }).treeData
                      }))
                    }
                  />,
                  <Button
                    icon="trash"
                    intent={Intent.DANGER}
                    minimal
                    onClick={() =>
                      this.setState(state => ({
                        treeData: removeNodeAtPath({
                          treeData: state.treeData,
                          path,
                          getNodeKey
                        })
                      }))
                    }
                  />
                ]
              })}
            />
          </div>

          {this.renderDialogCreateTag()}

          <Alert
            canEscapeKeyCancel={true}
            canOutsideClickCancel={true}
            confirmButtonText="Okay"
            intent={Intent.WARNING}
            isOpen={isOpenTitleAlertError}
            onClose={this.handleErrorClose.bind(this)}
          >
            <p>You must create a title for the notes!</p>
          </Alert>

          <Alert
            canEscapeKeyCancel={true}
            canOutsideClickCancel={true}
            confirmButtonText="Okay"
            intent={Intent.WARNING}
            isOpen={isOpenTagAlertError}
            onClose={this.handleErrorClose.bind(this)}
          >
            <p>You must select a tag for the notes!</p>
          </Alert>

          <ButtonGroup style={styleBtnGroup}>
            <Button
              icon="add"
              intent={Intent.PRIMARY}
              text="Add Node"
              onClick={() =>
                this.setState(state => ({
                  treeData: state.treeData.concat({
                    name: `${defaultNodeName}`,
                    title: `${defaultNodeName}`
                  })
                }))
              }
            />
            <Popover content={saveMenu} position={Position.BOTTOM_LEFT}>
              <Button
                icon="floppy-disk"
                intent={Intent.SUCCESS}
                text="Save as..."
              />
            </Popover>
          </ButtonGroup>
        </div>
      </Container>
    );
  }
}

NotesCreate.propTypes = {
  notesTemplates: PropTypes.array.isRequired,
  tags: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  getNotesTemplates: PropTypes.func.isRequired,
  getTags: PropTypes.func.isRequired,
  onClickCreate: PropTypes.func.isRequired,
  onClickEdit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.notesTemplates,
  ...state.tags
});

const mapDispatchToProps = dispatch => ({
  getNotesTemplates: () => dispatch(actionCreators.getNotesTemplates()),
  getTags: () => dispatch(actionCreators.getTags()),
  onClickCreate: notesData => dispatch(actionCreators.addNotes(notesData)),
  onClickEdit: (notesId, notesData) =>
    dispatch(actionCreators.editNotes(notesId, notesData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(mouseTrap(NotesCreate)));
