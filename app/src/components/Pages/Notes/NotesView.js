import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import SortableTree from 'react-sortable-tree';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';

const styleBtnEdit = {
  position: 'absolute',
  right: '20px',
  bottom: '20px'
};

class NotesView extends Component {
  state = {
    companyId: '',
    notesId: '',
    title: '',
    treeData: []
  };

  componentWillMount() {
    const { companyId, notesId } = this.props.match.params;

    this.setState({ companyId, notesId });
  }

  componentDidMount() {
    const { notesId } = this.props.match.params;

    this.props.getNotes(notesId);
  }

  componentWillReceiveProps(nextProps) {
    const notesState = this.state.notes;
    const notesNextProps = nextProps.notes;

    if (!_.isEqual(notesState, notesNextProps)) {
      this.setState(notesNextProps);
    }
  }

  render() {
    const { _id, companyId, title, treeData } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading text="Loading..." center small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner no-padding-right">
          <h4>{title}</h4>

          <div style={{ height: '100%' }}>
            <SortableTree
              treeData={treeData}
              onChange={treeData => this.setState({ treeData })}
              canDrag={false}
            />
          </div>

          <Link
            to={`/company/${companyId}/notes/${_id}`}
            className="pt-button pt-icon-edit pt-intent-success"
            style={styleBtnEdit}
          >
            Edit
          </Link>
        </div>
      </Container>
    );
  }
}

NotesView.propTypes = {
  loading: PropTypes.bool.isRequired,
  getNotes: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.notes
});

const mapDispatchToProps = dispatch => ({
  getNotes: id => dispatch(actionCreators.getNotes(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NotesView));
