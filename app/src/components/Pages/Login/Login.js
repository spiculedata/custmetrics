import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button, FormGroup, Intent, InputGroup } from '@blueprintjs/core';
import PasswordMask from 'react-password-mask';
import classnames from 'classnames';

import { actionCreators } from '../../../actions';

import Empty from '../../Layout/Empty';
import Logo from '../../UI/Logo';

import './Login.css';

class Login extends Component {
  constructor() {
    super();

    this.state = {
      email: '',
      password: '',
      isBtnLoading: false,
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { auth } = this.props;

    if (auth.isAuthenticated) {
      this.handleAuthenticate();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { auth, errors } = nextProps;

    if (auth.isAuthenticated) {
      this.handleAuthenticate();
    }

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  authenticated() {
    const $el = document.getElementById('ipl-progress-indicator');

    $el.removeAttribute('hidden');

    return new Promise(resolve => setTimeout(resolve, 2000));
  }

  handleAuthenticate() {
    const { push } = this.props.history;

    this.authenticated().then(() => {
      const $el = document.getElementById('ipl-progress-indicator');

      push('/companies');

      if ($el) {
        $el.classList.add('available');

        setTimeout(() => {
          $el.setAttribute('hidden', 'hidden');
          $el.classList.remove('available');
        }, 2000);
      }
    });
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const userData = {
      email: this.state.email,
      password: this.state.password
    };

    this.setState({ isBtnLoading: true });

    this.props.doLogin(userData);
  }

  render() {
    const { email, password, isBtnLoading, errors } = this.state;

    return (
      <Empty>
        <div className="login">
          <div className="logo">
            <Logo />
          </div>

          <form onSubmit={this.handleSubmit}>
            <FormGroup
              label="Email"
              labelFor="email"
              intent={errors.email ? Intent.DANGER : Intent.NONE}
              helperText={errors.email ? errors.email : ''}
            >
              <InputGroup
                type="email"
                id="email"
                name="email"
                value={email}
                intent={errors.email ? Intent.DANGER : Intent.NONE}
                onChange={this.handleChange}
              />
            </FormGroup>

            <FormGroup
              label="Password"
              labelFor="password"
              intent={errors.password ? Intent.DANGER : Intent.NONE}
              helperText={errors.password ? errors.password : ''}
            >
              <PasswordMask
                id="password"
                name="password"
                buttonClassName="btn-password-mask"
                inputClassName={classnames('pt-input', {
                  'pt-intent-danger': errors.password
                })}
                buttonStyles={{ marginTop: '-12px' }}
                value={password}
                onChange={this.handleChange}
              />
            </FormGroup>

            <div className="form-group form-group-button">
              <div className="form-group-button-description">
                <Link to="/reset_password">Forgot Password?</Link>
              </div>

              <Button
                type="submit"
                className="button-right"
                intent={Intent.PRIMARY}
                text="Login"
                loading={isBtnLoading}
                large
              />
            </div>
          </form>

          <div className="btn-register">
            <Link to="/register">
              Don't have an account? Click here to sign up!
            </Link>
          </div>
        </div>
      </Empty>
    );
  }
}

Login.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  doLogin: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  doLogin: userData => dispatch(actionCreators.loginUser(userData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
