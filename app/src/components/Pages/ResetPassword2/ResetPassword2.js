import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { NavLink, withRouter } from 'react-router-dom';
import {
  Button,
  Callout,
  FormGroup,
  Intent,
  NonIdealState
} from '@blueprintjs/core';
import _ from 'lodash';
import PasswordMask from 'react-password-mask';
import classnames from 'classnames';

import { actionCreators } from '../../../actions';

import Empty from '../../Layout/Empty';
import Logo from '../../UI/Logo';

class ResetPassword2 extends Component {
  constructor() {
    super();

    this.state = {
      password: '',
      password2: '',
      isBtnLoading: false,
      isResetedPassword: false,
      errors: {}
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    const { token } = this.props.match.params;
    const { auth } = this.props;

    if (auth.isAuthenticated) {
      this.handleAuthenticate();
    }

    this.props.checkToken(token);
  }

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, isResetedPassword: false, errors });
    }

    if (
      !_.isEmpty(nextProps.location.search) &&
      nextProps.location.search === '?reseted=true'
    ) {
      this.setState({
        isResetedPassword: true
      });
    }
  }

  authenticated() {
    const $el = document.getElementById('ipl-progress-indicator');

    $el.removeAttribute('hidden');

    return new Promise(resolve => setTimeout(resolve, 2000));
  }

  handleAuthenticate() {
    const { push } = this.props.history;

    this.authenticated().then(() => {
      const $el = document.getElementById('ipl-progress-indicator');

      push('/companies');

      if ($el) {
        $el.classList.add('available');

        setTimeout(() => {
          $el.setAttribute('hidden', 'hidden');
          $el.classList.remove('available');
        }, 2000);
      }
    });
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();

    const { token } = this.props.match.params;
    const { password, password2 } = this.state;

    let newPassword = this.props.auth.user || {};

    this.setState({ isBtnLoading: true });

    if (!_.isEmpty(newPassword)) {
      newPassword.password = password;
      newPassword.password2 = password2;
    } else {
      newPassword = {
        password,
        password2
      };
    }

    this.props.resetPassword2(token, newPassword, this.props.history);
  }

  render() {
    const {
      password,
      password2,
      isBtnLoading,
      isResetedPassword,
      errors
    } = this.state;

    return errors && errors.noTokenFound ? (
      <Empty>
        <div className="page-empty-container">
          <NonIdealState visual="error" title={errors.noTokenFound} />

          <div className="form-group form-group-button">
            <div className="form-group-button-description">
              <NavLink to="/">← Back to login</NavLink>
            </div>
          </div>
        </div>
      </Empty>
    ) : (
      <Empty>
        <div className="login">
          <div className="logo">
            <Logo />
          </div>

          <form onSubmit={this.handleSubmit}>
            {isResetedPassword ? (
              <div>
                <Callout
                  icon="tick"
                  intent={Intent.SUCCESS}
                  title="Success!"
                  style={{ marginBottom: '20px' }}
                >
                  <div>
                    <p>Password changed successfully!</p>
                  </div>
                </Callout>

                <div className="form-group form-group-button">
                  <div className="form-group-button-description">
                    <NavLink to="/">← Back to login</NavLink>
                  </div>
                </div>
              </div>
            ) : (
              <div>
                <FormGroup
                  label="New Password"
                  labelFor="password"
                  intent={errors.password ? Intent.DANGER : Intent.NONE}
                  helperText={errors.password ? errors.password : ''}
                >
                  <PasswordMask
                    id="password"
                    name="password"
                    buttonClassName="btn-password-mask"
                    inputClassName={classnames('pt-input', {
                      'pt-intent-danger': errors.password
                    })}
                    buttonStyles={{ marginTop: '-12px' }}
                    value={password}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <FormGroup
                  label="Confirm New Password"
                  labelFor="password2"
                  intent={errors.password2 ? Intent.DANGER : Intent.NONE}
                  helperText={errors.password2 ? errors.password2 : ''}
                >
                  <PasswordMask
                    id="password2"
                    name="password2"
                    buttonClassName="btn-password-mask"
                    inputClassName={classnames('pt-input', {
                      'pt-intent-danger': errors.password2
                    })}
                    buttonStyles={{ marginTop: '-12px' }}
                    value={password2}
                    onChange={this.handleChange}
                  />
                </FormGroup>

                <div className="form-group form-group-button">
                  <div className="form-group-button-description">
                    <NavLink to="/">← Back to login</NavLink>
                  </div>

                  <Button
                    type="submit"
                    className="button-right"
                    intent={Intent.PRIMARY}
                    text="Reset Password"
                    loading={isBtnLoading}
                    large
                  />
                </div>
              </div>
            )}
          </form>
        </div>
      </Empty>
    );
  }
}

ResetPassword2.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  checkToken: PropTypes.func.isRequired,
  resetPassword2: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  checkToken: token => dispatch(actionCreators.checkToken(token)),
  resetPassword2: (token, newPasswordData, history) =>
    dispatch(actionCreators.resetPassword2(token, newPasswordData, history))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ResetPassword2));
