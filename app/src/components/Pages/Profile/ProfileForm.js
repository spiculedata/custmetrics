import React, { Component } from 'react';
import { reduxForm, Field } from 'redux-form';
import { Button, Intent } from '@blueprintjs/core';
import classnames from 'classnames';

class ProfileForm extends Component {
  render() {
    const {
      handleSubmit,
      pristine,
      submitting,
      reset,
      isBtnLoading,
      errors
    } = this.props;

    return (
      <form className="profile-form" onSubmit={handleSubmit}>
        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.name
          })}
        >
          <label className="pt-label" htmlFor="name">
            Name
          </label>
          <Field
            name="name"
            component="input"
            type="text"
            tabIndex="1"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.name
              })
            }}
            autoFocus
          />
          <div className="pt-form-helper-text">{errors.name}</div>
        </div>

        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.email
          })}
        >
          <label className="pt-label" htmlFor="email">
            Email
          </label>
          <Field
            name="email"
            component="input"
            type="email"
            tabIndex="2"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.email
              })
            }}
          />
          <div className="pt-form-helper-text">{errors.email}</div>
        </div>

        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.password
          })}
        >
          <label className="pt-label" htmlFor="password">
            New password
          </label>
          <Field
            name="password"
            component="input"
            type="password"
            tabIndex="3"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.password
              })
            }}
          />
          <div className="pt-form-helper-text">{errors.password}</div>
        </div>

        <div
          className={classnames('pt-form-group', {
            'pt-intent-danger': errors.password2
          })}
        >
          <label className="pt-label" htmlFor="password2">
            Confirm Password
          </label>
          <Field
            name="password2"
            component="input"
            type="password"
            tabIndex="4"
            props={{
              className: classnames('pt-input', {
                'pt-intent-danger': errors.password2
              })
            }}
          />
          <div className="pt-form-helper-text">{errors.password2}</div>
        </div>

        <Field type="hidden" component="input" name="id" />

        <div className="pt-form-group pt-inline">
          <Button
            type="submit"
            intent={Intent.PRIMARY}
            text="Submit"
            tabIndex="5"
            loading={isBtnLoading}
            disabled={pristine || submitting}
          />
          <Button
            onClick={reset}
            text="Reset"
            tabIndex="6"
            disabled={pristine || submitting}
          />
        </div>
      </form>
    );
  }
}

export default reduxForm({
  // This is the form's name and must be unique across the app
  form: 'profileForm'
})(ProfileForm);
