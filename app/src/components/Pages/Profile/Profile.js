import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import ProfileForm from './ProfileForm';

import './Profile.css';

class Profile extends Component {
  state = {
    isBtnLoading: false,
    errors: {}
  };

  componentWillReceiveProps(nextProps) {
    const { errors } = nextProps;

    if (errors) {
      this.setState({ isBtnLoading: false, errors });
    }
  }

  handleSubmit(values) {
    const userId = values.id;
    const userEmail = this.props.auth.user.email;

    this.setState({ isBtnLoading: true });
    this.props.onSubmit(userId, userEmail, values);
  }

  render() {
    const { isBtnLoading, errors } = this.state;

    return (
      <Container>
        <div className="content-inner">
          <ProfileForm
            onSubmit={this.handleSubmit.bind(this)}
            initialValues={this.props.auth.user}
            isBtnLoading={isBtnLoading}
            errors={errors}
          />
        </div>
      </Container>
    );
  }
}

Profile.propTypes = {
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired,
  onSubmit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  onSubmit: (userId, userEmail, profileData) =>
    dispatch(actionCreators.editProfile(userId, userEmail, profileData))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
