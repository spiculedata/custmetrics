import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import uuid from 'uuid';
import _ from 'lodash';
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody
} from 'react-accessible-accordion';
import { Button, Dialog, Icon, NonIdealState } from '@blueprintjs/core';
import Select, { Creatable } from 'react-select';

import { actionCreators } from '../../../../actions';

import { PainLabelCreateDialog } from '../../Labels/PainLabel';
import { RelieverLabelCreateDialog } from '../../Labels/RelieverLabel';
import { GainLabelCreateDialog } from '../../Labels/GainLabel';

const styleTable = {
  tableLayout: 'fixed'
};

const styleActionColumn = {
  width: '100px',
  textAlign: 'center'
};

class ListTable extends Component {
  state = {
    items: [],
    dataList: [],
    painsLabels: [],
    relieversLabels: [],
    gainsLabels: [],
    isOpenDialog: false,
    dialogTitle: '',
    dialogRender: ''
  };

  componentWillMount() {
    const {
      items,
      dataList,
      painsLabels,
      relieversLabels,
      gainsLabels
    } = this.props;

    this.setState({
      items,
      dataList,
      painsLabels,
      relieversLabels,
      gainsLabels
    });
  }

  componentDidMount() {
    this.props.getPainsLabels();
    this.props.getRelieversLabels();
    this.props.getGainsLabels();
  }

  componentWillReceiveProps(nextProps) {
    const itemsState = this.state.items;
    const itemsNextProps = nextProps.items;

    const dataListState = this.state.dataList;
    const dataListNextProps = nextProps.dataList;

    const painsLabelsState = this.state.painsLabels;
    const painsLabelsNextProps = nextProps.painsLabels;

    const relieversLabelsState = this.state.relieversLabels;
    const relieversLabelsNextProps = nextProps.relieversLabels;

    const gainsLabelsState = this.state.gainsLabels;
    const gainsLabelsNextProps = nextProps.gainsLabels;

    if (!_.isEqual(itemsState, itemsNextProps)) {
      this.setState({ items: itemsNextProps });
    }

    if (!_.isEqual(dataListState, dataListNextProps)) {
      this.setState({ dataList: dataListNextProps });
    }

    if (!_.isEqual(painsLabelsState, painsLabelsNextProps)) {
      this.setState({ painsLabels: painsLabelsNextProps });
    }

    if (!_.isEqual(relieversLabelsState, relieversLabelsNextProps)) {
      this.setState({ relieversLabels: relieversLabelsNextProps });
    }

    if (!_.isEqual(gainsLabelsState, gainsLabelsNextProps)) {
      this.setState({ gainsLabels: gainsLabelsNextProps });
    }
  }

  addRowData(index) {
    const { dataList } = this.state;
    const selfDataList = dataList[index];
    const data = {
      id: uuid.v4(),
      pains: '',
      painsContext: [],
      relievers: '',
      relieversContext: [],
      gains: '',
      gainsContext: []
    };

    selfDataList.data.push(data);

    const newDataList = [
      ...dataList.slice(0, index),
      selfDataList,
      ...dataList.slice(index + 1)
    ];

    this.setState({
      dataList: newDataList
    });

    const { onListTableComplete } = this.props;

    if (typeof onListTableComplete === 'function') {
      onListTableComplete({
        action: 'ADD',
        data: newDataList
      });
    }
  }

  handleChange(type, option, indexTable, indexData) {
    const { dataList } = this.state;
    const { onListTableComplete } = this.props;

    if (type === 'PAINS') {
      dataList[indexTable].data[indexData].pains = option ? option.value : '';

      this.setState({
        dataList
      });

      if (typeof onListTableComplete === 'function') {
        onListTableComplete({
          action: 'ADD',
          data: dataList
        });
      }
    } else if (type === 'PAINS_CONTEXT') {
      dataList[indexTable].data[indexData].painsContext = [];

      option.forEach(({ value }) =>
        dataList[indexTable].data[indexData].painsContext.push({
          id: uuid.v4(),
          value,
          label: value
        })
      );

      this.setState({
        dataList
      });

      if (typeof onListTableComplete === 'function') {
        onListTableComplete({
          action: 'ADD',
          data: dataList
        });
      }
    }

    if (type === 'RELIEVERS') {
      dataList[indexTable].data[indexData].relievers = option
        ? option.value
        : '';

      this.setState({
        dataList
      });

      if (typeof onListTableComplete === 'function') {
        onListTableComplete({
          action: 'ADD',
          data: dataList
        });
      }
    } else if (type === 'RELIEVERS_CONTEXT') {
      dataList[indexTable].data[indexData].relieversContext = [];

      option.forEach(({ value }) =>
        dataList[indexTable].data[indexData].relieversContext.push({
          id: uuid.v4(),
          value,
          label: value
        })
      );

      this.setState({
        dataList
      });

      if (typeof onListTableComplete === 'function') {
        onListTableComplete({
          action: 'ADD',
          data: dataList
        });
      }
    }

    if (type === 'GAINS') {
      dataList[indexTable].data[indexData].gains = option ? option.value : '';

      this.setState({
        dataList
      });

      if (typeof onListTableComplete === 'function') {
        onListTableComplete({
          action: 'ADD',
          data: dataList
        });
      }
    } else if (type === 'GAINS_CONTEXT') {
      dataList[indexTable].data[indexData].gainsContext = [];

      option.forEach(({ value }) =>
        dataList[indexTable].data[indexData].gainsContext.push({
          id: uuid.v4(),
          value,
          label: value
        })
      );

      this.setState({
        dataList
      });

      if (typeof onListTableComplete === 'function') {
        onListTableComplete({
          action: 'ADD',
          data: dataList
        });
      }
    }
  }

  handleDelete(indexTable, indexData) {
    const { dataList } = this.state;

    const newDataListData = [
      ...dataList[indexTable].data.slice(0, indexData),
      ...dataList[indexTable].data.slice(indexData + 1)
    ];

    dataList[indexTable].data = newDataListData;

    this.setState({
      dataList
    });

    const { onListTableComplete } = this.props;

    if (typeof onListTableComplete === 'function') {
      onListTableComplete({
        action: 'ADD',
        data: dataList
      });
    }
  }

  handleCloseDialog() {
    this.setState({
      isOpenDialog: false
    });
  }

  renderRow(value, indexTable, indexData) {
    const { painsLabels, relieversLabels, gainsLabels } = this.state;

    return (
      <tr key={indexData}>
        <td>
          <Select
            name={`form-field-pains-${value.id}`}
            value={value.pains}
            options={painsLabels}
            placeholder="Select a pain"
            menuStyle={{ backgroundColor: 'rgba(220, 34, 40, 0.4)' }}
            style={{ backgroundColor: 'rgba(220, 34, 40, 0.4)' }}
            onChange={option =>
              this.handleChange('PAINS', option, indexTable, indexData)
            }
          />
        </td>
        <td>
          <Creatable
            name={`form-field-pains-context-${value.id}`}
            value={value.painsContext.map(v => v.value)}
            options={value.painsContext}
            placeholder="Add new context"
            menuStyle={{ backgroundColor: 'rgba(220, 34, 40, 0.4)' }}
            style={{ backgroundColor: 'rgba(220, 34, 40, 0.4)' }}
            onChange={option => {
              const i = option.length - 1;

              if (
                option.length === 0 ||
                (option[i] && option[i].value.trim() !== '')
              ) {
                this.handleChange(
                  'PAINS_CONTEXT',
                  option,
                  indexTable,
                  indexData
                );
              }
            }}
            multi
          />
        </td>
        <td>
          <Select
            name={`form-field-relievers-${value.id}`}
            value={value.relievers}
            options={relieversLabels}
            placeholder="Select a reliever"
            menuStyle={{ backgroundColor: 'rgba(253, 182, 13, 0.4)' }}
            style={{ backgroundColor: 'rgba(253, 182, 13, 0.4)' }}
            onChange={option =>
              this.handleChange('RELIEVERS', option, indexTable, indexData)
            }
          />
        </td>
        <td>
          <Creatable
            name={`form-field-relievers-context-${value.id}`}
            value={value.relieversContext.map(v => v.value)}
            options={value.relieversContext}
            placeholder="Add new context"
            menuStyle={{ backgroundColor: 'rgba(253, 182, 13, 0.4)' }}
            style={{ backgroundColor: 'rgba(253, 182, 13, 0.4)' }}
            onChange={option => {
              const i = option.length - 1;

              if (
                option.length === 0 ||
                (option[i] && option[i].value.trim() !== '')
              ) {
                this.handleChange(
                  'RELIEVERS_CONTEXT',
                  option,
                  indexTable,
                  indexData
                );
              }
            }}
            multi
          />
        </td>
        <td>
          <Select
            name={`form-field-gains-${value.id}`}
            value={value.gains}
            options={gainsLabels}
            placeholder="Select a gain"
            menuStyle={{ backgroundColor: 'rgba(56, 147, 55, 0.4)' }}
            style={{ backgroundColor: 'rgba(56, 147, 55, 0.4)' }}
            onChange={option =>
              this.handleChange('GAINS', option, indexTable, indexData)
            }
          />
        </td>
        <td>
          <Creatable
            name={`form-field-gains-context-${value.id}`}
            value={value.gainsContext.map(v => v.value)}
            options={value.gainsContext}
            placeholder="Add new context"
            menuStyle={{ backgroundColor: 'rgba(56, 147, 55, 0.4)' }}
            style={{ backgroundColor: 'rgba(56, 147, 55, 0.4)' }}
            onChange={option => {
              const i = option.length - 1;

              if (
                option.length === 0 ||
                (option[i] && option[i].value.trim() !== '')
              ) {
                this.handleChange(
                  'GAINS_CONTEXT',
                  option,
                  indexTable,
                  indexData
                );
              }
            }}
            multi
          />
        </td>
        <td style={styleActionColumn}>
          <Button
            icon="cross"
            intent="danger"
            title="Delete"
            onClick={() => this.handleDelete(indexTable, indexData)}
          />
        </td>
      </tr>
    );
  }

  renderTable(index) {
    const { dataList } = this.state;
    const selfDataList = dataList[index];
    const btnCreateData = (
      <Button
        icon="add"
        intent="primary"
        text="Create Data"
        minimal
        onClick={() => this.addRowData(index)}
      />
    );

    return (
      <div>
        <p>{btnCreateData}</p>
        <table className="pt-html-table" style={styleTable}>
          <thead>
            <tr>
              <th>
                <a
                  onClick={() =>
                    this.setState({
                      isOpenDialog: true,
                      dialogTitle: 'Pain',
                      dialogRender: 'pain-form'
                    })
                  }
                  style={{ color: '#182026', fontWeight: '600' }}
                >
                  What Pains?
                  <Icon
                    icon="small-plus"
                    intent="primary"
                    title="Create Pain Label"
                  />
                </a>
              </th>
              <th>Context</th>
              <th>
                <a
                  onClick={() =>
                    this.setState({
                      isOpenDialog: true,
                      dialogTitle: 'Reliever',
                      dialogRender: 'reliever-form'
                    })
                  }
                  style={{ color: '#182026', fontWeight: '600' }}
                >
                  Relievers
                  <Icon
                    icon="small-plus"
                    intent="primary"
                    title="Create Reliever Label"
                  />
                </a>
              </th>
              <th>Context</th>
              <th>
                <a
                  onClick={() =>
                    this.setState({
                      isOpenDialog: true,
                      dialogTitle: 'Gain',
                      dialogRender: 'gain-form'
                    })
                  }
                  style={{ color: '#182026', fontWeight: '600' }}
                >
                  Gains
                  <Icon
                    icon="small-plus"
                    intent="primary"
                    title="Create Gain Label"
                  />
                </a>
              </th>
              <th>Context</th>
              <th style={styleActionColumn}>Action</th>
            </tr>
          </thead>
          <tbody>
            {selfDataList.data.map((value, indexData) =>
              this.renderRow(value, index, indexData)
            )}
          </tbody>
        </table>
      </div>
    );
  }

  renderItem(value, index) {
    return (
      <AccordionItem key={index} uuid={index}>
        <AccordionItemTitle>
          <h6 className="u-position-relative">
            {value}
            <div className="accordion__arrow" role="presentation" />
          </h6>
        </AccordionItemTitle>
        <AccordionItemBody>{this.renderTable(index)}</AccordionItemBody>
      </AccordionItem>
    );
  }

  renderDialog() {
    const { isOpenDialog, dialogTitle, dialogRender } = this.state;
    let labelFormCreate;

    if (dialogRender === 'pain-form') {
      labelFormCreate = (
        <PainLabelCreateDialog
          dialogClose={this.handleCloseDialog.bind(this)}
        />
      );
    } else if (dialogRender === 'reliever-form') {
      labelFormCreate = (
        <RelieverLabelCreateDialog
          dialogClose={this.handleCloseDialog.bind(this)}
        />
      );
    } else {
      labelFormCreate = (
        <GainLabelCreateDialog
          dialogClose={this.handleCloseDialog.bind(this)}
        />
      );
    }

    return (
      <Dialog
        autoFocus={true}
        canEscapeKeyClose={true}
        canOutsideClickClose={true}
        enforceFocus={true}
        icon="label"
        isOpen={isOpenDialog}
        onClose={this.handleCloseDialog.bind(this)}
        title={`Create ${dialogTitle} Label`}
        usePortal={true}
      >
        {labelFormCreate}
      </Dialog>
    );
  }

  render() {
    const { name } = this.props;
    const { items } = this.state;

    return items.length === 0 ? (
      <NonIdealState description={`No ${name.toLowerCase()} created`} />
    ) : (
      <div>
        <Accordion>
          {items.map((value, index) => this.renderItem(value, index))}
        </Accordion>

        {this.renderDialog()}
      </div>
    );
  }
}

ListTable.propTypes = {
  name: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  dataList: PropTypes.array.isRequired,
  painsLabels: PropTypes.array.isRequired,
  relieversLabels: PropTypes.array.isRequired,
  gainsLabels: PropTypes.array.isRequired,
  onListTableComplete: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.painsLabels,
  ...state.relieversLabels,
  ...state.gainsLabels
});

const mapDispatchToProps = dispatch => ({
  getPainsLabels: () => dispatch(actionCreators.getPainsLabels()),
  getRelieversLabels: () => dispatch(actionCreators.getRelieversLabels()),
  getGainsLabels: () => dispatch(actionCreators.getGainsLabels())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ListTable);
