/**
 * @fileoverview dragscroll - scroll area by dragging
 * @version 0.0.8
 *
 * @license MIT, see http://github.com/asvd/dragscroll
 * @copyright 2015 asvd <heliosframework@gmail.com>
 */

// Support Mobile
// Pull Request: https://github.com/asvd/dragscroll/pull/32/files

/* eslint-disable no-loop-func */

function fixTouches(e) {
  if (e.touches) {
    e.clientX = e.touches[0].clientX;
    e.clientY = e.touches[0].clientY;
  }
}

((root, factory) => {
  if (typeof exports !== 'undefined') {
    factory(exports);
  } else {
    factory((root.dragscroll = {}));
  }
})(this, exports => {
  const _window = window;
  const _document = document;
  const mousemove = 'mousemove touchmove';
  const mouseup = 'mouseup touchend';
  const mousedown = 'mousedown touchstart';
  const EventListener = 'EventListener';
  const addEventListener = `add${EventListener}`;
  const removeEventListener = `remove${EventListener}`;
  let newScrollX;
  let newScrollY;

  let dragged = [];
  const reset = (i, el) => {
    for (i = 0; i < dragged.length; ) {
      el = dragged[i++];
      el = el.container || el;
      el[removeEventListener](mousedown, el.md, 0);
      mouseup.split(' ').forEach(ev => {
        _window[removeEventListener](ev, el.mu, 0);
      });
      mousemove.split(' ').forEach(ev => {
        _window[removeEventListener](ev, el.mm, 0);
      });
    }

    // cloning into array since HTMLCollection is updated dynamically
    dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
    for (i = 0; i < dragged.length; ) {
      ((el, lastClientX, lastClientY, pushed, scroller, cont) => {
        mousedown.split(' ').forEach(ev => {
          (cont = el.container || el)[addEventListener](
            ev,
            (cont.md = e => {
              fixTouches(e);
              if (
                !el.hasAttribute('nochilddrag') ||
                _document.elementFromPoint(e.pageX, e.pageY) === cont
              ) {
                pushed = 1;
                lastClientX = e.clientX;
                lastClientY = e.clientY;

                e.preventDefault();
              }
            }),
            0
          );
        });

        mouseup.split(' ').forEach(ev => {
          _window[addEventListener](
            ev,
            (cont.mu = () => {
              pushed = 0;
            }),
            0
          );
        });
        mousemove.split(' ').forEach(ev => {
          _window[addEventListener](
            ev,
            (cont.mm = e => {
              fixTouches(e);
              if (pushed) {
                (scroller = el.scroller || el).scrollLeft -= newScrollX =
                  -lastClientX + (lastClientX = e.clientX);
                scroller.scrollTop -= newScrollY =
                  -lastClientY + (lastClientY = e.clientY);
                if (el === _document.body) {
                  (scroller =
                    _document.documentElement).scrollLeft -= newScrollX;
                  scroller.scrollTop -= newScrollY;
                }
              }
            }),
            0
          );
        });
      })(dragged[i++]);
    }
  };

  if (_document.readyState === 'complete') {
    reset();
  } else {
    _window[addEventListener]('load', reset, 0);
  }

  exports.reset = reset;
});
