export const configs = {
  container: '',
  theme: 'custmetrics',
  mode: 'side',
  support_html: true,
  view: {
    line_width: 5,
    line_color: '#43005a'
  },
  layout: {
    hspace: 500,
    vspace: 20,
    pspace: 13
  }
};
