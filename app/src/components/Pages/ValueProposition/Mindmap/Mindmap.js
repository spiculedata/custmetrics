import React, { Component } from 'react';
import PropTypes from 'prop-types';
import jsmind from 'jsmind2';
import dragscroll from './dragscroll';
import _ from 'lodash';

import { configs } from './configs';

import 'jsmind2/src/css/jsmind2.css';
import './Mindmap.css';

class Mindmap extends Component {
  state = {
    container: '',
    data: []
  };

  componentWillMount() {
    const { container, data } = this.props;

    this.setState({ container, data });
  }

  componentWillReceiveProps(nextProps) {
    const containerState = this.state.container;
    const containerNextProps = nextProps.container;

    const dataState = this.state.data;
    const dataNextProps = nextProps.data;

    if (!_.isEqual(containerState, containerNextProps)) {
      this.setState({ container: containerNextProps });
    }

    if (!_.isEqual(dataState, dataNextProps)) {
      this.setState({ data: dataNextProps });
    }
  }

  async componentDidMount() {
    const { container, data } = this.state;
    const mindMapData = {
      format: 'node_array',
      data
    };

    configs.container = container;

    if (!_.isEmpty(data)) {
      this.mindMap = await jsmind.show(configs, mindMapData);

      await this.forceMindMapZoomOut();

      await document.querySelector('.jsmind-inner').classList.add('dragscroll');

      dragscroll.reset();
    }
  }

  forceMindMapZoomOut() {
    // const $zoomOutButton = document.querySelector('#zoom-out-button');

    for (let i = 0; i < 3; i++) {
      // if ((i + 1) === 3) {
      //   $zoomOutButton.setAttribute('disabled', true);
      // }
      // else {
      // this.mindMap.view.zoomOut();
      // }

      this.mindMap.view.zoomOut();
    }
  }

  zoomButtonActions(event) {
    const textContent = event.target.innerText;
    const $zoomInButton = document.querySelector('#zoom-in-button');
    const $zoomOutButton = document.querySelector('#zoom-out-button');

    if (textContent === '+') {
      if (this.mindMap.view.zoomIn()) {
        $zoomOutButton.removeAttribute('disabled');
      } else {
        $zoomInButton.setAttribute('disabled', true);
      }
    } else {
      if (this.mindMap.view.zoomOut()) {
        $zoomInButton.removeAttribute('disabled');
      } else {
        $zoomOutButton.setAttribute('disabled', true);
      }
    }
  }

  render() {
    const { container } = this.state;

    return (
      <div>
        <div className="container-mindmap" id={container} />

        <div className="btn-group-zoom">
          <button
            id="zoom-in-button"
            title="Zoom in"
            onClick={this.zoomButtonActions.bind(this)}
          >
            +
          </button>

          <button
            id="zoom-out-button"
            title="Zoom out"
            onClick={this.zoomButtonActions.bind(this)}
          >
            -
          </button>
        </div>
      </div>
    );
  }
}

Mindmap.propTypes = {
  container: PropTypes.string.isRequired,
  data: PropTypes.array.isRequired
};

export default Mindmap;
