import uuid from 'uuid';
import _ from 'lodash';

const getDataName = (list, propName) => {
  const data = [];

  for (let x = 0; x < list.length; x++) {
    for (let y = 0; y < list[x].data.length; y++) {
      data.push(list[x].data[y][propName]);
    }
  }

  return data;
};

const countDataName = arr => {
  const countedDataNames = arr.reduce((allNames, name) => {
    if (name !== '' || name.trim() !== '') {
      if (name in allNames) {
        allNames[name]++;
      } else {
        allNames[name] = 1;
      }
    }

    return allNames;
  }, {});

  return countedDataNames;

  // => { 'Alice': 2, 'Bob': 1, 'Tiff': 1, 'Bruce': 1 }
};

const sortBasedOnFrequency = obj => {
  const frequency = _.reduce(
    obj,
    function(result, value, key) {
      (result[value] || (result[value] = [])).push(key);

      return result;
    },
    {}
  );

  return frequency;

  // => { '1': ['Bob', 'Tiff', 'Bruce'], '2': ['Alice'] }
};

const topicTemplate = data => {
  const topic = `<ol>${data.map(
    v => `<li>${v.length > 45 ? v.replace(/(.{46})/g, '$1<br>') : v}</li>`
  )}</ol>`;

  return topic.replace(/>,</g, '><');
};

const getPainsData = list => {
  const painsDataName = getDataName(list, 'pains');
  const painsCountDataName = countDataName(painsDataName);
  const painsBasedOnFrequency = sortBasedOnFrequency(painsCountDataName);

  const filter = [];
  const data = [
    {
      id: 'root-pains',
      isroot: true,
      topic: 'Pains',
      frequency: 0,
      'border-color': '5px solid #dc2228'
    }
  ];

  for (let x = 0; x < list.length; x++) {
    for (let y = 0; y < list[x].data.length; y++) {
      const dataContext = [];
      const parentId = uuid.v4();
      const painValueFrequencyPos = _.toArray(painsBasedOnFrequency).reverse();
      const pos =
        painValueFrequencyPos.findIndex(
          v => v.indexOf(list[x].data[y].pains) !== -1
        ) + 1;

      if (
        _.findIndex(filter, { topic: list[x].data[y].pains }) === -1 &&
        (list[x].data[y].pains !== '' || list[x].data[y].pains.trim() !== '')
      ) {
        data.push({
          id: parentId,
          parentid: 'root-pains',
          topic: `${pos}. ${list[x].data[y].pains}`,
          frequency: pos,
          'border-color': '5px solid #dc2228'
        });

        for (let z = 0; z < list[x].data[y].painsContext.length; z++) {
          dataContext.push(list[x].data[y].painsContext[z].value);
        }

        if (dataContext.length > 0) {
          data.push({
            id: uuid.v4(),
            parentid: parentId,
            topic: topicTemplate(dataContext),
            frequency: pos,
            'border-color': '5px solid #dc2228'
          });
        }
      } else {
        for (let z = 0; z < list[x].data[y].painsContext.length; z++) {
          dataContext.push(list[x].data[y].painsContext[z].value);
        }

        if (dataContext.length > 0) {
          data.push({
            id: uuid.v4(),
            parentid:
              filter.find(v => v.topic === list[x].data[y].pains).id ||
              parentId,
            topic: topicTemplate(dataContext),
            frequency: pos,
            'border-color': '5px solid #dc2228'
          });
        }
      }

      filter.push({ id: parentId, topic: list[x].data[y].pains });
    }
  }

  return _.orderBy(data, ['frequency'], ['asc']);
};

const getRelieversData = list => {
  const relieversDataName = getDataName(list, 'relievers');
  const relieversCountDataName = countDataName(relieversDataName);
  const relieversBasedOnFrequency = sortBasedOnFrequency(
    relieversCountDataName
  );

  const filter = [];
  const data = [
    {
      id: 'root-relievers',
      parentid: 'root-pains',
      topic: 'Relievers',
      frequency: 0,
      'border-color': '5px solid #fdb60d'
    }
  ];

  for (let x = 0; x < list.length; x++) {
    for (let y = 0; y < list[x].data.length; y++) {
      const dataContext = [];
      const parentId = uuid.v4();
      const relieverValueFrequencyPos = _.toArray(
        relieversBasedOnFrequency
      ).reverse();
      const pos =
        relieverValueFrequencyPos.findIndex(
          v => v.indexOf(list[x].data[y].relievers) !== -1
        ) + 1;

      if (
        _.findIndex(filter, { topic: list[x].data[y].relievers }) === -1 &&
        (list[x].data[y].relievers !== '' ||
          list[x].data[y].relievers.trim() !== '')
      ) {
        data.push({
          id: parentId,
          parentid: 'root-relievers',
          topic: `${pos}. ${list[x].data[y].relievers}`,
          frequency: pos,
          'border-color': '5px solid #fdb60d'
        });

        for (let z = 0; z < list[x].data[y].relieversContext.length; z++) {
          dataContext.push(list[x].data[y].relieversContext[z].value);
        }

        if (dataContext.length > 0) {
          data.push({
            id: uuid.v4(),
            parentid: parentId,
            topic: topicTemplate(dataContext),
            frequency: pos,
            'border-color': '5px solid #fdb60d'
          });
        }
      } else {
        for (let z = 0; z < list[x].data[y].relieversContext.length; z++) {
          dataContext.push(list[x].data[y].relieversContext[z].value);
        }

        if (dataContext.length > 0) {
          data.push({
            id: uuid.v4(),
            parentid:
              filter.find(v => v.topic === list[x].data[y].relievers).id ||
              parentId,
            topic: topicTemplate(dataContext),
            frequency: pos,
            'border-color': '5px solid #fdb60d'
          });
        }
      }

      filter.push({ id: parentId, topic: list[x].data[y].relievers });
    }
  }

  return _.orderBy(data, ['frequency'], ['asc']);
};

const getGainsData = list => {
  const gainsDataName = getDataName(list, 'gains');
  const gainsCountDataName = countDataName(gainsDataName);
  const gainsBasedOnFrequency = sortBasedOnFrequency(gainsCountDataName);

  const filter = [];
  const data = [
    {
      id: 'root-gains',
      parentid: 'root-relievers',
      topic: 'Gains',
      frequency: 0,
      'border-color': '5px solid #389337'
    }
  ];

  for (let x = 0; x < list.length; x++) {
    for (let y = 0; y < list[x].data.length; y++) {
      const dataContext = [];
      const parentId = uuid.v4();
      const gainValueFrequencyPos = _.toArray(gainsBasedOnFrequency).reverse();
      const pos =
        gainValueFrequencyPos.findIndex(
          v => v.indexOf(list[x].data[y].gains) !== -1
        ) + 1;

      if (
        _.findIndex(filter, { topic: list[x].data[y].gains }) === -1 &&
        (list[x].data[y].gains !== '' || list[x].data[y].gains.trim() !== '')
      ) {
        data.push({
          id: parentId,
          parentid: 'root-gains',
          topic: `${pos}. ${list[x].data[y].gains}`,
          frequency: pos,
          'border-color': '5px solid #389337'
        });

        for (let z = 0; z < list[x].data[y].gainsContext.length; z++) {
          dataContext.push(list[x].data[y].gainsContext[z].value);
        }

        if (dataContext.length > 0) {
          data.push({
            id: uuid.v4(),
            parentid: parentId,
            topic: topicTemplate(dataContext),
            frequency: pos,
            'border-color': '5px solid #389337'
          });
        }
      } else {
        for (let z = 0; z < list[x].data[y].gainsContext.length; z++) {
          dataContext.push(list[x].data[y].gainsContext[z].value);
        }

        if (dataContext.length > 0) {
          data.push({
            id: uuid.v4(),
            parentid:
              filter.find(v => v.topic === list[x].data[y].gains).id ||
              parentId,
            topic: topicTemplate(dataContext),
            frequency: pos,
            'border-color': '5px solid #389337'
          });
        }
      }

      filter.push({ id: parentId, topic: list[x].data[y].gains });
    }
  }

  return _.orderBy(data, ['frequency'], ['asc']);
};

const splitData = (painsData, relieversData, gainsData) => {
  const painsIndex = Math.round(painsData.length / 2);
  const relieversIndex = Math.round(relieversData.length / 2);

  const newData = [
    ...painsData.slice(0, painsIndex),
    ...relieversData.slice(0, relieversIndex),
    ...gainsData.map(v => v),
    ...relieversData.slice(relieversIndex),
    ...painsData.slice(painsIndex)
  ];

  return newData;
};

class GenerateMindMap {
  constructor(list) {
    this.list = list;
  }

  getMindMapData() {
    const { list } = this;
    const painsData = getPainsData(list);
    const relieversData = getRelieversData(list);
    const gainsData = getGainsData(list);
    const mindMapData = splitData(painsData, relieversData, gainsData);

    return mindMapData;
  }
}

export default GenerateMindMap;
