import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ReactChartkick, { PieChart } from 'react-chartkick';
import * as ChartJS from 'chart.js';
import { NonIdealState } from '@blueprintjs/core';
import _ from 'lodash';

import './Chart.css';

ReactChartkick.addAdapter(ChartJS);

class Chart extends Component {
  state = {
    data: {}
  };

  componentWillMount() {
    const { data } = this.props;

    this.setState({ data });
  }

  render() {
    const { data } = this.state;

    return (
      <div className="chart-group">
        <div className="chart-item">
          {_.isEmpty(data.pains) ? (
            <div>
              <NonIdealState description="No data" />
              <h6>Pains</h6>
            </div>
          ) : (
            <div>
              <PieChart data={data.pains} />
              <h6>Pains</h6>
            </div>
          )}
        </div>
        <div className="chart-item">
          {_.isEmpty(data.relievers) ? (
            <div>
              <NonIdealState description="No data" />
              <h6>Relievers</h6>
            </div>
          ) : (
            <div>
              <PieChart data={data.relievers} />
              <h6>Relievers</h6>
            </div>
          )}
        </div>
        <div className="chart-item">
          {_.isEmpty(data.gains) ? (
            <div>
              <NonIdealState description="No data" />
              <h6>Gains</h6>
            </div>
          ) : (
            <div>
              <PieChart data={data.gains} />
              <h6>Gains</h6>
            </div>
          )}
        </div>
      </div>
    );
  }
}

Chart.propTypes = {
  data: PropTypes.object.isRequired
};

export default Chart;
