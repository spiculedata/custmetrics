const getDataName = (list, propName) => {
  const data = [];

  for (let x = 0; x < list.length; x++) {
    for (let y = 0; y < list[x].data.length; y++) {
      data.push(list[x].data[y][propName]);
    }
  }

  return data;
};

const countDataName = arr => {
  const countedDataNames = arr.reduce((allNames, name) => {
    if (name !== '' || name.trim() !== '') {
      if (name in allNames) {
        allNames[name]++;
      } else {
        allNames[name] = 1;
      }
    }

    return allNames;
  }, {});

  return countedDataNames;
};

class GenerateChart {
  constructor(list) {
    this.list = list;
  }

  getChartData() {
    const { list } = this;
    const painsDataName = getDataName(list, 'pains');
    const relieversDataName = getDataName(list, 'relievers');
    const gainsDataName = getDataName(list, 'gains');
    const chartData = {
      pains: countDataName(painsDataName),
      relievers: countDataName(relieversDataName),
      gains: countDataName(gainsDataName)
    };

    return chartData;
  }
}

export default GenerateChart;
