import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import uuid from 'uuid';
import _ from 'lodash';
import {
  Alert,
  Button,
  Intent,
  NonIdealState,
  Position,
  Toaster
} from '@blueprintjs/core';
import ReactTable from 'react-table';

import { actionCreators } from '../../../actions';

import Loading from '../../UI/Loading';

class ValuePropositionList extends Component {
  state = {
    companyId: '',
    valuePropositionId: '',
    valuePropositions: [],
    valuePropositionsPerCompany: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false
  };

  componentWillMount() {
    const { companyId, valuePropositions } = this.props;
    const valuePropositionsFilter = valuePropositions.filter(
      value => value.companyId === companyId
    );
    const valuePropositionsPerCompany = valuePropositionsFilter;

    this.setState({
      companyId,
      valuePropositions: valuePropositionsFilter,
      valuePropositionsPerCompany
    });
  }

  componentWillReceiveProps(nextProps) {
    const companyIdState = this.state.companyId;
    const companyIdNextProps = nextProps.companyId;

    const valuePropositionsState = this.state.valuePropositions;
    const valuePropositionsNextProps = nextProps.valuePropositions;

    if (
      !_.isEqual(companyIdState, companyIdNextProps) ||
      !_.isEqual(valuePropositionsState, valuePropositionsNextProps)
    ) {
      const valuePropositionsFilter = valuePropositionsNextProps.filter(
        value => value.companyId === companyIdNextProps
      );
      const valuePropositionsPerCompany = valuePropositionsFilter;

      this.setState({
        companyId: companyIdNextProps,
        valuePropositions: valuePropositionsFilter,
        valuePropositionsPerCompany
      });
    }
  }

  onFilter() {
    const { valuePropositionsPerCompany } = this.state;
    const filter = this.filter.value;

    let valuePropositionsFilter = valuePropositionsPerCompany;

    valuePropositionsFilter = valuePropositionsPerCompany.filter(
      value => value.title.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );

    this.setState({
      valuePropositions: valuePropositionsFilter
    });
  }

  handleCancelAlertDelete() {
    this.setState({
      valuePropositionId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false
    });
  }

  handleConfirmAlertDelete() {
    const { valuePropositionId } = this.state;
    const toaster = Toaster.create({
      position: Position.TOP_RIGHT
    });
    const toasterConfig = {
      icon: 'trash',
      intent: Intent.DANGER,
      message: 'Deleted!',
      timeout: 2000
    };

    this.props.onClickDelete(valuePropositionId);

    toaster.show(toasterConfig);

    this.setState({
      valuePropositionId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false
    });
  }

  render() {
    const {
      companyId,
      valuePropositions,
      valuePropositionsPerCompany,
      itemNameToDelete,
      isOpenAlertDelete
    } = this.state;
    const btnCreateValueProposition = (
      <Link
        to={`/company/${companyId}/valueproposition/${uuid.v4()}/new`}
        className="pt-button pt-minimal pt-icon-add pt-intent-primary"
      >
        Create Value Proposition
      </Link>
    );

    return this.props.isFetching ? (
      <div>
        <Loading small />
      </div>
    ) : (
      <div>
        {valuePropositionsPerCompany.length === 0 ? (
          <NonIdealState
            visual="timeline-line-chart"
            title="No value proposition created"
            description="Create a new value proposition:"
            className="m-t-20"
            action={btnCreateValueProposition}
          />
        ) : (
          <div>
            <div className="pt-form-group form-group-search m-t-20">
              <div className="pt-input-group m-r-10" style={{ width: '50%' }}>
                <span className="pt-icon pt-icon-search" />
                <input
                  type="text"
                  ref={input => (this.filter = input)}
                  onChange={() => this.onFilter()}
                  className="pt-input"
                  placeholder="Search Value Proposition"
                />
              </div>

              <div className="m-r-30">{btnCreateValueProposition}</div>
            </div>

            <ReactTable
              data={valuePropositions}
              columns={[
                {
                  Header: 'Info',
                  columns: [
                    {
                      Header: 'Title',
                      accessor: 'title',
                      headerClassName: ['text-left', 'bold']
                    }
                  ]
                },
                {
                  Header: 'Actions',
                  columns: [
                    {
                      Header: 'View',
                      headerClassName: 'bold',
                      width: 80,
                      className: 'text-center',
                      sortable: false,
                      resizable: false,
                      Cell: d => (
                        <Link
                          to={`/company/${
                            d.original.companyId
                          }/valueproposition/${d.original.id}/analytics`}
                          className="pt-button pt-minimal pt-icon-eye-open pt-intent-primary"
                          title="View"
                        />
                      )
                    },
                    {
                      Header: 'Edit',
                      headerClassName: 'bold',
                      width: 80,
                      className: 'text-center',
                      sortable: false,
                      resizable: false,
                      Cell: d => (
                        <Link
                          to={`/company/${
                            d.original.companyId
                          }/valueproposition/${d.original.id}`}
                          className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                          title="Edit"
                        />
                      )
                    },
                    {
                      Header: 'Delete',
                      headerClassName: 'bold',
                      width: 80,
                      className: 'text-center',
                      sortable: false,
                      resizable: false,
                      Cell: d => (
                        <Button
                          icon="trash"
                          intent={Intent.DANGER}
                          title="Delete"
                          minimal
                          onClick={() =>
                            this.setState({
                              valuePropositionId: d.original.id,
                              itemNameToDelete: d.original.title,
                              isOpenAlertDelete: true
                            })
                          }
                        />
                      )
                    }
                  ]
                }
              ]}
              defaultPageSize={20}
              style={{
                height: '90vh'
              }}
              className="-striped -highlight"
            />

            <Alert
              canEscapeKeyCancel={true}
              canOutsideClickCancel={true}
              cancelButtonText="Cancel"
              confirmButtonText="Delete"
              icon="trash"
              intent={Intent.DANGER}
              isOpen={isOpenAlertDelete}
              onCancel={this.handleCancelAlertDelete.bind(this)}
              onConfirm={this.handleConfirmAlertDelete.bind(this)}
            >
              <p>
                Are you sure that you want to delete the{' '}
                <b>{itemNameToDelete}</b> value proposition?
              </p>
            </Alert>
          </div>
        )}
      </div>
    );
  }
}

ValuePropositionList.propTypes = {
  companyId: PropTypes.string.isRequired
};

const mapStateToProps = state => ({
  ...state.valuePropositions
});

const mapDispatchToProps = dispatch => ({
  onClickDelete: id => dispatch(actionCreators.deleteValueProposition(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ValuePropositionList));
