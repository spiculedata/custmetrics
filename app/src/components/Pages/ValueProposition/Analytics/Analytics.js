import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Icon } from '@blueprintjs/core';

import { actionCreators } from '../../../../actions';

import Container from '../../../Layout/Container';
import Loading from '../../../UI/Loading';

import Mindmap from '../Mindmap';
import Chart from '../Chart';

const styleBtnEdit = {
  position: 'fixed',
  right: '20px',
  bottom: '20px',
  zIndex: '5'
};

class Analytics extends Component {
  state = {
    wantsMindmap: [],
    wantsChart: {},
    needsMindmap: [],
    needsChart: {}
  };

  componentDidMount() {
    const { valuePropositionId } = this.props.match.params;

    this.props.getValueProposition(valuePropositionId);
  }

  componentWillReceiveProps(nextProps) {
    const valuePropositionState = this.state.valueProposition;
    const valuePropositionNextProps = nextProps.valueProposition;

    if (!_.isEqual(valuePropositionState, valuePropositionNextProps)) {
      this.setState(valuePropositionNextProps);
    }
  }

  render() {
    const { wantsMindmap, wantsChart } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading text="Loading..." center small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          <Tabs>
            <TabList>
              <Tab>
                <Icon icon="graph" />
                &nbsp; Mind Map
              </Tab>
              <Tab>
                <Icon icon="pie-chart" />
                &nbsp; Charts
              </Tab>
            </TabList>
            <TabPanel>
              <Mindmap container="wants-mindmap" data={wantsMindmap} />
            </TabPanel>
            <TabPanel>
              <Chart data={wantsChart} />
            </TabPanel>
          </Tabs>

          <Link
            to={`/company/${this.state.companyId}/valueproposition/${
              this.state._id
            }`}
            className="pt-button pt-icon-edit pt-intent-success"
            style={styleBtnEdit}
          >
            Edit
          </Link>
        </div>
      </Container>
    );
  }
}

Analytics.propTypes = {
  valueProposition: PropTypes.object.isRequired,
  loading: PropTypes.bool.isRequired,
  getValueProposition: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.valuePropositions
});

const mapDispatchToProps = dispatch => ({
  getValueProposition: valuePropositionId =>
    dispatch(actionCreators.getValueProposition(valuePropositionId))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Analytics));
