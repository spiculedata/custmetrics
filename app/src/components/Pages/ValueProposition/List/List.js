import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  SortableContainer,
  SortableElement,
  arrayMove
} from 'react-sortable-hoc';
import _ from 'lodash';
import { Button, Dialog } from '@blueprintjs/core';

import './List.css';

const SortableItem = SortableElement(
  ({ indexItem, value, editItem, deleteItem }) => (
    <div className="group-list-item">
      <li
        className="pt-card noselect m-b-10"
        style={{ padding: '10px', listStyle: 'none' }}
      >
        {value}
      </li>
      <Button
        icon="edit"
        intent="success"
        title="Edit"
        className="edit-item"
        minimal
        onClick={() => editItem(indexItem)}
      />
      <Button
        icon="small-cross"
        intent="danger"
        title="Delete"
        className="delete-item"
        minimal
        onClick={() => deleteItem(indexItem)}
      />
    </div>
  )
);

const SortableList = SortableContainer(({ items, editItem, deleteItem }) => {
  return (
    <ul style={{ padding: '0', listStyle: 'none' }}>
      {items.map((value, index) => (
        <SortableItem
          key={`item-${index}`}
          index={index}
          indexItem={index}
          value={value}
          editItem={editItem}
          deleteItem={deleteItem}
        />
      ))}
    </ul>
  );
});

class List extends Component {
  state = {
    items: [],
    itemIndexToEdit: '',
    itemToEdit: '',
    isOpenDialog: false
  };

  componentWillMount() {
    const { items } = this.props;

    this.setState({ items });
  }

  componentWillReceiveProps(nextProps) {
    const itemsState = this.state.items;
    const itemsNextProps = nextProps.items;

    if (!_.isEqual(itemsState, itemsNextProps)) {
      this.setState({ items: itemsNextProps });
    }
  }

  onSortEnd({ oldIndex, newIndex }) {
    const { onListComplete } = this.props;

    this.setState({
      items: arrayMove(this.state.items, oldIndex, newIndex)
    });

    if (typeof onListComplete === 'function') {
      onListComplete({
        action: 'MOVE',
        data: this.state.items,
        oldIndex,
        newIndex
      });
    }
  }

  editItem(indexItem) {
    const { items } = this.state;

    this.setState({
      itemIndexToEdit: indexItem,
      itemToEdit: items[indexItem],
      isOpenDialog: true
    });
  }

  deleteItem(indexItem) {
    const { items } = this.state;
    const { onListComplete } = this.props;
    const newItems = [
      ...items.slice(0, indexItem),
      ...items.slice(indexItem + 1)
    ];

    this.setState({
      items: newItems
    });

    if (typeof onListComplete === 'function') {
      onListComplete({
        action: 'DELETE',
        data: newItems,
        oldIndex: indexItem
      });
    }
  }

  handleCloseDialog() {
    this.setState({
      itemIndexToEdit: '',
      itemToEdit: '',
      isOpenDialog: false
    });
  }

  render() {
    const { items, itemToEdit, isOpenDialog } = this.state;

    return (
      <div>
        <SortableList
          items={items}
          onSortEnd={this.onSortEnd.bind(this)}
          editItem={this.editItem.bind(this)}
          deleteItem={this.deleteItem.bind(this)}
        />

        <Dialog
          autoFocus={true}
          canEscapeKeyClose={true}
          canOutsideClickClose={true}
          enforceFocus={true}
          icon="edit"
          isOpen={isOpenDialog}
          onClose={this.handleCloseDialog.bind(this)}
          title="Edit Item"
          usePortal={true}
        >
          <div className="p-t-20 p-r-20 p-b-10 p-l-20">
            <div className="pt-input-group">
              <input
                type="text"
                className="pt-input"
                placeholder="Edit item"
                value={itemToEdit}
                autoFocus
                onChange={event => {
                  const { value } = event.target;

                  this.setState({ itemToEdit: value });
                }}
                onKeyPress={event => {
                  const key = event.key || event.which || event.charCode;
                  const { items, itemIndexToEdit, itemToEdit } = this.state;
                  const { onListComplete } = this.props;

                  if (
                    (key === 'Enter' || key === 13) &&
                    (itemIndexToEdit !== '' || itemIndexToEdit.trim() !== '')
                  ) {
                    items[itemIndexToEdit] = itemToEdit;

                    this.setState({
                      items
                    });

                    if (typeof onListComplete === 'function') {
                      onListComplete({
                        action: 'EDIT',
                        data: items
                      });
                    }

                    this.handleCloseDialog();
                  }
                }}
              />
              <Button
                intent="primary"
                text="Submit"
                onClick={event => {
                  const { items, itemIndexToEdit, itemToEdit } = this.state;
                  const { onListComplete } = this.props;

                  if (itemToEdit !== '' || itemToEdit.trim() !== '') {
                    items[itemIndexToEdit] = itemToEdit;

                    this.setState({
                      items
                    });

                    if (typeof onListComplete === 'function') {
                      onListComplete({
                        action: 'EDIT',
                        data: items
                      });
                    }

                    this.handleCloseDialog();
                  }
                }}
              />
            </div>
          </div>
        </Dialog>
      </div>
    );
  }
}

List.propTypes = {
  items: PropTypes.array.isRequired,
  onListComplete: PropTypes.func.isRequired
};

export default List;
