import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  Alert,
  Button,
  Dialog,
  EditableText,
  Icon,
  Intent
} from '@blueprintjs/core';
import move from 'lodash-move';
import Mousetrap from 'mousetrap';
import mouseTrap from 'react-mousetrap';
import Picky from 'react-picky';
import tinycolor from 'tinycolor2';

import { actionCreators } from '../../../actions';

import GenerateMindMap from './Mindmap/GenerateMindMap';
import GenerateChart from './Chart/GenerateChart';

import Container from '../../Layout/Container';

import List from './List';
import ListTable from './ListTable';

import { TagCreateDialog } from '../Tag';

const styleBtnSave = {
  position: 'fixed',
  right: '20px',
  bottom: '20px'
};

class ValuePropositionCreate extends Component {
  state = {
    companyId: '',
    valuePropositionId: '',
    tagId: '',
    tag: [],
    tags: [],
    title: '',
    wantsItems: [],
    wantsList: [],
    wantsMindmap: [],
    wantsChart: {},
    needsItems: [],
    needsList: [],
    needsMindmap: [],
    needsChart: {},
    inputValueWants: '',
    inputValueNeeds: '',
    createdValueProposition: false,
    isOpenDialogCreateTag: false,
    isOpenTitleAlertError: false,
    isOpenTagAlertError: false
  };

  componentWillMount() {
    const { companyId, valuePropositionId } = this.props.match.params;
    const { tags } = this.props;

    this.setState({ companyId, valuePropositionId, tags });

    // Disable the shortcuts when the focus is on input fields
    Mousetrap.prototype.stopCallback = (e, element, combo) => {
      // If call button Save & Preview
      if (combo === 'mod+s') {
        return false;
      }

      // stop for input, select, and textarea
      return (
        element.tagName === 'INPUT' ||
        element.tagName === 'SELECT' ||
        element.tagName === 'TEXTAREA' ||
        (element.contentEditable && element.contentEditable === 'true')
      );
    };

    this.props.bindShortcut('mod+s', () => {
      // 'mod+s' is a helper to ['command+s', 'ctrl+s']

      this.handleSave();

      // return false to prevent default browser behavior
      // and stop event from bubbling
      return false;
    });
  }

  componentDidMount() {
    this.props.getTags();
  }

  componentWillReceiveProps(nextProps) {
    const valuePropositionIdState = this.state.valuePropositionId;
    const valuePropositionIdNextProps =
      nextProps.match.params.valuePropositionId;

    const tagsState = this.state.tags;
    const tagsNextProps = nextProps.tags;

    if (!_.isEqual(valuePropositionIdState, valuePropositionIdNextProps)) {
      this.setState({
        valuePropositionId: valuePropositionIdNextProps,
        tagId: '',
        tag: [],
        title: '',
        wantsItems: [],
        wantsList: [],
        wantsMindmap: [],
        wantsChart: {},
        needsItems: [],
        needsList: [],
        needsMindmap: [],
        needsChart: {},
        inputValueWants: '',
        inputValueNeeds: '',
        createdValueProposition: false,
        isOpenDialogCreateTag: false,
        isOpenTitleAlertError: false,
        isOpenTagAlertError: false
      });
    }

    if (!_.isEqual(tagsState, tagsNextProps)) {
      this.setState({ tags: tagsNextProps });
    }
  }

  onUpdateWants({ action, data, oldIndex, newIndex }) {
    if (action === 'ADD') {
      this.setState({ wantsList: data });
    } else if (action === 'EDIT') {
      this.setState({ wantsItems: data });
    } else if (action === 'MOVE') {
      const { wantsList } = this.state;
      const newWantsList = move(wantsList, oldIndex, newIndex);

      this.setState({ wantsItems: data, wantsList: newWantsList });
    } else if (action === 'DELETE') {
      const { wantsList } = this.state;
      const newWantsList = [
        ...wantsList.slice(0, oldIndex),
        ...wantsList.slice(oldIndex + 1)
      ];

      this.setState({ wantsItems: data, wantsList: newWantsList });
    }
  }

  onUpdateNeeds({ action, data, oldIndex, newIndex }) {
    if (action === 'ADD') {
      this.setState({ needsList: data });
    } else if (action === 'EDIT') {
      this.setState({ needsItems: data });
    } else if (action === 'MOVE') {
      const { needsList } = this.state;
      const newNeedsList = move(needsList, oldIndex, newIndex);

      this.setState({ needsItems: data, needsList: newNeedsList });
    } else if (action === 'DELETE') {
      const { needsList } = this.state;
      const newNeedsList = [
        ...needsList.slice(0, oldIndex),
        ...needsList.slice(oldIndex + 1)
      ];

      this.setState({ needsItems: data, needsList: newNeedsList });
    }
  }

  addWantsList(item) {
    const { wantsList } = this.state;

    const data = {
      id: uuid.v4(),
      name: item,
      data: []
    };

    this.setState({
      wantsList: wantsList.concat(data)
    });
  }

  addNeedsList(item) {
    const { needsList } = this.state;

    const data = {
      id: uuid.v4(),
      name: item,
      data: []
    };

    this.setState({
      needsList: needsList.concat(data)
    });
  }

  async callGenerateMindMap(dataList) {
    const generateMindMap = new GenerateMindMap(dataList);
    const data = await generateMindMap.getMindMapData();

    return data;
  }

  async callGenerateChart(dataList) {
    const generateChart = new GenerateChart(dataList);
    const data = await generateChart.getChartData();

    return data;
  }

  async handleSave() {
    const type = 'vp';
    const {
      companyId,
      valuePropositionId,
      tagId,
      title,
      wantsItems,
      wantsList,
      needsItems,
      needsList,
      createdValueProposition
    } = this.state;

    if (title === '' || title.trim() === '') {
      this.setState({ isOpenTitleAlertError: true });
    } else if (tagId === '') {
      this.setState({ isOpenTagAlertError: true });
    } else {
      await this.callGenerateMindMap(wantsList).then(data =>
        this.setState({ wantsMindmap: data })
      );

      await this.callGenerateChart(wantsList).then(data =>
        this.setState({ wantsChart: data })
      );

      await this.callGenerateMindMap(needsList).then(data =>
        this.setState({ needsMindmap: data })
      );

      await this.callGenerateChart(needsList).then(data =>
        this.setState({ needsChart: data })
      );

      const { wantsMindmap, wantsChart, needsMindmap, needsChart } = this.state;

      // console.log(
      //   JSON.stringify({
      //     companyId,
      //     valuePropositionId,
      //     tagId,
      //     title,
      //     wantsItems,
      //     wantsList,
      //     wantsMindmap,
      //     wantsChart,
      //     needsItems,
      //     needsList,
      //     needsMindmap,
      //     needsChart
      //   });
      // );

      if (createdValueProposition) {
        this.props.onClickEdit(valuePropositionId, {
          companyId,
          valuePropositionId,
          tagId,
          title,
          type,
          wantsItems,
          wantsList,
          wantsMindmap,
          wantsChart,
          needsItems,
          needsList,
          needsMindmap,
          needsChart
        });
      } else {
        this.setState({ createdValueProposition: true });
        this.props.onClickCreate({
          companyId,
          valuePropositionId,
          tagId,
          title,
          type,
          wantsItems,
          wantsList,
          wantsMindmap,
          wantsChart,
          needsItems,
          needsList,
          needsMindmap,
          needsChart
        });
      }
    }
  }

  handleSelectTag(tag) {
    this.setState({ tagId: tag._id, tag });
  }

  handleCloseDialogCreateTag() {
    this.setState({
      isOpenDialogCreateTag: false
    });
  }

  handleErrorClose() {
    this.setState({
      isOpenTitleAlertError: false,
      isOpenTagAlertError: false
    });
  }

  renderDialogCreateTag() {
    const { isOpenDialogCreateTag } = this.state;

    return (
      <Dialog
        autoFocus={true}
        canEscapeKeyClose={true}
        canOutsideClickClose={true}
        enforceFocus={true}
        icon="tag"
        isOpen={isOpenDialogCreateTag}
        onClose={this.handleCloseDialogCreateTag.bind(this)}
        title="Create Tag"
        usePortal={true}
      >
        <TagCreateDialog
          dialogClose={this.handleCloseDialogCreateTag.bind(this)}
        />
      </Dialog>
    );
  }

  render() {
    const {
      tag,
      tags,
      title,
      wantsItems,
      wantsList,
      isOpenTitleAlertError,
      isOpenTagAlertError
    } = this.state;

    return (
      <Container>
        <div className="content-inner">
          <div className="group-title-tags">
            <h4>
              <EditableText
                maxLength={100}
                placeholder="Click to edit title..."
                className="m-b-20"
                onChange={title => this.setState({ title })}
                value={title}
              />
            </h4>

            <label style={{ width: '300px' }}>
              <a
                onClick={event => {
                  event.preventDefault();

                  this.setState({ isOpenDialogCreateTag: true });
                }}
                style={{ color: '#182026', fontWeight: '600' }}
              >
                Tags
                <Icon
                  icon="small-plus"
                  intent={Intent.PRIMARY}
                  title="Create Tag"
                />
              </a>
              <Picky
                options={tags}
                value={tag}
                valueKey="_id"
                labelKey="name"
                numberDisplayed={0}
                dropdownHeight={300}
                multiple={false}
                onChange={this.handleSelectTag.bind(this)}
                includeFilter
                render={({
                  style,
                  isSelected,
                  item,
                  selectValue,
                  labelKey,
                  valueKey,
                  multiple
                }) => {
                  return (
                    <li
                      style={{
                        backgroundColor: item.color ? item.color : '#333333'
                      }}
                      className={isSelected ? 'selected' : ''}
                      key={item._id}
                      onClick={() => selectValue(item)}
                    >
                      <input type="checkbox" checked={isSelected} readOnly />
                      <span
                        style={{
                          color: tinycolor(item.color).isLight()
                            ? '#000000'
                            : '#ffffff'
                        }}
                      >
                        {item.name}
                      </span>
                    </li>
                  );
                }}
              />
            </label>
          </div>

          <div className="box-group-wantsneeds">
            <div className="box-wants">
              <h5>Wants / Needs</h5>
              <List
                items={wantsItems}
                onListComplete={this.onUpdateWants.bind(this)}
              />
              <div className="pt-input-group">
                <input
                  type="text"
                  className="pt-input"
                  placeholder="Add more wants or needs"
                  value={this.state.inputValueWants}
                  onChange={event => {
                    const { value } = event.target;

                    this.setState({ inputValueWants: value });
                  }}
                  onKeyPress={event => {
                    const key = event.key || event.which || event.charCode;
                    const { wantsItems, inputValueWants } = this.state;

                    if (
                      (key === 'Enter' || key === 13) &&
                      (inputValueWants !== '' || inputValueWants.trim() !== '')
                    ) {
                      this.setState({
                        wantsItems: wantsItems.concat(inputValueWants),
                        inputValueWants: ''
                      });

                      this.addWantsList(inputValueWants);
                    }
                  }}
                />
                <button
                  className="pt-button pt-minimal pt-intent-primary pt-icon-add"
                  onClick={event => {
                    const { wantsItems, inputValueWants } = this.state;

                    if (
                      inputValueWants !== '' ||
                      inputValueWants.trim() !== ''
                    ) {
                      this.setState({
                        wantsItems: wantsItems.concat(inputValueWants),
                        inputValueWants: ''
                      });

                      this.addWantsList(inputValueWants);
                    }
                  }}
                />
              </div>
            </div>

            {/*
            <div className="box-needs">
              <h5>Needs</h5>
              <List
                items={needsItems}
                onListComplete={this.onUpdateNeeds.bind(this)}
              />
              <div className="pt-input-group">
                <input
                  type="text"
                  className="pt-input"
                  placeholder="Add more needs"
                  value={this.state.inputValueNeeds}
                  onChange={event => {
                    const { value } = event.target;

                    this.setState({ inputValueNeeds: value });
                  }}
                  onKeyPress={event => {
                    const key = event.key || event.which || event.charCode;
                    const { needsItems, inputValueNeeds } = this.state;

                    if (
                      (key === 'Enter' || key === 13) &&
                      (inputValueNeeds !== '' || inputValueNeeds.trim() !== '')
                    ) {
                      this.setState({
                        needsItems: needsItems.concat(inputValueNeeds),
                        inputValueNeeds: ''
                      });

                      this.addNeedsList(inputValueNeeds);
                    }
                  }}
                />
                <button
                  className="pt-button pt-minimal pt-intent-primary pt-icon-add"
                  onClick={event => {
                    const { needsItems, inputValueNeeds } = this.state;

                    if (
                      inputValueNeeds !== '' ||
                      inputValueNeeds.trim() !== ''
                    ) {
                      this.setState({
                        needsItems: needsItems.concat(inputValueNeeds),
                        inputValueNeeds: ''
                      });

                      this.addNeedsList(inputValueNeeds);
                    }
                  }}
                />
              </div>
            </div>
            */}
          </div>

          {/*
          <Tabs id="TabsWantsNeeds" animate={false}>
            <Tab
              id="tab-wants"
              title="Wants"
              panel={
                <ListTable
                  name="Wants"
                  items={wantsItems}
                  dataList={wantsList}
                  onListTableComplete={this.onUpdateWants.bind(this)}
                />
              }
            />
            <Tab
              id="tab-needs"
              title="Needs"
              panel={
                <ListTable
                  name="Needs"
                  items={needsItems}
                  dataList={needsList}
                  onListTableComplete={this.onUpdateNeeds.bind(this)}
                />
              }
            />
          </Tabs>
          */}

          <ListTable
            name="Wants / Needs"
            items={wantsItems}
            dataList={wantsList}
            onListTableComplete={this.onUpdateWants.bind(this)}
          />

          {this.renderDialogCreateTag()}

          <Alert
            canEscapeKeyCancel={true}
            canOutsideClickCancel={true}
            confirmButtonText="Okay"
            intent={Intent.WARNING}
            isOpen={isOpenTitleAlertError}
            onClose={this.handleErrorClose.bind(this)}
          >
            <p>You must create a title for the value proposition!</p>
          </Alert>

          <Alert
            canEscapeKeyCancel={true}
            canOutsideClickCancel={true}
            confirmButtonText="Okay"
            intent={Intent.WARNING}
            isOpen={isOpenTagAlertError}
            onClose={this.handleErrorClose.bind(this)}
          >
            <p>You must select a tag for the value proposition!</p>
          </Alert>

          <Button
            icon="floppy-disk"
            intent={Intent.SUCCESS}
            text="Save"
            style={styleBtnSave}
            onClick={this.handleSave.bind(this)}
          />
        </div>
      </Container>
    );
  }
}

ValuePropositionCreate.propTypes = {
  tags: PropTypes.array.isRequired,
  getTags: PropTypes.func.isRequired,
  onClickCreate: PropTypes.func.isRequired,
  onClickEdit: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.tags
});

const mapDispatchToProps = dispatch => ({
  getTags: () => dispatch(actionCreators.getTags()),
  onClickCreate: valuePropositionData =>
    dispatch(actionCreators.addValueProposition(valuePropositionData)),
  onClickEdit: (valuePropositionId, valuePropositionData) =>
    dispatch(
      actionCreators.editValueProposition(
        valuePropositionId,
        valuePropositionData
      )
    )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(mouseTrap(ValuePropositionCreate)));
