export default [
  {
    id: '0',
    companyId: '0',
    valuePropositionId: '0',
    tagId: '4',
    title: 'Spicule VP',
    type: 'vp',
    wantsItems: [
      'Identify Probs/give Solutions',
      'Clairity',
      'PP are Specialists',
      "Confident they're compilant"
    ],
    wantsList: [
      {
        id: '56645a45-5978-4175-a735-d49cf66b9058',
        name: 'Identify Probs/give Solutions',
        data: [
          {
            id: '8351105d-ce20-463a-b265-ba151c797f64',
            pains: 'Worry',
            painsContext: [
              {
                id: '8fb9ae29-5903-4ac5-a714-803daf8e34b7',
                value: 'Due to being illegal',
                label: 'Due to being illegal'
              },
              {
                id: '6ab98738-ae42-43f4-9ff7-4fc71add76b7',
                value: 'Due to being fined',
                label: 'Due to being fined'
              },
              {
                id: '9b43bab2-50ee-41ab-a97c-ecb7c079b7ff',
                value: 'Reputational damage',
                label: 'Reputational damage'
              },
              {
                id: '17634aa6-76a6-4408-9bd9-3df29d267ddb',
                value: 'Blamed for negligence',
                label: 'Blamed for negligence'
              }
            ],
            relievers: 'Knowledge transfer',
            relieversContext: [
              {
                id: '9ef6299d-0b5f-4edb-a61c-b7943d919384',
                value: 'Education around laws',
                label: 'Education around laws'
              },
              {
                id: '37e35a78-9b34-48c7-81b1-247053bcddb5',
                value: 'Education around requirements',
                label: 'Education around requirements'
              },
              {
                id: '33f2bab8-5454-46c4-8bb9-a49f3670b272',
                value: 'Clear non data or legal speak',
                label: 'Clear non data or legal speak'
              },
              {
                id: 'c9bc92b6-167d-4b60-8e6d-8d54fa288e59',
                value: 'Clear steps to solve problems',
                label: 'Clear steps to solve problems'
              }
            ],
            gains: 'Confidence',
            gainsContext: [
              {
                id: '56166505-0e9f-4026-a185-cf68f02b400f',
                value: 'In data security',
                label: 'In data security'
              },
              {
                id: '5fe657c4-4402-4883-8a7a-74c58c15b3c2',
                value: 'To move business forward',
                label: 'To move business forward'
              },
              {
                id: '771fd725-8ff3-4650-b23c-cb44f07578e0',
                value: 'In their product/offering',
                label: 'In their product/offering'
              }
            ]
          }
        ]
      },
      {
        id: '38e9867d-3b95-4aec-b43c-7018dd79aed2',
        name: 'Clairity',
        data: [
          {
            id: 'a566520d-ec49-4482-960f-648646cd25a1',
            pains: 'Confusion',
            painsContext: [
              {
                id: 'cbbc1335-792f-4239-9451-c17047cce22e',
                value: 'Understanding the laws',
                label: 'Understanding the laws'
              },
              {
                id: '849b0088-8710-433b-9b1f-5f5435854fea',
                value: 'What needs to be done to solve their problems',
                label: 'What needs to be done to solve their problems'
              },
              {
                id: '98764db3-bbf9-4e0a-a432-45d685effe6a',
                value: 'Where their data is',
                label: 'Where their data is'
              },
              {
                id: '735d816b-4c26-4548-ba36-ac845e46877a',
                value: 'What data falls under new laws',
                label: 'What data falls under new laws'
              }
            ],
            relievers: 'Audit and report',
            relieversContext: [
              {
                id: 'deb58fae-e317-4ae5-b998-a321d73794c5',
                value: 'Demonstrating clarity of audit and report',
                label: 'Demonstrating clarity of audit and report'
              },
              {
                id: '4dae4f86-9756-4ec0-ae6f-7eda0988236c',
                value: 'Clear next steps and recommendations',
                label: 'Clear next steps and recommendations'
              },
              {
                id: 'e318f2af-ca4b-411d-addb-5dae0f90a3c0',
                value: 'Example Audit and report',
                label: 'Example Audit and report'
              },
              {
                id: '05082bdb-974a-477e-8532-104ccee23121',
                value: 'Example Case studies',
                label: 'Example Case studies'
              }
            ],
            gains: 'Reduced Stress',
            gainsContext: [
              {
                id: 'ace5af85-26b8-4ec8-bd6a-feeace58703d',
                value: 'Know where your data is',
                label: 'Know where your data is'
              },
              {
                id: 'ad06db7c-3c81-4e6e-a6c0-e4e8f4f7d6a7',
                value: 'Know you are compliant',
                label: 'Know you are compliant'
              },
              {
                id: '61ec65e9-4cbf-41f4-863a-58fbef468306',
                value: 'Headspace',
                label: 'Headspace'
              },
              {
                id: '7e17f0ba-4dcb-4ed6-b959-efc5f2df12ad',
                value: 'Easier life',
                label: 'Easier life'
              }
            ]
          },
          {
            id: 'acc4f17b-2747-4f99-a4ea-e48baf541962',
            pains: '',
            painsContext: [],
            relievers: 'Demo',
            relieversContext: [
              {
                id: '5b8a1d0e-d458-4cba-8a8b-e0c7f6f045bc',
                value: 'Easy to understand results',
                label: 'Easy to understand results'
              },
              {
                id: '0b26a96c-47f0-4916-8921-bd6413c5e0e6',
                value: 'Actionable outcomes',
                label: 'Actionable outcomes'
              }
            ],
            gains: 'Clarity',
            gainsContext: [
              {
                id: '4cf396db-24a4-4b96-a2df-2563bf45cf0a',
                value: 'Understand Law',
                label: 'Understand Law'
              },
              {
                id: '01a4ebbb-3f2c-43be-8022-6bab2b1d899f',
                value: 'Know where your data is',
                label: 'Know where your data is'
              },
              {
                id: '7c3f17af-8cb3-479a-a387-5faf7b6e0c7b',
                value: 'Confidence in business',
                label: 'Confidence in business'
              }
            ]
          },
          {
            id: '03610bab-b541-4bef-ae48-bd14335b3c31',
            pains: '',
            painsContext: [],
            relievers: '',
            relieversContext: [],
            gains: 'Save Time',
            gainsContext: [
              {
                id: 'e2ae54de-4605-484d-8454-aecae642ae5a',
                value: 'No "by hand" processes',
                label: 'No "by hand" processes'
              },
              {
                id: '2c9f591e-b3ad-4332-b836-d28d0c6176f5',
                value: 'Headspace',
                label: 'Headspace'
              },
              {
                id: '9e50dd4d-94b0-4727-85c4-dee3714781fb',
                value: 'Save money',
                label: 'Save money'
              }
            ]
          }
        ]
      },
      {
        id: 'e32add9a-e9c8-4a70-ac41-fe3cec9b85a9',
        name: 'PP are Specialists',
        data: [
          {
            id: 'a5339056-2643-4187-8c2c-ef41cb7be469',
            pains: 'Legal implications',
            painsContext: [
              {
                id: 'ec77d6db-aabe-467d-9308-eeaff0a70f95',
                value: 'Due to being illegal',
                label: 'Due to being illegal'
              },
              {
                id: 'e3e49ce6-c75b-431e-b781-86f968cc3c5e',
                value: 'Due to being fined',
                label: 'Due to being fined'
              },
              {
                id: '50e9de3d-46fe-421f-9fe1-8f8242910096',
                value: 'Reputational damage',
                label: 'Reputational damage'
              },
              {
                id: 'cee22c28-5a5a-4740-a216-c3cb01431858',
                value: 'Blamed for negligence',
                label: 'Blamed for negligence'
              },
              {
                id: '7c6b989b-6ad5-417f-b2bf-cb4eabd6b75c',
                value: 'Costs',
                label: 'Costs'
              },
              {
                id: 'cba452f9-e917-4451-af5b-f161f4e19dc3',
                value:
                  'Unable to function as a business without data collection',
                label:
                  'Unable to function as a business without data collection'
              }
            ],
            relievers: '',
            relieversContext: [],
            gains: '',
            gainsContext: []
          }
        ]
      },
      {
        id: 'e7ba84c8-237e-4ae8-b18d-060106dac8ce',
        name: "Confident they're compilant",
        data: [
          {
            id: '61770666-8d25-4dfb-87a3-9ea11de0a083',
            pains: 'Waste Money',
            painsContext: [
              {
                id: '0d3df197-b533-4960-abc7-a522af0b1e67',
                value: 'Not solve their problem',
                label: 'Not solve their problem'
              },
              {
                id: '422fdf42-7e2c-4498-97c1-438694949156',
                value: 'Job will not be "Complete solution"',
                label: 'Job will not be "Complete solution"'
              }
            ],
            relievers: 'Testimionals',
            relieversContext: [
              {
                id: '2155cb73-d03e-4241-920b-a95ddbb98caf',
                value: 'High profile people and brands',
                label: 'High profile people and brands'
              },
              {
                id: '34159979-903c-446c-ba2e-8f60ebc26f0b',
                value:
                  'Content focusing on customer pains, relievers and gains',
                label: 'Content focusing on customer pains, relievers and gains'
              }
            ],
            gains: 'Make informed decisions',
            gainsContext: [
              {
                id: '773b585f-e52f-4821-b839-cbed1d193723',
                value: 'Have all available data/information to hand',
                label: 'Have all available data/information to hand'
              },
              {
                id: 'c8a11f71-e5a6-495f-961e-1a7415e01a20',
                value: 'Easily to communicate around business',
                label: 'Easily to communicate around business'
              },
              {
                id: '8ee5526e-25a5-44d8-a445-5770c5ab798b',
                value: 'Clear next steps and actionable tasks',
                label: 'Clear next steps and actionable tasks'
              }
            ]
          },
          {
            id: '785485d6-aeee-429a-b82d-e55c49538521',
            pains: 'Reputational damage',
            painsContext: [
              {
                id: 'f55ccf58-55e1-4715-8503-760dc54dfc3f',
                value: 'Damage to brand',
                label: 'Damage to brand'
              },
              {
                id: 'fbb0818b-6749-4858-bd31-78ebeca7f11d',
                value: 'Lose trust of customers',
                label: 'Lose trust of customers'
              },
              {
                id: '38e7e690-0304-404a-868c-8ca4cdb84be2',
                value: 'lose confidence of investors (current of future)',
                label: 'lose confidence of investors (current of future)'
              }
            ],
            relievers: '',
            relieversContext: [],
            gains: '',
            gainsContext: []
          },
          {
            id: '7c580289-a1f9-4c3e-81c5-aa62d5fcaf1b',
            pains: 'Due diligence',
            painsContext: [
              {
                id: '962aaeff-3532-4cea-ab7c-ae0e6863112d',
                value: 'Will Persona Protection find all our data?',
                label: 'Will Persona Protection find all our data?'
              },
              {
                id: 'd46d0606-ab12-4a28-80c1-5fcc8265fa57',
                value: 'Will Persona Protection make/help us to be compliant?',
                label: 'Will Persona Protection make/help us to be compliant?'
              }
            ],
            relievers: '',
            relieversContext: [],
            gains: '',
            gainsContext: []
          },
          {
            id: 'd432fa14-a37f-4bd6-a035-9272b0b0fa03',
            pains: 'Fines/Penilties ',
            painsContext: [
              {
                id: '157068f1-4041-43f4-a879-0e97011cd843',
                value: 'Costs',
                label: 'Costs'
              },
              {
                id: 'f1ab9c74-6e36-4df8-9b16-23a801141293',
                value: 'Inhibit business growth',
                label: 'Inhibit business growth'
              },
              {
                id: '08eee9df-d65e-40b3-b3c3-3e29f5ad7f9a',
                value: 'Bankrupt business',
                label: 'Bankrupt business'
              }
            ],
            relievers: '',
            relieversContext: [],
            gains: '',
            gainsContext: []
          }
        ]
      }
    ],
    wantsMindmap: [
      {
        id: 'root-pains',
        isroot: true,
        topic: 'Pains',
        'border-color': '5px solid #dc2228'
      },
      {
        id: '3102f119-fae4-4145-9999-c0d05bee8336',
        parentid: 'root-pains',
        topic: '1. Worry',
        'border-color': '5px solid #dc2228'
      },
      {
        id: '66121625-84a3-499e-aa95-13777feacd9b',
        parentid: '3102f119-fae4-4145-9999-c0d05bee8336',
        topic:
          '<ol><li>Due to being illegal</li><li>Due to being fined</li><li>Reputational damage</li><li>Blamed for negligence</li></ol>',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'dfe1fa9c-5bc1-4c5d-aa78-aac476a096f8',
        parentid: 'root-pains',
        topic: '2. Confusion',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'de32b217-45eb-485d-8196-ac3aa139bce5',
        parentid: 'dfe1fa9c-5bc1-4c5d-aa78-aac476a096f8',
        topic:
          '<ol><li>Understanding the laws</li><li>What needs to be done to solve their problems</li><li>Where their data is</li><li>What data falls under new laws</li></ol>',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'a4dbcde7-9d0d-4910-88d3-c2d648f0c3a3',
        parentid: 'root-pains',
        topic: '3. Legal implications',
        'border-color': '5px solid #dc2228'
      },
      {
        id: '29b54aa1-4a6d-457b-84a3-4e48f40e7b67',
        parentid: 'a4dbcde7-9d0d-4910-88d3-c2d648f0c3a3',
        topic:
          '<ol><li>Due to being illegal</li><li>Due to being fined</li><li>Reputational damage</li><li>Blamed for negligence</li><li>Costs</li><li>Unable to function as a business without data <br>collection</li></ol>',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'e992dfed-8058-425c-a244-1ae1afff0706',
        parentid: 'root-pains',
        topic: '4. Waste Money',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'root-relievers',
        parentid: 'root-pains',
        topic: 'Relievers',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: '17c9eaac-2d9b-4bf3-b719-841a4773a50a',
        parentid: 'root-relievers',
        topic: '1. Knowledge transfer',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: '472dec48-953f-430b-b80f-b04b97be5881',
        parentid: '17c9eaac-2d9b-4bf3-b719-841a4773a50a',
        topic:
          '<ol><li>Education around laws</li><li>Education around requirements</li><li>Clear non data or legal speak</li><li>Clear steps to solve problems</li></ol>',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: 'afd605b9-e633-497d-8670-04a78e6e8916',
        parentid: 'root-relievers',
        topic: '2. Audit and report',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: '5eca9c00-33e7-4c24-a228-b832e964ded5',
        parentid: 'afd605b9-e633-497d-8670-04a78e6e8916',
        topic:
          '<ol><li>Demonstrating clarity of audit and report</li><li>Clear next steps and recommendations</li><li>Example Audit and report</li><li>Example Case studies</li></ol>',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: 'root-gains',
        parentid: 'root-relievers',
        topic: 'Gains',
        'border-color': '5px solid #389337'
      },
      {
        id: '31cff426-981f-4893-9aa6-0442a86581fa',
        parentid: 'root-gains',
        topic: '1. Confidence',
        'border-color': '5px solid #389337'
      },
      {
        id: 'c611f91e-bb04-4fca-ad15-1248f3878ff0',
        parentid: '31cff426-981f-4893-9aa6-0442a86581fa',
        topic:
          '<ol><li>In data security</li><li>To move business forward</li><li>In their product/offering</li></ol>',
        'border-color': '5px solid #389337'
      },
      {
        id: '6b8bdba6-ee9e-43de-b666-8c08491ccc79',
        parentid: 'root-gains',
        topic: '2. Reduced Stress',
        'border-color': '5px solid #389337'
      },
      {
        id: 'a041830f-5a9c-4a2d-890c-7058fde1f70f',
        parentid: '6b8bdba6-ee9e-43de-b666-8c08491ccc79',
        topic:
          '<ol><li>Know where your data is</li><li>Know you are compliant</li><li>Headspace</li><li>Easier life</li></ol>',
        'border-color': '5px solid #389337'
      },
      {
        id: 'd4ba329b-6e4a-4c25-9b45-58e6c8fa2365',
        parentid: 'root-gains',
        topic: '2. Clarity',
        'border-color': '5px solid #389337'
      },
      {
        id: '983bb014-6c58-4b38-9bb6-9fe777ec67bf',
        parentid: 'd4ba329b-6e4a-4c25-9b45-58e6c8fa2365',
        topic:
          '<ol><li>Understand Law</li><li>Know where your data is</li><li>Confidence in business</li></ol>',
        'border-color': '5px solid #389337'
      },
      {
        id: 'f8aaed75-c2e4-48da-829b-f98f4d48e3ee',
        parentid: 'root-gains',
        topic: '2. Save Time',
        'border-color': '5px solid #389337'
      },
      {
        id: 'ff772ada-1bd9-4987-a618-2cb6c4bbfe15',
        parentid: 'f8aaed75-c2e4-48da-829b-f98f4d48e3ee',
        topic:
          '<ol><li>No "by hand" processes</li><li>Headspace</li><li>Save money</li></ol>',
        'border-color': '5px solid #389337'
      },
      {
        id: 'de61c83d-1010-4ebb-85fa-b2256b82afc2',
        parentid: 'root-gains',
        topic: '4. Make informed decisions',
        'border-color': '5px solid #389337'
      },
      {
        id: '2d30e0a1-2c02-4a7b-8133-76184cd03617',
        parentid: 'de61c83d-1010-4ebb-85fa-b2256b82afc2',
        topic:
          '<ol><li>Have all available data/information to hand</li><li>Easily to communicate around business</li><li>Clear next steps and actionable tasks</li></ol>',
        'border-color': '5px solid #389337'
      },
      {
        id: '99be5ddd-8c1e-40ff-a019-58dd63aa5319',
        parentid: 'root-relievers',
        topic: '2. Demo',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: 'dc3c01cc-0102-45ac-bc66-aca16431a233',
        parentid: '99be5ddd-8c1e-40ff-a019-58dd63aa5319',
        topic:
          '<ol><li>Easy to understand results</li><li>Actionable outcomes</li></ol>',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: '9c1b356b-5978-4d06-b5e0-6da578076b43',
        parentid: 'root-relievers',
        topic: '4. Testimionals',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: 'a770250b-40d4-4e2c-b991-ef1a5c61f024',
        parentid: '9c1b356b-5978-4d06-b5e0-6da578076b43',
        topic:
          '<ol><li>High profile people and brands</li><li>Content focusing on customer pains, relievers <br>and gains</li></ol>',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: 'a6015382-52fa-4fb9-aa5d-85817329afa0',
        parentid: 'e992dfed-8058-425c-a244-1ae1afff0706',
        topic:
          '<ol><li>Not solve their problem</li><li>Job will not be "Complete solution"</li></ol>',
        'border-color': '5px solid #dc2228'
      },
      {
        id: '768c9224-867b-4a8e-ad9e-03d3cceadaa5',
        parentid: 'root-pains',
        topic: '4. Reputational damage',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'ab297670-344d-4a29-95cc-46991b706de7',
        parentid: '768c9224-867b-4a8e-ad9e-03d3cceadaa5',
        topic:
          '<ol><li>Damage to brand</li><li>Lose trust of customers</li><li>lose confidence of investors (current of futur<br>e)</li></ol>',
        'border-color': '5px solid #dc2228'
      },
      {
        id: '58f74b87-a289-4360-8e7f-29962a200d02',
        parentid: 'root-pains',
        topic: '4. Due diligence',
        'border-color': '5px solid #dc2228'
      },
      {
        id: '7458b35b-6430-4902-bffb-d5bdfafddfd7',
        parentid: '58f74b87-a289-4360-8e7f-29962a200d02',
        topic:
          '<ol><li>Will Persona Protection find all our data?</li><li>Will Persona Protection make/help us to be com<br>pliant?</li></ol>',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'cec97900-4883-4650-b5b2-e6662663a65e',
        parentid: 'root-pains',
        topic: '4. Fines/Penilties ',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'e197451c-00e5-4235-a6fd-7e47816d2f0f',
        parentid: 'cec97900-4883-4650-b5b2-e6662663a65e',
        topic:
          '<ol><li>Costs</li><li>Inhibit business growth</li><li>Bankrupt business</li></ol>',
        'border-color': '5px solid #dc2228'
      }
    ],
    wantsChart: {
      pains: {
        Worry: 1,
        Confusion: 1,
        'Legal implications': 1,
        'Waste Money': 1,
        'Reputational damage': 1,
        'Due diligence': 1,
        'Fines/Penilties ': 1
      },
      relievers: {
        'Knowledge transfer': 1,
        'Audit and report': 1,
        Demo: 1,
        Testimionals: 1
      },
      gains: {
        Confidence: 1,
        'Reduced Stress': 1,
        Clarity: 1,
        'Save Time': 1,
        'Make informed decisions': 1
      }
    },
    needsItems: [
      'To know where data is',
      'Evidence that it works',
      'PP are not a data risk/security',
      'Costing Structure'
    ],
    needsList: [
      {
        id: '5a196363-e949-409a-ac14-f26fc81b245e',
        name: 'To know where data is',
        data: []
      },
      {
        id: '6c52d136-f9ce-4f1d-b3b6-668ba829b0d7',
        name: 'Evidence that it works',
        data: []
      },
      {
        id: '848248b4-e631-4314-9524-6208eef4b3bb',
        name: 'PP are not a data risk/security',
        data: []
      },
      {
        id: '49faa6dd-5091-49dc-8f59-637baebdc940',
        name: 'Costing Structure',
        data: []
      }
    ],
    needsMindmap: [
      {
        id: 'root-pains',
        isroot: true,
        topic: 'Pains',
        'border-color': '5px solid #dc2228'
      },
      {
        id: 'root-relievers',
        parentid: 'root-pains',
        topic: 'Relievers',
        'border-color': '5px solid #fdb60d'
      },
      {
        id: 'root-gains',
        parentid: 'root-relievers',
        topic: 'Gains',
        'border-color': '5px solid #389337'
      }
    ],
    needsChart: {
      pains: {},
      relievers: {},
      gains: {}
    }
  }
];
