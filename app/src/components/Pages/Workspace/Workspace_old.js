import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import { Icon } from '@blueprintjs/core';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';
import { ValuePropositionList } from '../ValueProposition';
import { NotesList } from '../Notes';

class Workspace extends Component {
  state = {
    companyId: '',
    tabIndex: 0
  };

  componentWillMount() {
    const { search } = this.props.location;
    const { companyId } = this.props.match.params;
    const tabIndex = Number(search.split('=')[1]) || 0;

    this.setState({ companyId, tabIndex });
  }

  componentWillReceiveProps(nextProps) {
    const { companyId } = nextProps.match.params;

    this.setState({ companyId });
  }

  render() {
    const { companyId, tabIndex } = this.state;

    return this.props.isFetching ? (
      <Container>
        <div className="content-inner">
          <Loading small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          <Tabs
            selectedIndex={tabIndex}
            onSelect={tabIndex => {
              this.setState({ tabIndex });
              this.props.history.push(`?tab=${tabIndex}`);
            }}
          >
            <TabList>
              <Tab>
                <Icon icon="timeline-line-chart" />
                &nbsp; Value Propositions
              </Tab>
              <Tab>
                <Icon icon="comment" />
                &nbsp; Notes Tree
              </Tab>
            </TabList>

            <TabPanel>
              <ValuePropositionList companyId={companyId} />
            </TabPanel>
            <TabPanel>
              <NotesList companyId={companyId} />
            </TabPanel>
          </Tabs>
        </div>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  ...state.notes
});

export default connect(
  mapStateToProps,
  null
)(withRouter(Workspace));
