import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Alert, Button, Icon, Intent } from '@blueprintjs/core';
import ReactTable from 'react-table';
import BlockUi from 'react-block-ui';

import { actionCreators } from '../../../actions';

class WorkspaceItem extends Component {
  state = {
    tag: {},
    valuePropositionId: '',
    notesId: '',
    linkId: '',
    valuePropositions: [],
    notes: [],
    links: [],
    data: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false,
    isBlockingUi: false
  };

  componentWillMount() {
    const { tag, valuePropositions, notes, links } = this.props;
    const valuePropositionsFilter = valuePropositions.filter(
      v => v.tagId === tag._id
    );
    const notesFilter = notes.filter(v => v.tagId === tag._id && !v.template);
    const linksFilter = links.filter(v => v.tagId === tag._id);

    this.setState({
      tag,
      valuePropositions: valuePropositionsFilter,
      notes: notesFilter,
      links: linksFilter,
      data: _.flattenDeep([valuePropositionsFilter, notesFilter, linksFilter])
    });
  }

  componentWillReceiveProps(nextProps) {
    const tagState = this.state.tag;
    const tagNextProps = nextProps.tag;

    const valuePropositionsState = this.state.valuePropositions;
    const valuePropositionsNextProps = nextProps.valuePropositions;

    const notesState = this.state.notes;
    const notesNextProps = nextProps.notes;

    const linksState = this.state.links;
    const linksNextProps = nextProps.links;

    const tagsState = this.state.tags;
    const tagsNextProps = nextProps.tags;

    if (
      !_.isEqual(tagState, tagNextProps) ||
      !_.isEqual(valuePropositionsState, valuePropositionsNextProps) ||
      !_.isEqual(notesState, notesNextProps) ||
      !_.isEqual(linksState, linksNextProps) ||
      !_.isEqual(tagsState, tagsNextProps)
    ) {
      const valuePropositionsFilter = valuePropositionsNextProps.filter(
        v => v.tagId === tagNextProps._id
      );
      const notesFilter = notesNextProps.filter(
        v => v.tagId === tagNextProps._id && !v.template
      );
      const linksFilter = linksNextProps.filter(
        v => v.tagId === tagNextProps._id
      );

      this.setState({
        tagNextProps,
        valuePropositions: valuePropositionsFilter,
        notes: notesFilter,
        links: linksFilter,
        data: _.flattenDeep([valuePropositionsFilter, notesFilter, linksFilter])
      });
    }

    if (!nextProps.loading && this.state.isBlockingUi) {
      this.setState({ isBlockingUi: false });
    }
  }

  handleCancelAlertDelete() {
    this.setState({
      valuePropositionId: '',
      notesId: '',
      linkId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: false
    });
  }

  handleConfirmAlertDelete() {
    const { valuePropositionId, notesId, linkId } = this.state;

    if (!_.isEmpty(valuePropositionId)) {
      this.props.onClickDeleteValueProposition(valuePropositionId);
    } else if (!_.isEmpty(notesId)) {
      this.props.onClickDeleteNotes(notesId);
    } else if (!_.isEmpty(linkId)) {
      this.props.onClickDeleteLink(linkId);
    }

    this.setState({
      valuePropositionId: '',
      notesId: '',
      linkId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: true
    });
  }

  render() {
    const {
      data,
      itemNameToDelete,
      isOpenAlertDelete,
      isBlockingUi
    } = this.state;

    return (
      <div>
        <BlockUi tag="div" blocking={isBlockingUi}>
          <ReactTable
            data={data}
            filterable
            columns={[
              {
                Header: 'Info',
                columns: [
                  {
                    Header: 'Title',
                    accessor: 'title',
                    headerClassName: ['text-left', 'bold'],
                    filterMethod: (filter, row) => {
                      const { value } = filter;
                      const { title } = row._original;

                      return (
                        title.toLowerCase().indexOf(value.toLowerCase()) !== -1
                      );
                    },
                    Cell: d => {
                      const { title, type } = d.original;

                      if (type === 'vp') {
                        return (
                          <div>
                            <Icon icon="timeline-line-chart" />
                            &nbsp; {title}
                          </div>
                        );
                      } else if (type === 'notes') {
                        return (
                          <div>
                            <Icon icon="comment" />
                            &nbsp; {title}
                          </div>
                        );
                      } else if (type === 'link') {
                        return (
                          <div>
                            <Icon icon="link" />
                            &nbsp; {title}
                          </div>
                        );
                      }
                    }
                  }
                ]
              },
              {
                Header: 'Actions',
                columns: [
                  {
                    Header: 'View',
                    headerClassName: 'bold',
                    width: 80,
                    className: 'text-center',
                    filterable: false,
                    sortable: false,
                    resizable: false,
                    Cell: d => {
                      const { _id, companyId, type } = d.original;

                      if (type === 'vp') {
                        return (
                          <Link
                            to={`/company/${companyId}/valueproposition/${_id}/analytics`}
                            className="pt-button pt-minimal pt-icon-eye-open pt-intent-primary"
                            title="View"
                          />
                        );
                      } else if (type === 'notes') {
                        return (
                          <Link
                            to={`/company/${companyId}/notes/view/${_id}`}
                            className="pt-button pt-minimal pt-icon-eye-open pt-intent-primary"
                            title="View"
                          />
                        );
                      } else if (type === 'link') {
                        return (
                          <a
                            href={d.original.url}
                            className="pt-button pt-minimal pt-icon-eye-open pt-intent-primary"
                            title="View"
                            target="_blank"
                          >
                            {''}
                          </a>
                        );
                      }
                    }
                  },
                  {
                    Header: 'Edit',
                    headerClassName: 'bold',
                    width: 80,
                    className: 'text-center',
                    filterable: false,
                    sortable: false,
                    resizable: false,
                    Cell: d => {
                      const { _id, companyId, type } = d.original;

                      if (type === 'vp') {
                        return (
                          <Link
                            to={`/company/${companyId}/valueproposition/${_id}`}
                            className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                            title="Edit"
                          />
                        );
                      } else if (type === 'notes') {
                        return (
                          <Link
                            to={`/company/${companyId}/notes/${_id}`}
                            className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                            title="Edit"
                          />
                        );
                      } else if (type === 'link') {
                        return (
                          <Link
                            to={`/company/${companyId}/link/${_id}`}
                            className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                            title="Edit"
                          />
                        );
                      }
                    }
                  },
                  {
                    Header: 'Delete',
                    headerClassName: 'bold',
                    width: 80,
                    className: 'text-center',
                    filterable: false,
                    sortable: false,
                    resizable: false,
                    Cell: d => {
                      const { _id, title, type } = d.original;

                      if (type === 'vp') {
                        return (
                          <Button
                            icon="trash"
                            intent={Intent.DANGER}
                            title="Delete"
                            minimal
                            onClick={() =>
                              this.setState({
                                valuePropositionId: _id,
                                itemNameToDelete: title,
                                isOpenAlertDelete: true
                              })
                            }
                          />
                        );
                      } else if (type === 'notes') {
                        return (
                          <Button
                            icon="trash"
                            intent={Intent.DANGER}
                            title="Delete"
                            minimal
                            onClick={() =>
                              this.setState({
                                notesId: _id,
                                itemNameToDelete: title,
                                isOpenAlertDelete: true
                              })
                            }
                          />
                        );
                      } else if (type === 'link') {
                        return (
                          <Button
                            icon="trash"
                            intent={Intent.DANGER}
                            title="Delete"
                            minimal
                            onClick={() =>
                              this.setState({
                                linkId: _id,
                                itemNameToDelete: title,
                                isOpenAlertDelete: true
                              })
                            }
                          />
                        );
                      }
                    }
                  }
                ]
              }
            ]}
            defaultPageSize={10}
            style={{
              height: '50vh'
            }}
            className="-striped -highlight"
          />
        </BlockUi>

        <Alert
          canEscapeKeyCancel={true}
          canOutsideClickCancel={true}
          cancelButtonText="Cancel"
          confirmButtonText="Delete"
          icon="trash"
          intent={Intent.DANGER}
          isOpen={isOpenAlertDelete}
          onCancel={this.handleCancelAlertDelete.bind(this)}
          onConfirm={this.handleConfirmAlertDelete.bind(this)}
        >
          <p>
            Are you sure that you want to delete the <b>{itemNameToDelete}</b>{' '}
            from workspace?
          </p>
        </Alert>
      </div>
    );
  }
}

WorkspaceItem.propTypes = {
  tag: PropTypes.object.isRequired,
  valuePropositions: PropTypes.array,
  notes: PropTypes.array,
  links: PropTypes.array,
  loading: PropTypes.bool.isRequired,
  onClickDeleteValueProposition: PropTypes.func.isRequired,
  onClickDeleteNotes: PropTypes.func.isRequired,
  onClickDeleteLink: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  onClickDeleteValueProposition: id =>
    dispatch(actionCreators.deleteValueProposition(id)),
  onClickDeleteNotes: id => dispatch(actionCreators.deleteNotes(id)),
  onClickDeleteLink: id => dispatch(actionCreators.deleteLink(id))
});

export default connect(
  null,
  mapDispatchToProps
)(WorkspaceItem);
