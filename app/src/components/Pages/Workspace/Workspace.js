import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
  Accordion,
  AccordionItem,
  AccordionItemTitle,
  AccordionItemBody
} from 'react-accessible-accordion';
import { NonIdealState, Tag } from '@blueprintjs/core';
import tinycolor from 'tinycolor2';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';
import WorkspaceItem from './WorkspaceItem';

class Workspace extends Component {
  state = {
    companyId: '',
    valuePropositions: [],
    notesAll: [],
    links: [],
    tags: []
  };

  componentWillMount() {
    const { companyId } = this.props.match.params;
    const { valuePropositions, notesAll, links, tags } = this.props;

    let valuePropositionsTags = [];
    let notesTags = [];
    let linksTags = [];

    const valuePropositionsFilter = valuePropositions.filter(value => {
      if (value.companyId === companyId) {
        const tagsFilter = tags.filter(t => t._id === value.tagId);

        valuePropositionsTags.push(tagsFilter);

        return value;
      }

      return false;
    });
    const notesFilter = notesAll.filter(value => {
      if (value.companyId === companyId && !value.template) {
        const tagsFilter = tags.filter(t => t._id === value.tagId);

        notesTags.push(tagsFilter);

        return value;
      }

      return false;
    });
    const linksFilter = links.filter(value => {
      if (value.companyId === companyId) {
        const tagsFilter = tags.filter(t => t._id === value.tagId);

        linksTags.push(tagsFilter);

        return value;
      }

      return false;
    });

    this.setState({
      companyId,
      valuePropositions: valuePropositionsFilter,
      notesAll: notesFilter,
      links: linksFilter,
      tags: _.uniqBy(
        _.flattenDeep([valuePropositionsTags, notesTags, linksTags]),
        'name'
      )
    });
  }

  componentDidMount() {
    this.callApi();
  }

  componentWillReceiveProps(nextProps) {
    const companyIdState = this.state.companyId;
    const companyIdNextProps = nextProps.match.params.companyId;

    const valuePropositionsState = this.state.valuePropositions;
    const valuePropositionsNextProps = nextProps.valuePropositions;

    const notesState = this.state.notesAll;
    const notesNextProps = nextProps.notesAll;

    const linksState = this.state.links;
    const linksNextProps = nextProps.links;

    const tagsState = this.state.tags;
    const tagsNextProps = nextProps.tags;

    let notesTags = [];
    let valuePropositionsTags = [];
    let linksTags = [];

    if (
      !_.isEqual(companyIdState, companyIdNextProps) ||
      !_.isEqual(valuePropositionsState, valuePropositionsNextProps) ||
      !_.isEqual(notesState, notesNextProps) ||
      !_.isEqual(linksState, linksNextProps) ||
      !_.isEqual(tagsState, tagsNextProps)
    ) {
      const valuePropositionsFilter = valuePropositionsNextProps.filter(
        value => {
          if (value.companyId === companyIdNextProps) {
            const tagsFilter = tagsNextProps.filter(t => t._id === value.tagId);

            valuePropositionsTags.push(tagsFilter);

            return value;
          }

          return false;
        }
      );
      const notesFilter = notesNextProps.filter(value => {
        if (value.companyId === companyIdNextProps && !value.template) {
          const tagsFilter = tagsNextProps.filter(t => t._id === value.tagId);

          notesTags.push(tagsFilter);

          return value;
        }

        return false;
      });
      const linksFilter = linksNextProps.filter(value => {
        if (value.companyId === companyIdNextProps) {
          const tagsFilter = tagsNextProps.filter(t => t._id === value.tagId);

          linksTags.push(tagsFilter);

          return value;
        }

        return false;
      });

      this.setState({
        companyId: companyIdNextProps,
        valuePropositions: valuePropositionsFilter,
        notesAll: notesFilter,
        links: linksFilter,
        tags: _.uniqBy(
          _.flattenDeep([valuePropositionsTags, notesTags, linksTags]),
          'name'
        )
      });
    }
  }

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.companyId !== this.state.companyId) {
      this.callApi();
    }
  }

  callApi = () => {
    const { companyId } = this.props.match.params;

    this.props.getValuePropositions(companyId);
    this.props.getNotesAll(companyId);
    this.props.getLinks(companyId);
    this.props.getTags();
  };

  renderItem(tag, index) {
    const { valuePropositions, notesAll, links } = this.state;
    const { name, color } = tag;

    return (
      <AccordionItem key={index} uuid={index}>
        <AccordionItemTitle>
          <h6 className="u-position-relative">
            <Tag
              large
              style={{
                backgroundColor: color ? color : '#333333',
                color: tinycolor(color).isLight() ? '#000000' : '#ffffff'
              }}
            >
              {name}
            </Tag>
            <div className="accordion__arrow" role="presentation" />
          </h6>
        </AccordionItemTitle>
        <AccordionItemBody>
          <WorkspaceItem
            tag={tag}
            valuePropositions={valuePropositions}
            notes={notesAll}
            links={links}
            loading={this.props.loading}
          />
        </AccordionItemBody>
      </AccordionItem>
    );
  }

  render() {
    const { tags } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading text="Loading..." center small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          {tags.length === 0 ? (
            <NonIdealState visual="box" title="Workspace Empty" />
          ) : (
            <Accordion>
              {tags.map((tag, index) => this.renderItem(tag, index))}
            </Accordion>
          )}
        </div>
      </Container>
    );
  }
}

Workspace.propTypes = {
  valuePropositions: PropTypes.array.isRequired,
  notesAll: PropTypes.array.isRequired,
  links: PropTypes.array.isRequired,
  tags: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  getValuePropositions: PropTypes.func.isRequired,
  getNotesAll: PropTypes.func.isRequired,
  getLinks: PropTypes.func.isRequired,
  getTags: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.valuePropositions,
  ...state.notes,
  ...state.links,
  ...state.tags
});

const mapDispatchToProps = dispatch => ({
  getValuePropositions: companyId =>
    dispatch(actionCreators.getValuePropositions(companyId)),
  getNotesAll: companyId => dispatch(actionCreators.getNotesAll(companyId)),
  getLinks: companyId => dispatch(actionCreators.getLinks(companyId)),
  getTags: () => dispatch(actionCreators.getTags())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Workspace));
