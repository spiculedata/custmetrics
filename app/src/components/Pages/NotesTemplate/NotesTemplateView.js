import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import SortableTree from 'react-sortable-tree';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';

const styleBtnEdit = {
  position: 'absolute',
  right: '20px',
  bottom: '20px'
};

class NotesTemplateView extends Component {
  state = {
    notesId: '',
    title: '',
    treeData: []
  };

  componentWillMount() {
    const { notesId } = this.props.match.params;

    this.setState({ notesId });
  }

  componentDidMount() {
    const { notesId } = this.props.match.params;

    this.props.getNotesTemplate(notesId);
  }

  componentWillReceiveProps(nextProps) {
    const notesTemplateState = this.state.notesTemplate;
    const notesTemplateNextProps = nextProps.notesTemplate;

    if (!_.isEqual(notesTemplateState, notesTemplateNextProps)) {
      this.setState(notesTemplateNextProps);
    }
  }

  render() {
    const { _id, title, treeData } = this.state;

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <Loading text="Loading..." center small />
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner no-padding-right">
          <h4>{title}</h4>

          <div style={{ height: '100%' }}>
            <SortableTree
              treeData={treeData}
              onChange={treeData => this.setState({ treeData })}
              canDrag={false}
            />
          </div>

          <Link
            to={`/notes_template/${_id}`}
            className="pt-button pt-icon-edit pt-intent-success"
            style={styleBtnEdit}
          >
            Edit
          </Link>
        </div>
      </Container>
    );
  }
}

NotesTemplateView.propTypes = {
  loading: PropTypes.bool.isRequired,
  getNotesTemplate: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.notesTemplates
});

const mapDispatchToProps = dispatch => ({
  getNotesTemplate: id => dispatch(actionCreators.getNotesTemplate(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(NotesTemplateView));
