import NotesTemplateList from './NotesTemplateList';
import NotesTemplateCreate from './NotesTemplateCreate';
import NotesTemplateView from './NotesTemplateView';
import NotesTemplateEdit from './NotesTemplateEdit';

export {
  NotesTemplateList,
  NotesTemplateCreate,
  NotesTemplateView,
  NotesTemplateEdit
};
