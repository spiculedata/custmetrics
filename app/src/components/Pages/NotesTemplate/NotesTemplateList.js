import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import uuid from 'uuid';
import _ from 'lodash';
import { Alert, Button, Intent, NonIdealState } from '@blueprintjs/core';
import ReactTable from 'react-table';
import BlockUi from 'react-block-ui';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';
import Loading from '../../UI/Loading';

class NotesTemplateList extends Component {
  state = {
    notesId: '',
    notesTemplates: [],
    itemNameToDelete: '',
    isOpenAlertDelete: false,
    isOpenDeleteAlertError: false,
    isBlockingUi: false,
    errors: {}
  };

  componentWillMount() {
    const { notesTemplates } = this.props;

    this.setState({ notesTemplates });
  }

  componentDidMount() {
    this.props.getNotesTemplates();
  }

  componentWillReceiveProps(nextProps) {
    const notesTemplatesState = this.state.notesTemplates;
    const notesTemplatesNextProps = nextProps.notesTemplates;
    const { errors } = nextProps;

    if (!_.isEqual(notesTemplatesState, notesTemplatesNextProps)) {
      this.setState({ notesTemplates: notesTemplatesNextProps });
    }

    if (!_.isEmpty(errors) && this.state.isBlockingUi) {
      this.setState({
        isOpenDeleteAlertError: true,
        isBlockingUi: false,
        errors
      });
    }

    if (!nextProps.loading && this.state.isBlockingUi) {
      this.setState({ isBlockingUi: false });
    }
  }

  onFilter() {
    const { notesTemplates } = this.props;
    const filter = this.filter.value;

    let notesTemplatesFilter = notesTemplates;

    notesTemplatesFilter = notesTemplates.filter(
      value => value.title.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    );

    this.setState({
      notesTemplates: notesTemplatesFilter
    });
  }

  handleCancelAlertDelete() {
    this.setState({
      notesId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: false
    });
  }

  handleConfirmAlertDelete() {
    const { notesId } = this.state;

    this.props.onClickDelete(notesId);

    this.setState({
      notesId: '',
      itemNameToDelete: '',
      isOpenAlertDelete: false,
      isBlockingUi: true
    });
  }

  handleErrorClose() {
    this.props.clearErrors();
    this.props.getNotesTemplates();

    this.setState({
      isOpenDeleteAlertError: false,
      isBlockingUi: false,
      errors: {}
    });
  }

  render() {
    const {
      notesTemplates,
      itemNameToDelete,
      isOpenAlertDelete,
      isOpenDeleteAlertError,
      isBlockingUi,
      errors
    } = this.state;
    const btnCreateNotesTreeTemplate = (
      <Link
        to={`/notes_template/${uuid.v4()}/new`}
        className="pt-button pt-minimal pt-icon-add pt-intent-primary"
      >
        Create Notes Tree Template
      </Link>
    );

    return this.props.loading ? (
      <Container>
        <div className="content-inner">
          <div className="center-block">
            <Loading small /> &nbsp; Loading...
          </div>
        </div>
      </Container>
    ) : (
      <Container>
        <div className="content-inner">
          {this.props.notesTemplates.length === 0 ? (
            <NonIdealState
              visual="comment"
              title="No notes tree template created"
              description="Create a new notes tree template:"
              action={btnCreateNotesTreeTemplate}
            />
          ) : (
            <div>
              <div className="pt-form-group form-group-search">
                <div className="pt-input-group m-r-10" style={{ width: '50%' }}>
                  <span className="pt-icon pt-icon-search" />
                  <input
                    type="text"
                    ref={input => (this.filter = input)}
                    onChange={() => this.onFilter()}
                    className="pt-input"
                    placeholder="Search Notes Tree Template"
                  />
                </div>

                <div className="m-r-30">{btnCreateNotesTreeTemplate}</div>
              </div>

              <BlockUi tag="div" blocking={isBlockingUi}>
                <ReactTable
                  data={notesTemplates}
                  columns={[
                    {
                      Header: 'Info',
                      columns: [
                        {
                          Header: 'Title',
                          accessor: 'title',
                          headerClassName: ['text-left', 'bold']
                        }
                      ]
                    },
                    {
                      Header: 'Actions',
                      columns: [
                        {
                          Header: 'View',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Link
                              to={`/notes_template/view/${d.original._id}`}
                              className="pt-button pt-minimal pt-icon-eye-open pt-intent-primary"
                              title="View"
                            />
                          )
                        },
                        {
                          Header: 'Edit',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Link
                              to={`/notes_template/${d.original._id}`}
                              className="pt-button pt-minimal pt-icon-edit pt-intent-success"
                              title="Edit"
                            />
                          )
                        },
                        {
                          Header: 'Delete',
                          headerClassName: 'bold',
                          width: 80,
                          className: 'text-center',
                          sortable: false,
                          resizable: false,
                          Cell: d => (
                            <Button
                              icon="trash"
                              intent={Intent.DANGER}
                              title="Delete"
                              minimal
                              onClick={() =>
                                this.setState({
                                  notesId: d.original._id,
                                  itemNameToDelete: d.original.title,
                                  isOpenAlertDelete: true
                                })
                              }
                            />
                          )
                        }
                      ]
                    }
                  ]}
                  defaultPageSize={20}
                  style={{
                    height: '90vh'
                  }}
                  className="-striped -highlight"
                />
              </BlockUi>

              <Alert
                canEscapeKeyCancel={true}
                canOutsideClickCancel={true}
                confirmButtonText="Close"
                icon="error"
                intent={Intent.DANGER}
                isOpen={isOpenDeleteAlertError}
                onClose={this.handleErrorClose.bind(this)}
              >
                <p>{errors.noNotesTemplatesFound}</p>
              </Alert>

              <Alert
                canEscapeKeyCancel={true}
                canOutsideClickCancel={true}
                cancelButtonText="Cancel"
                confirmButtonText="Delete"
                icon="trash"
                intent={Intent.DANGER}
                isOpen={isOpenAlertDelete}
                onCancel={this.handleCancelAlertDelete.bind(this)}
                onConfirm={this.handleConfirmAlertDelete.bind(this)}
              >
                <p>
                  Are you sure that you want to delete the{' '}
                  <b>{itemNameToDelete}</b> notes tree template?
                </p>
              </Alert>
            </div>
          )}
        </div>
      </Container>
    );
  }
}

NotesTemplateList.propTypes = {
  notesTemplates: PropTypes.array.isRequired,
  loading: PropTypes.bool.isRequired,
  errors: PropTypes.object.isRequired,
  getNotesTemplates: PropTypes.func.isRequired,
  onClickDelete: PropTypes.func.isRequired,
  clearErrors: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  ...state.notesTemplates,
  errors: state.errors
});

const mapDispatchToProps = dispatch => ({
  getNotesTemplates: () => dispatch(actionCreators.getNotesTemplates()),
  onClickDelete: id => dispatch(actionCreators.deleteNotesTemplate(id)),
  clearErrors: () => dispatch(actionCreators.clearErrors())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotesTemplateList);
