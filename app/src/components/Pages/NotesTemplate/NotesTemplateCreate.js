import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import _ from 'lodash';
import SortableTree, {
  addNodeUnderParent,
  changeNodeAtPath,
  removeNodeAtPath
} from 'react-sortable-tree';
import {
  Alert,
  Button,
  ButtonGroup,
  EditableText,
  Intent
} from '@blueprintjs/core';
import Mousetrap from 'mousetrap';
import mouseTrap from 'react-mousetrap';

import { actionCreators } from '../../../actions';

import Container from '../../Layout/Container';

const styleBtnGroup = {
  position: 'absolute',
  right: '20px',
  bottom: '20px'
};

class NotesTemplateCreate extends Component {
  state = {
    notesId: '',
    title: '',
    template: true,
    treeData: [],
    createdNotesTemplate: false,
    isOpenTitleAlertError: false
  };

  componentWillMount() {
    const { notesId } = this.props.match.params;

    this.setState({ notesId });

    // Disable the shortcuts when the focus is on input fields
    Mousetrap.prototype.stopCallback = (e, element, combo) => {
      // If call button Save & Preview
      if (combo === 'mod+s') {
        return false;
      }

      // stop for input, select, and textarea
      return (
        element.tagName === 'INPUT' ||
        element.tagName === 'SELECT' ||
        element.tagName === 'TEXTAREA' ||
        (element.contentEditable && element.contentEditable === 'true')
      );
    };

    this.props.bindShortcut('mod+s', () => {
      // 'mod+s' is a helper to ['command+s', 'ctrl+s']

      this.handleSave();

      // return false to prevent default browser behavior
      // and stop event from bubbling
      return false;
    });
  }

  componentWillReceiveProps(nextProps) {
    const notesIdState = this.state.notesId;
    const notesIdNextProps = nextProps.match.params.notesId;

    if (!_.isEqual(notesIdState, notesIdNextProps)) {
      this.setState({ notesId: notesIdNextProps });
    }
  }

  handleSave() {
    const {
      notesId,
      title,
      template,
      treeData,
      createdNotesTemplate
    } = this.state;
    if (title === '' || title.trim() === '') {
      this.setState({ isOpenTitleAlertError: true });
    } else {
      if (createdNotesTemplate) {
        this.props.onClickEdit(notesId, { notesId, title, template, treeData });
      } else {
        this.setState({ createdNotesTemplate: true });
        this.props.onClickCreate({ notesId, title, template, treeData });
      }
    }
  }

  handleErrorClose() {
    this.setState({ isOpenTitleAlertError: false });
  }

  render() {
    const { title, isOpenTitleAlertError } = this.state;
    const defaultNodeName = _.uniqueId('new_node_');
    const getNodeKey = ({ treeIndex }) => treeIndex;

    return (
      <Container>
        <div className="content-inner no-padding-right">
          <div className="group-title-tags">
            <h4>
              <EditableText
                maxLength={50}
                placeholder="Click to edit title..."
                onChange={title => this.setState({ title })}
                value={title}
              />
            </h4>
          </div>

          <div style={{ height: '100%' }}>
            <SortableTree
              treeData={this.state.treeData}
              onChange={treeData => this.setState({ treeData })}
              generateNodeProps={({ node, path }) => ({
                title: (
                  <input
                    className="pt-input"
                    style={{ width: '300px' }}
                    value={node.name}
                    onChange={event => {
                      const name = event.target.value;

                      this.setState(state => ({
                        treeData: changeNodeAtPath({
                          treeData: state.treeData,
                          path,
                          getNodeKey,
                          newNode: { ...node, name, title: name }
                        })
                      }));
                    }}
                  />
                ),
                buttons: [
                  <Button
                    icon="add"
                    intent={Intent.PRIMARY}
                    minimal
                    onClick={() =>
                      this.setState(state => ({
                        treeData: addNodeUnderParent({
                          treeData: state.treeData,
                          parentKey: path[path.length - 1],
                          expandParent: true,
                          getNodeKey,
                          newNode: {
                            name: `${defaultNodeName}`,
                            title: `${defaultNodeName}`
                          }
                        }).treeData
                      }))
                    }
                  />,
                  <Button
                    icon="trash"
                    intent={Intent.DANGER}
                    minimal
                    onClick={() =>
                      this.setState(state => ({
                        treeData: removeNodeAtPath({
                          treeData: state.treeData,
                          path,
                          getNodeKey
                        })
                      }))
                    }
                  />
                ]
              })}
            />
          </div>

          <Alert
            canEscapeKeyCancel={true}
            canOutsideClickCancel={true}
            confirmButtonText="Okay"
            intent={Intent.WARNING}
            isOpen={isOpenTitleAlertError}
            onClose={this.handleErrorClose.bind(this)}
          >
            <p>You must create a title for the notes template!</p>
          </Alert>

          <ButtonGroup style={styleBtnGroup}>
            <Button
              icon="add"
              intent={Intent.PRIMARY}
              text="Add Node"
              onClick={() =>
                this.setState(state => ({
                  treeData: state.treeData.concat({
                    name: `${defaultNodeName}`,
                    title: `${defaultNodeName}`
                  })
                }))
              }
            />
            <Button
              icon="floppy-disk"
              intent="success"
              text="Save"
              onClick={this.handleSave.bind(this)}
            />
          </ButtonGroup>
        </div>
      </Container>
    );
  }
}

NotesTemplateCreate.propTypes = {
  onClickCreate: PropTypes.func.isRequired,
  onClickEdit: PropTypes.func.isRequired
};

const mapDispatchToProps = dispatch => ({
  onClickCreate: notesData =>
    dispatch(actionCreators.addNotesTemplate(notesData)),
  onClickEdit: (notesId, notesData) =>
    dispatch(actionCreators.editNotesTemplate(notesId, notesData))
});

export default connect(
  null,
  mapDispatchToProps
)(withRouter(mouseTrap(NotesTemplateCreate)));
