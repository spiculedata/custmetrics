import Login from './components/Pages/Login';
import ResetPassword from './components/Pages/ResetPassword';
import ResetPassword2 from './components/Pages/ResetPassword2';
import Register from './components/Pages/Register';
import CheckRegister from './components/Pages/CheckRegister';
import Workspace from './components/Pages/Workspace';
import {
  CompanyList,
  CompanyCreate,
  CompanyEdit
} from './components/Pages/Company';
import { TagList, TagCreate, TagEdit } from './components/Pages/Tag';
import Labels from './components/Pages/Labels';
import {
  PainLabelCreate,
  PainLabelEdit
} from './components/Pages/Labels/PainLabel';
import {
  RelieverLabelCreate,
  RelieverLabelEdit
} from './components/Pages/Labels/RelieverLabel';
import {
  GainLabelCreate,
  GainLabelEdit
} from './components/Pages/Labels/GainLabel';
import {
  ValuePropositionCreate,
  ValuePropositionEdit
} from './components/Pages/ValueProposition';
import Analytics from './components/Pages/ValueProposition/Analytics';
import { NotesCreate, NotesView, NotesEdit } from './components/Pages/Notes';
import {
  NotesTemplateList,
  NotesTemplateCreate,
  NotesTemplateView,
  NotesTemplateEdit
} from './components/Pages/NotesTemplate';
import { LinkCreate, LinkEdit } from './components/Pages/Link';
import Settings from './components/Pages/Settings';
import Profile from './components/Pages/Profile';

export const routes = [
  {
    path: '/',
    title: 'Login',
    exact: true,
    private: false,
    component: Login
  },
  {
    path: '/reset_password',
    title: 'Reset Password',
    exact: true,
    private: false,
    component: ResetPassword
  },
  {
    path: '/reset_password/:token',
    title: 'Reset Password',
    exact: true,
    private: false,
    component: ResetPassword2
  },
  {
    path: '/register',
    title: 'Register',
    exact: true,
    private: false,
    component: Register
  },
  {
    path: '/check_register/:userId',
    title: 'Check Register',
    exact: true,
    private: false,
    component: CheckRegister
  },
  {
    path: '/workspace/:companyId',
    title: 'Workspace',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: Workspace
  },
  {
    path: '/companies',
    title: 'Companies',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: CompanyList
  },
  {
    path: '/company/new',
    title: 'Create Company',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: CompanyCreate
  },
  {
    path: '/company/:companyId',
    title: 'Edit Company',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: CompanyEdit
  },
  {
    path: '/tags',
    title: 'Tags',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: TagList
  },
  {
    path: '/tag/new',
    title: 'Create Tag',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: TagCreate
  },
  {
    path: '/tag/:tagId',
    title: 'Edit Tag',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: TagEdit
  },
  {
    path: '/labels',
    title: 'Value Proposition Labels',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: Labels
  },
  {
    path: '/label/pain/new',
    title: 'Create Pain Label',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: PainLabelCreate
  },
  {
    path: '/label/pain/:labelId',
    title: 'Edit Pain Label',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: PainLabelEdit
  },
  {
    path: '/label/reliever/new',
    title: 'Create Reliever Label',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: RelieverLabelCreate
  },
  {
    path: '/label/reliever/:labelId',
    title: 'Edit Reliever Label',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: RelieverLabelEdit
  },
  {
    path: '/label/gain/new',
    title: 'Create Gain Label',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: GainLabelCreate
  },
  {
    path: '/label/gain/:labelId',
    title: 'Edit Gain Label',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: GainLabelEdit
  },
  {
    path: '/company/:companyId/valueproposition/:valuePropositionId/new',
    title: 'Create Value Proposition',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    // workspace_tab: '?tab=0',
    exact: true,
    private: true,
    component: ValuePropositionCreate
  },
  {
    path: '/company/:companyId/valueproposition/:valuePropositionId',
    title: 'Edit Value Proposition',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    // workspace_tab: '?tab=0',
    exact: true,
    private: true,
    component: ValuePropositionEdit
  },
  {
    path: '/company/:companyId/valueproposition/:valuePropositionId/analytics',
    title: 'Value Proposition Analytics',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    // workspace_tab: '?tab=0',
    exact: true,
    private: true,
    component: Analytics
  },
  {
    path: '/company/:companyId/notes/:notesId/new',
    title: 'Create Notes',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    // workspace_tab: '?tab=1',
    exact: true,
    private: true,
    component: NotesCreate
  },
  {
    path: '/company/:companyId/notes/view/:notesId',
    title: 'View Notes',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    // workspace_tab: '?tab=1',
    exact: true,
    private: true,
    component: NotesView
  },
  {
    path: '/company/:companyId/notes/:notesId',
    title: 'Edit Notes',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    // workspace_tab: '?tab=1',
    exact: true,
    private: true,
    component: NotesEdit
  },
  {
    path: '/notes_templates',
    title: 'Notes Templates',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: NotesTemplateList
  },
  {
    path: '/notes_template/:notesId/new',
    title: 'Create Notes Template',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: NotesTemplateCreate
  },
  {
    path: '/notes_template/view/:notesId',
    title: 'View Notes Template',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: NotesTemplateView
  },
  {
    path: '/notes_template/:notesId',
    title: 'Edit Notes Template',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: NotesTemplateEdit
  },
  {
    path: '/company/:companyId/link/new',
    title: 'Create Link',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    exact: true,
    private: true,
    component: LinkCreate
  },
  {
    path: '/company/:companyId/link/:linkId',
    title: 'Edit Link',
    show_sidebar: true,
    show_btn_toolbar_add: true,
    show_btn_toolbar_back: true,
    exact: true,
    private: true,
    component: LinkEdit
  },
  {
    path: '/settings',
    title: 'Settings',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: Settings
  },
  {
    path: '/profile',
    title: 'Profile',
    show_sidebar: false,
    show_btn_toolbar_add: false,
    show_btn_toolbar_back: false,
    exact: true,
    private: true,
    component: Profile
  }
];
