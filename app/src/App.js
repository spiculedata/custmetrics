import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import jwt_decode from 'jwt-decode';
import setAuthToken from './utils/setAuthToken';
import { setCurrentUser, logoutUser } from './actions/authActions';

import { Provider } from 'react-redux';
import store from './store';

import { routes } from './routes';

import PrivateRoute from './components/Common/PrivateRoute';
import NoMatch from './components/Pages/NoMatch';

import '@blueprintjs/core/lib/css/blueprint.css';
import '@blueprintjs/icons/lib/css/blueprint-icons.css';
import 'react-table/react-table.css';
import 'react-sortable-tree/style.css';
import 'react-tabs/style/react-tabs.css';
import 'react-accessible-accordion/dist/fancy-example.css';
import 'react-select/dist/react-select.css';
import 'react-picky/dist/picky.css';
import 'react-block-ui/style.css';
import './styles/app.css';

// Check for token
if (localStorage.jwtToken) {
  const { jwtToken } = localStorage;

  // Set auth token header auth
  setAuthToken(jwtToken);

  // Decode token and get user info and exp
  const decoded = jwt_decode(jwtToken);

  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));

  // Check for expired token
  const currentTime = Date.now() / 1000;

  if (decoded.exp < currentTime) {
    // Logout user
    store.dispatch(logoutUser());

    // Redirect to login
    window.location.href = '/';
  }
}

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            {routes.map((route, index) => {
              return route.private ? (
                <PrivateRoute
                  key={index}
                  path={route.path}
                  component={route.component}
                  exact={route.exact}
                />
              ) : (
                <Route
                  key={index}
                  path={route.path}
                  component={route.component}
                  exact={route.exact}
                />
              );
            })}
            <Route component={NoMatch} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
