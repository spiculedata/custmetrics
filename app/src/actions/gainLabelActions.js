import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_GAIN_LABEL,
  GET_GAINS_LABELS,
  GET_GAIN_LABEL,
  ADD_GAIN_LABEL,
  EDIT_GAIN_LABEL,
  DELETE_GAIN_LABEL,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getGainsLabels = () => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get('/api/label/gains')
    .then(res =>
      dispatch({
        type: GET_GAINS_LABELS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_GAINS_LABELS,
        payload: []
      })
    );
};

export const getGainLabel = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/label/gains/${id}`)
    .then(res =>
      dispatch({
        type: GET_GAIN_LABEL,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_GAIN_LABEL,
        payload: {}
      })
    );
};

export const addGainLabel = (labelData, history) => dispatch => {
  axios
    .post('/api/label/gains', labelData)
    .then(res => {
      dispatch({
        type: ADD_GAIN_LABEL,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      if (history) {
        history.push('/labels?tab=2');
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editGainLabel = (id, labelData, history) => dispatch => {
  axios
    .put(`/api/label/gains/${id}`, labelData)
    .then(res => {
      dispatch({
        type: EDIT_GAIN_LABEL,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      history.push('/labels?tab=2');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteGainLabel = id => dispatch => {
  axios
    .delete(`/api/label/gains/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_GAIN_LABEL,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_GAIN_LABEL
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
