import axios from 'axios';
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';
import uuid from 'uuid';

import {
  LOADING_USER,
  CHECK_USER,
  RESET_PASSWORD,
  SET_CURRENT_USER,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

// Register User
export const registerUser = (userData, history) => dispatch => {
  axios
    .post('/api/users/register', userData)
    .then(res => {
      const userId = res.data._id;

      dispatch(clearErrors());

      history.push(`/check_register/${userId}`);
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Check user by id
export const checkUser = userId => dispatch => {
  dispatch(setUserLoading());

  axios
    .get(`/api/users/${userId}`)
    .then(res =>
      dispatch({
        type: CHECK_USER,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Login - Get User Token
export const loginUser = userData => dispatch => {
  axios
    .post('/api/users/login', userData)
    .then(res => {
      // Save to localStorage
      const { token } = res.data;

      // Set token to localStorage
      localStorage.setItem('jwtToken', token);

      // Set token to Auth header
      setAuthToken(token);

      // Decode token to get user data
      const decoded = jwt_decode(token);

      dispatch(clearErrors());

      // Set current user
      dispatch(setCurrentUser(decoded));
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const checkToken = token => dispatch => {
  dispatch(setUserLoading());

  axios
    .get(`/api/users/reset_password/${token}`)
    .then(res =>
      dispatch({
        type: RESET_PASSWORD,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const resetPassword = (email, history) => dispatch => {
  axios
    .post(`/api/users/reset_password/${uuid.v4()}`, email)
    .then(res => {
      dispatch(clearErrors());
      history.push('/reset_password/?reset=true');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const resetPassword2 = (token, newPasswordData, history) => dispatch => {
  axios
    .put(`/api/users/reset_password/${token}`, newPasswordData)
    .then(res => {
      dispatch(clearErrors());
      history.push(`/reset_password/${token}/?reseted=true`);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// Log user out
export const logoutUser = () => dispatch => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');

  // Remove auth header for future requests
  setAuthToken(false);

  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
};

// Set loading state
export const setUserLoading = () => {
  return {
    type: LOADING_USER
  };
};

// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
