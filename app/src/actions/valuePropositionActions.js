import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_VALUE_PROPOSITION,
  GET_VALUE_PROPOSITIONS,
  GET_VALUE_PROPOSITION,
  ADD_VALUE_PROPOSITION,
  EDIT_VALUE_PROPOSITION,
  DELETE_VALUE_PROPOSITION,
  CLEAR_VALUE_PROPOSITION,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getValuePropositions = companyId => dispatch => {
  dispatch(setLoading());

  axios
    .get(`/api/valuepropositions/company/${companyId}`)
    .then(res =>
      dispatch({
        type: GET_VALUE_PROPOSITIONS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VALUE_PROPOSITIONS,
        payload: []
      })
    );
};

export const getValueProposition = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/valuepropositions/${id}`)
    .then(res =>
      dispatch({
        type: GET_VALUE_PROPOSITION,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_VALUE_PROPOSITION,
        payload: {}
      })
    );
};

export const addValueProposition = valuePropositionData => dispatch => {
  axios
    .post('/api/valuepropositions', valuePropositionData)
    .then(res => {
      dispatch({
        type: ADD_VALUE_PROPOSITION,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editValueProposition = (
  valuepropositionId,
  valuepropositionData
) => dispatch => {
  axios
    .put(`/api/valuepropositions/${valuepropositionId}`, valuepropositionData)
    .then(res => {
      dispatch({
        type: EDIT_VALUE_PROPOSITION,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteValueProposition = id => dispatch => {
  axios
    .delete(`/api/valuepropositions/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_VALUE_PROPOSITION,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const clearValueProposition = () => {
  return {
    type: CLEAR_VALUE_PROPOSITION
  };
};

export const setLoading = () => {
  return {
    type: LOADING_VALUE_PROPOSITION
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
