import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_TAG,
  GET_TAGS,
  GET_TAG,
  ADD_TAG,
  EDIT_TAG,
  DELETE_TAG,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getTags = () => dispatch => {
  dispatch(setLoading());

  axios
    .get('/api/tags')
    .then(res =>
      dispatch({
        type: GET_TAGS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_TAGS,
        payload: []
      })
    );
};

export const getTag = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/tags/${id}`)
    .then(res =>
      dispatch({
        type: GET_TAG,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_TAG,
        payload: {}
      })
    );
};

export const addTag = (tagData, history) => dispatch => {
  axios
    .post('/api/tags', tagData)
    .then(res => {
      dispatch({
        type: ADD_TAG,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      if (history) {
        history.push('/tags');
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editTag = (id, tagData, history) => dispatch => {
  axios
    .put(`/api/tags/${id}`, tagData)
    .then(res => {
      dispatch({
        type: EDIT_TAG,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      history.push('/tags');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteTag = id => dispatch => {
  axios
    .delete(`/api/tags/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_TAG,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_TAG
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
