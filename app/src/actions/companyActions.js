import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_COMPANY,
  GET_COMPANIES,
  GET_COMPANY,
  ADD_COMPANY,
  EDIT_COMPANY,
  DELETE_COMPANY,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getCompanies = () => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get('/api/companies')
    .then(res =>
      dispatch({
        type: GET_COMPANIES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_COMPANIES,
        payload: []
      })
    );
};

export const getCompany = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/companies/${id}`)
    .then(res =>
      dispatch({
        type: GET_COMPANY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_COMPANY,
        payload: {}
      })
    );
};

export const addCompany = (companyData, history) => dispatch => {
  axios
    .post('/api/companies', companyData)
    .then(res => {
      dispatch({
        type: ADD_COMPANY,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      history.push('/companies');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editCompany = (id, companyData, history) => dispatch => {
  axios
    .put(`/api/companies/${id}`, companyData)
    .then(res => {
      dispatch({
        type: EDIT_COMPANY,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      history.push('/companies');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteCompany = id => dispatch => {
  axios
    .delete(`/api/companies/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_COMPANY,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_COMPANY
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
