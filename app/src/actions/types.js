export const LOADING_USER = 'LOADING_USER';
export const CHECK_USER = 'CHECK_USER';
export const RESET_PASSWORD = 'RESET_PASSWORD';
export const SET_CURRENT_USER = 'SET_CURRENT_USER';

export const LOADING_COMPANY = 'LOADING_COMPANY';
export const GET_COMPANIES = 'GET_COMPANIES';
export const GET_COMPANY = 'GET_COMPANY';
export const ADD_COMPANY = 'ADD_COMPANY';
export const EDIT_COMPANY = 'EDIT_COMPANY';
export const DELETE_COMPANY = 'DELETE_COMPANY';

export const LOADING_TAG = 'LOADING_TAG';
export const GET_TAGS = 'GET_TAGS';
export const GET_TAG = 'GET_TAG';
export const ADD_TAG = 'ADD_TAG';
export const EDIT_TAG = 'EDIT_TAG';
export const DELETE_TAG = 'DELETE_TAG';

export const LOADING_PAIN_LABEL = 'LOADING_PAIN_LABEL';
export const GET_PAINS_LABELS = 'GET_PAINS_LABELS';
export const GET_PAIN_LABEL = 'GET_PAIN_LABEL';
export const ADD_PAIN_LABEL = 'ADD_PAIN_LABEL';
export const EDIT_PAIN_LABEL = 'EDIT_PAIN_LABEL';
export const DELETE_PAIN_LABEL = 'DELETE_PAIN_LABEL';

export const LOADING_RELIEVER_LABEL = 'LOADING_RELIEVER_LABEL';
export const GET_RELIEVERS_LABELS = 'GET_RELIEVERS_LABELS';
export const GET_RELIEVER_LABEL = 'GET_RELIEVER_LABEL';
export const ADD_RELIEVER_LABEL = 'ADD_RELIEVER_LABEL';
export const EDIT_RELIEVER_LABEL = 'EDIT_RELIEVER_LABEL';
export const DELETE_RELIEVER_LABEL = 'DELETE_RELIEVER_LABEL';

export const LOADING_GAIN_LABEL = 'LOADING_GAIN_LABEL';
export const GET_GAINS_LABELS = 'GET_GAINS_LABELS';
export const GET_GAIN_LABEL = 'GET_GAIN_LABEL';
export const ADD_GAIN_LABEL = 'ADD_GAIN_LABEL';
export const EDIT_GAIN_LABEL = 'EDIT_GAIN_LABEL';
export const DELETE_GAIN_LABEL = 'DELETE_GAIN_LABEL';

export const LOADING_VALUE_PROPOSITION = 'LOADING_VALUE_PROPOSITION';
export const GET_VALUE_PROPOSITIONS = 'GET_VALUE_PROPOSITIONS';
export const GET_VALUE_PROPOSITION = 'GET_VALUE_PROPOSITION';
export const ADD_VALUE_PROPOSITION = 'ADD_VALUE_PROPOSITION';
export const EDIT_VALUE_PROPOSITION = 'EDIT_VALUE_PROPOSITION';
export const DELETE_VALUE_PROPOSITION = 'DELETE_VALUE_PROPOSITION';
export const CLEAR_VALUE_PROPOSITION = 'CLEAR_VALUE_PROPOSITION';

export const LOADING_NOTES = 'LOADING_NOTES';
export const GET_NOTES_ALL = 'GET_NOTES_ALL';
export const GET_NOTES = 'GET_NOTES';
export const ADD_NOTES = 'ADD_NOTES';
export const EDIT_NOTES = 'EDIT_NOTES';
export const DELETE_NOTES = 'DELETE_NOTES';

export const LOADING_NOTES_TEMPLATE = 'LOADING_NOTES_TEMPLATE';
export const GET_NOTES_TEMPLATES = 'GET_NOTES_TEMPLATES';
export const GET_NOTES_TEMPLATE = 'GET_NOTES_TEMPLATE';
export const ADD_NOTES_TEMPLATE = 'ADD_NOTES_TEMPLATE';
export const EDIT_NOTES_TEMPLATE = 'EDIT_NOTES_TEMPLATE';
export const DELETE_NOTES_TEMPLATE = 'DELETE_NOTES_TEMPLATE';

export const LOADING_LINK = 'LOADING_LINK';
export const GET_LINKS = 'GET_LINKS';
export const GET_LINK = 'GET_LINK';
export const ADD_LINK = 'ADD_LINK';
export const EDIT_LINK = 'EDIT_LINK';
export const DELETE_LINK = 'DELETE_LINK';

export const GET_ERRORS = 'GET_ERRORS';
export const CLEAR_ERRORS = 'CLEAR_ERRORS';
