import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_RELIEVER_LABEL,
  GET_RELIEVERS_LABELS,
  GET_RELIEVER_LABEL,
  ADD_RELIEVER_LABEL,
  EDIT_RELIEVER_LABEL,
  DELETE_RELIEVER_LABEL,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getRelieversLabels = () => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get('/api/label/relievers')
    .then(res =>
      dispatch({
        type: GET_RELIEVERS_LABELS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_RELIEVERS_LABELS,
        payload: []
      })
    );
};

export const getRelieverLabel = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/label/relievers/${id}`)
    .then(res =>
      dispatch({
        type: GET_RELIEVER_LABEL,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_RELIEVER_LABEL,
        payload: {}
      })
    );
};

export const addRelieverLabel = (labelData, history) => dispatch => {
  axios
    .post('/api/label/relievers', labelData)
    .then(res => {
      dispatch({
        type: ADD_RELIEVER_LABEL,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      if (history) {
        history.push('/labels/?tab=1');
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editRelieverLabel = (id, labelData, history) => dispatch => {
  axios
    .put(`/api/label/relievers/${id}`, labelData)
    .then(res => {
      dispatch({
        type: EDIT_RELIEVER_LABEL,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      history.push('/labels/?tab=1');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteRelieverLabel = id => dispatch => {
  axios
    .delete(`/api/label/relievers/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_RELIEVER_LABEL,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_RELIEVER_LABEL
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
