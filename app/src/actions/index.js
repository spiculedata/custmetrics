import * as authActions from './authActions.js';
import * as profileActions from './profileActions.js';
import * as companyActions from './companyActions';
import * as tagActions from './tagActions';
import * as painLabelActions from './painLabelActions';
import * as relieverLabelActions from './relieverLabelActions';
import * as gainLabelActions from './gainLabelActions';
import * as valuePropositionActions from './valuePropositionActions';
import * as notesActions from './notesActions';
import * as notesTemplateActions from './notesTemplateActions';
import * as linkActions from './linkActions';

export const actionCreators = Object.assign(
  {},
  authActions,
  profileActions,
  companyActions,
  tagActions,
  painLabelActions,
  relieverLabelActions,
  gainLabelActions,
  valuePropositionActions,
  notesActions,
  notesTemplateActions,
  linkActions
);
