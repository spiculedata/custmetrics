import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_PAIN_LABEL,
  GET_PAINS_LABELS,
  GET_PAIN_LABEL,
  ADD_PAIN_LABEL,
  EDIT_PAIN_LABEL,
  DELETE_PAIN_LABEL,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getPainsLabels = () => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get('/api/label/pains')
    .then(res =>
      dispatch({
        type: GET_PAINS_LABELS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PAINS_LABELS,
        payload: []
      })
    );
};

export const getPainLabel = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/label/pains/${id}`)
    .then(res =>
      dispatch({
        type: GET_PAIN_LABEL,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_PAIN_LABEL,
        payload: {}
      })
    );
};

export const addPainLabel = (labelData, history) => dispatch => {
  axios
    .post('/api/label/pains', labelData)
    .then(res => {
      dispatch({
        type: ADD_PAIN_LABEL,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      if (history) {
        history.push('/labels/?tab=0');
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editPainLabel = (id, labelData, history) => dispatch => {
  axios
    .put(`/api/label/pains/${id}`, labelData)
    .then(res => {
      dispatch({
        type: EDIT_PAIN_LABEL,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);

      history.push('/labels/?tab=0');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deletePainLabel = id => dispatch => {
  axios
    .delete(`/api/label/pains/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_PAIN_LABEL,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_PAIN_LABEL
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
