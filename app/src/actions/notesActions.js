import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_NOTES,
  GET_NOTES_ALL,
  GET_NOTES,
  ADD_NOTES,
  EDIT_NOTES,
  DELETE_NOTES,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getNotesAll = companyId => dispatch => {
  dispatch(setLoading());

  axios
    .get(`/api/notes/company/${companyId}`)
    .then(res =>
      dispatch({
        type: GET_NOTES_ALL,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_NOTES_ALL,
        payload: []
      })
    );
};

export const getNotes = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/notes/${id}`)
    .then(res =>
      dispatch({
        type: GET_NOTES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_NOTES,
        payload: {}
      })
    );
};

export const addNotes = notesData => dispatch => {
  axios
    .post('/api/notes', notesData)
    .then(res => {
      dispatch({
        type: ADD_NOTES,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editNotes = (notesId, notesData) => dispatch => {
  axios
    .put(`/api/notes/${notesId}`, notesData)
    .then(res => {
      dispatch({
        type: EDIT_NOTES,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteNotes = id => dispatch => {
  axios
    .delete(`/api/notes/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_NOTES,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_NOTES
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
