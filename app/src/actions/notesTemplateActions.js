import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_NOTES_TEMPLATE,
  GET_NOTES_TEMPLATES,
  GET_NOTES_TEMPLATE,
  ADD_NOTES_TEMPLATE,
  EDIT_NOTES_TEMPLATE,
  DELETE_NOTES_TEMPLATE,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getNotesTemplates = () => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get('/api/notestemplates')
    .then(res =>
      dispatch({
        type: GET_NOTES_TEMPLATES,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_NOTES_TEMPLATES,
        payload: []
      })
    );
};

export const getNotesTemplate = notesId => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/notestemplates/${notesId}`)
    .then(res =>
      dispatch({
        type: GET_NOTES_TEMPLATE,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_NOTES_TEMPLATE,
        payload: {}
      })
    );
};

export const addNotesTemplate = notesData => dispatch => {
  axios
    .post('/api/notestemplates', notesData)
    .then(res => {
      dispatch({
        type: ADD_NOTES_TEMPLATE,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editNotesTemplate = (notesId, notesData) => dispatch => {
  axios
    .put(`/api/notestemplates/${notesId}`, notesData)
    .then(res => {
      dispatch({
        type: EDIT_NOTES_TEMPLATE,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteNotesTemplate = id => dispatch => {
  axios
    .delete(`/api/notestemplates/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_NOTES_TEMPLATE,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_NOTES_TEMPLATE
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
