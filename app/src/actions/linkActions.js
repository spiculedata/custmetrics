import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';

import {
  LOADING_LINK,
  GET_LINKS,
  GET_LINK,
  ADD_LINK,
  EDIT_LINK,
  DELETE_LINK,
  GET_ERRORS,
  CLEAR_ERRORS
} from './types';

export const getLinks = companyId => dispatch => {
  dispatch(setLoading());

  axios
    .get(`/api/links/company/${companyId}`)
    .then(res =>
      dispatch({
        type: GET_LINKS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_LINKS,
        payload: []
      })
    );
};

export const getLink = id => dispatch => {
  dispatch(clearErrors());
  dispatch(setLoading());

  axios
    .get(`/api/links/${id}`)
    .then(res =>
      dispatch({
        type: GET_LINK,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_LINK,
        payload: {}
      })
    );
};

export const addLink = (linkData, history) => dispatch => {
  axios
    .post('/api/links', linkData)
    .then(res => {
      dispatch({
        type: ADD_LINK,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };
      const { companyId } = linkData;

      toaster.show(toasterConfig);

      if (history) {
        history.push(`/workspace/${companyId}`);
      }
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const editLink = (id, linkData, history) => dispatch => {
  axios
    .put(`/api/links/${id}`, linkData)
    .then(res => {
      dispatch({
        type: EDIT_LINK,
        payload: res.data
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'Saved!',
        timeout: 2000
      };
      const { companyId } = linkData;

      toaster.show(toasterConfig);

      history.push(`/workspace/${companyId}`);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const deleteLink = id => dispatch => {
  axios
    .delete(`/api/links/${id}`)
    .then(res => {
      dispatch({
        type: DELETE_LINK,
        payload: id
      });

      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_RIGHT
      });
      const toasterConfig = {
        icon: 'trash',
        intent: Intent.DANGER,
        message: 'Deleted!',
        timeout: 2000
      };

      toaster.show(toasterConfig);
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      })
    );
};

export const setLoading = () => {
  return {
    type: LOADING_LINK
  };
};

export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
