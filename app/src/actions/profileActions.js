import axios from 'axios';
import { Intent, Position, Toaster } from '@blueprintjs/core';
import setAuthToken from '../utils/setAuthToken';

import { SET_CURRENT_USER, GET_ERRORS, CLEAR_ERRORS } from './types';

export const editProfile = (userId, userEmail, userData) => dispatch => {
  axios
    .put(`/api/profile/${userId}/${userEmail}`, userData)
    .then(res => {
      dispatch(clearErrors());

      const toaster = Toaster.create({
        position: Position.TOP_CENTER
      });
      const toasterConfig = {
        icon: 'tick',
        intent: Intent.SUCCESS,
        message: 'User Updated! Making logout . . .',
        timeout: 3000
      };

      toaster.show(toasterConfig);

      // Log user out
      setTimeout(() => {
        // Remove token from localStorage
        localStorage.removeItem('jwtToken');

        // Remove auth header for future requests
        setAuthToken(false);

        // Set current user to {} which will set isAuthenticated to false
        dispatch(setCurrentUser({}));
      }, 3000);
    })
    .catch(err => {
      dispatch({
        type: GET_ERRORS,
        payload: err.response.data
      });
    });
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// Clear errors
export const clearErrors = () => {
  return {
    type: CLEAR_ERRORS
  };
};
