import {
  LOADING_RELIEVER_LABEL,
  GET_RELIEVERS_LABELS,
  GET_RELIEVER_LABEL,
  ADD_RELIEVER_LABEL,
  EDIT_RELIEVER_LABEL,
  DELETE_RELIEVER_LABEL
} from '../actions/types';

const initialState = {
  relieversLabels: [],
  relieverLabel: {},
  loading: false
};

export const relieverLabelReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_RELIEVER_LABEL:
      return {
        ...state,
        loading: true
      };
    case GET_RELIEVERS_LABELS:
      return {
        ...state,
        relieversLabels: action.payload,
        loading: false
      };
    case GET_RELIEVER_LABEL:
      return {
        ...state,
        relieverLabel: action.payload,
        loading: false
      };
    case ADD_RELIEVER_LABEL:
      return {
        ...state,
        relieversLabels: state.relieversLabels.concat(action.payload)
      };
    case EDIT_RELIEVER_LABEL:
      index = state.relieversLabels.findIndex(
        v => v._id === action.payload._id
      );

      return {
        ...state,
        relieversLabels: [
          ...state.relieversLabels.slice(0, index),
          action.payload,
          ...state.relieversLabels.slice(index + 1)
        ]
      };
    case DELETE_RELIEVER_LABEL:
      index = state.relieversLabels.findIndex(v => v._id === action.payload);

      return {
        ...state,
        relieversLabels: [
          ...state.relieversLabels.slice(0, index),
          ...state.relieversLabels.slice(index + 1)
        ]
      };
    default:
      return state;
  }
};
