import {
  LOADING_LINK,
  GET_LINKS,
  GET_LINK,
  ADD_LINK,
  EDIT_LINK,
  DELETE_LINK
} from '../actions/types';

const initialState = {
  links: [],
  link: {},
  loading: false
};

export const linkReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_LINK:
      return {
        ...state,
        loading: true
      };
    case GET_LINKS:
      return {
        ...state,
        links: action.payload,
        loading: false
      };
    case GET_LINK:
      return {
        ...state,
        link: action.payload,
        loading: false
      };
    case ADD_LINK:
      return {
        ...state,
        links: state.links.concat(action.payload)
      };
    case EDIT_LINK:
      index = state.links.findIndex(v => v._id === action.payload._id);

      return {
        ...state,
        links: [
          ...state.links.slice(0, index),
          action.payload,
          ...state.links.slice(index + 1)
        ]
      };
    case DELETE_LINK:
      index = state.links.findIndex(v => v._id === action.payload);

      return {
        ...state,
        links: [...state.links.slice(0, index), ...state.links.slice(index + 1)]
      };
    default:
      return state;
  }
};
