import {
  LOADING_NOTES_TEMPLATE,
  GET_NOTES_TEMPLATES,
  GET_NOTES_TEMPLATE,
  ADD_NOTES_TEMPLATE,
  EDIT_NOTES_TEMPLATE,
  DELETE_NOTES_TEMPLATE
} from '../actions/types';

const initialState = {
  notesTemplates: [],
  notesTemplate: {},
  loading: false
};

export const notesTemplatesReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_NOTES_TEMPLATE:
      return {
        ...state,
        loading: true
      };
    case GET_NOTES_TEMPLATES:
      return {
        ...state,
        notesTemplates: action.payload,
        loading: false
      };
    case GET_NOTES_TEMPLATE:
      return {
        ...state,
        notesTemplate: action.payload,
        loading: false
      };
    case ADD_NOTES_TEMPLATE:
      return {
        ...state,
        notesTemplates: state.notesTemplates.concat(action.payload)
      };
    case EDIT_NOTES_TEMPLATE:
      index = state.notesTemplates.findIndex(
        v => v.notesId === action.payload.notesId
      );

      return {
        ...state,
        notesTemplates: [
          ...state.notesTemplates.slice(0, index),
          action.payload,
          ...state.notesTemplates.slice(index + 1)
        ]
      };
    case DELETE_NOTES_TEMPLATE:
      index = state.notesTemplates.findIndex(v => v._id === action.payload);

      return {
        ...state,
        notesTemplates: [
          ...state.notesTemplates.slice(0, index),
          ...state.notesTemplates.slice(index + 1)
        ]
      };
    default:
      return state;
  }
};
