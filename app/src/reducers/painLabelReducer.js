import {
  LOADING_PAIN_LABEL,
  GET_PAINS_LABELS,
  GET_PAIN_LABEL,
  ADD_PAIN_LABEL,
  EDIT_PAIN_LABEL,
  DELETE_PAIN_LABEL
} from '../actions/types';

const initialState = {
  painsLabels: [],
  painLabel: {},
  loading: false
};

export const painLabelReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_PAIN_LABEL:
      return {
        ...state,
        loading: true
      };
    case GET_PAINS_LABELS:
      return {
        ...state,
        painsLabels: action.payload,
        loading: false
      };
    case GET_PAIN_LABEL:
      return {
        ...state,
        painLabel: action.payload,
        loading: false
      };
    case ADD_PAIN_LABEL:
      return {
        ...state,
        painsLabels: state.painsLabels.concat(action.payload)
      };
    case EDIT_PAIN_LABEL:
      index = state.painsLabels.findIndex(v => v._id === action.payload._id);

      return {
        ...state,
        painsLabels: [
          ...state.painsLabels.slice(0, index),
          action.payload,
          ...state.painsLabels.slice(index + 1)
        ]
      };
    case DELETE_PAIN_LABEL:
      index = state.painsLabels.findIndex(v => v._id === action.payload);

      return {
        ...state,
        painsLabels: [
          ...state.painsLabels.slice(0, index),
          ...state.painsLabels.slice(index + 1)
        ]
      };
    default:
      return state;
  }
};
