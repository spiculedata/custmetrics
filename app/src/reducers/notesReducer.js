import {
  LOADING_NOTES,
  GET_NOTES_ALL,
  GET_NOTES,
  ADD_NOTES,
  EDIT_NOTES,
  DELETE_NOTES
} from '../actions/types';

const initialState = {
  notesAll: [],
  notes: {},
  loading: false
};

export const notesReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_NOTES:
      return {
        ...state,
        loading: true
      };
    case GET_NOTES_ALL:
      return {
        ...state,
        notesAll: action.payload,
        loading: false
      };
    case GET_NOTES:
      return {
        ...state,
        notes: action.payload,
        loading: false
      };
    case ADD_NOTES:
      return {
        ...state,
        notesAll: state.notesAll.concat(action.payload)
      };
    case EDIT_NOTES:
      index = state.notesAll.findIndex(
        v => v.notesId === action.payload.notesId
      );

      return {
        ...state,
        notesAll: [
          ...state.notesAll.slice(0, index),
          action.payload,
          ...state.notesAll.slice(index + 1)
        ]
      };
    case DELETE_NOTES:
      index = state.notesAll.findIndex(v => v._id === action.payload);

      return {
        ...state,
        notesAll: [
          ...state.notesAll.slice(0, index),
          ...state.notesAll.slice(index + 1)
        ]
      };
    default:
      return state;
  }
};
