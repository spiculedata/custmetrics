import {
  LOADING_TAG,
  GET_TAGS,
  GET_TAG,
  ADD_TAG,
  EDIT_TAG,
  DELETE_TAG
} from '../actions/types';

const initialState = {
  tags: [],
  tag: {},
  loading: false
};

export const tagReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_TAG:
      return {
        ...state,
        loading: true
      };
    case GET_TAGS:
      return {
        ...state,
        tags: action.payload,
        loading: false
      };
    case GET_TAG:
      return {
        ...state,
        tag: action.payload,
        loading: false
      };
    case ADD_TAG:
      return {
        ...state,
        tags: state.tags.concat(action.payload)
      };
    case EDIT_TAG:
      index = state.tags.findIndex(v => v._id === action.payload._id);

      return {
        ...state,
        tags: [
          ...state.tags.slice(0, index),
          action.payload,
          ...state.tags.slice(index + 1)
        ]
      };
    case DELETE_TAG:
      index = state.tags.findIndex(v => v._id === action.payload);

      return {
        ...state,
        tags: [...state.tags.slice(0, index), ...state.tags.slice(index + 1)]
      };
    default:
      return state;
  }
};
