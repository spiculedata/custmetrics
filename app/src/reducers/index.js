import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

import { authReducer } from './authReducer';
import { companyReducer } from './companyReducer';
import { tagReducer } from './tagReducer';
import { painLabelReducer } from './painLabelReducer';
import { relieverLabelReducer } from './relieverLabelReducer';
import { gainLabelReducer } from './gainLabelReducer';
import { valuePropositionReducer } from './valuePropositionReducer';
import { notesReducer } from './notesReducer';
import { notesTemplatesReducer } from './notesTemplatesReducer';
import { linkReducer } from './linkReducer';
import { errorReducer } from './errorReducer';

export default combineReducers({
  auth: authReducer,
  companies: companyReducer,
  tags: tagReducer,
  painsLabels: painLabelReducer,
  relieversLabels: relieverLabelReducer,
  gainsLabels: gainLabelReducer,
  valuePropositions: valuePropositionReducer,
  notes: notesReducer,
  notesTemplates: notesTemplatesReducer,
  links: linkReducer,
  errors: errorReducer,
  form: formReducer
});
