import _ from 'lodash';

import {
  LOADING_USER,
  CHECK_USER,
  RESET_PASSWORD,
  SET_CURRENT_USER
} from '../actions/types';

const initialState = {
  isAuthenticated: false,
  user: {},
  loading: false
};

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_USER:
      return {
        ...state,
        loading: true
      };
    case CHECK_USER:
      return {
        ...state,
        user: action.payload,
        loading: false
      };
    case RESET_PASSWORD:
      return {
        ...state,
        user: action.payload,
        loading: false
      };
    case SET_CURRENT_USER:
      return {
        ...state,
        isAuthenticated: !_.isEmpty(action.payload),
        user: action.payload
      };
    default:
      return state;
  }
};
