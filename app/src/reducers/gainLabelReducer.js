import {
  LOADING_GAIN_LABEL,
  GET_GAINS_LABELS,
  GET_GAIN_LABEL,
  ADD_GAIN_LABEL,
  EDIT_GAIN_LABEL,
  DELETE_GAIN_LABEL
} from '../actions/types';

const initialState = {
  gainsLabels: [],
  gainLabel: {},
  loading: false
};

export const gainLabelReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_GAIN_LABEL:
      return {
        ...state,
        loading: true
      };
    case GET_GAINS_LABELS:
      return {
        ...state,
        gainsLabels: action.payload,
        loading: false
      };
    case GET_GAIN_LABEL:
      return {
        ...state,
        gainLabel: action.payload,
        loading: false
      };
    case ADD_GAIN_LABEL:
      return {
        ...state,
        gainsLabels: state.gainsLabels.concat(action.payload)
      };
    case EDIT_GAIN_LABEL:
      index = state.gainsLabels.findIndex(v => v._id === action.payload._id);

      return {
        ...state,
        gainsLabels: [
          ...state.gainsLabels.slice(0, index),
          action.payload,
          ...state.gainsLabels.slice(index + 1)
        ]
      };
    case DELETE_GAIN_LABEL:
      index = state.gainsLabels.findIndex(v => v._id === action.payload);

      return {
        ...state,
        gainsLabels: [
          ...state.gainsLabels.slice(0, index),
          ...state.gainsLabels.slice(index + 1)
        ]
      };
    default:
      return state;
  }
};
