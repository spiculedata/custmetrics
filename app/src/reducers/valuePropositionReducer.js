import {
  LOADING_VALUE_PROPOSITION,
  GET_VALUE_PROPOSITIONS,
  GET_VALUE_PROPOSITION,
  ADD_VALUE_PROPOSITION,
  EDIT_VALUE_PROPOSITION,
  DELETE_VALUE_PROPOSITION,
  CLEAR_VALUE_PROPOSITION
} from '../actions/types';

const initialState = {
  valuePropositions: [],
  valueProposition: {},
  loading: false
};

export const valuePropositionReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_VALUE_PROPOSITION:
      return {
        ...state,
        loading: true
      };
    case GET_VALUE_PROPOSITIONS:
      return {
        ...state,
        valuePropositions: action.payload,
        loading: false
      };
    case GET_VALUE_PROPOSITION:
      return {
        ...state,
        valueProposition: action.payload,
        loading: false
      };
    case ADD_VALUE_PROPOSITION:
      return {
        ...state,
        valuePropositions: state.valuePropositions.concat(action.payload)
      };
    case EDIT_VALUE_PROPOSITION:
      index = state.valuePropositions.findIndex(
        v => v.valuePropositionId === action.payload.valuePropositionId
      );

      return {
        ...state,
        valuePropositions: [
          ...state.valuePropositions.slice(0, index),
          action.payload,
          ...state.valuePropositions.slice(index + 1)
        ]
      };
    case DELETE_VALUE_PROPOSITION:
      index = state.valuePropositions.findIndex(v => v._id === action.payload);

      return {
        ...state,
        valuePropositions: [
          ...state.valuePropositions.slice(0, index),
          ...state.valuePropositions.slice(index + 1)
        ]
      };
    case CLEAR_VALUE_PROPOSITION:
      return {
        ...state,
        // valuePropositions: [],
        valueProposition: {}
      };
    default:
      return state;
  }
};
