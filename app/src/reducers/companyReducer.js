import {
  LOADING_COMPANY,
  GET_COMPANIES,
  GET_COMPANY,
  ADD_COMPANY,
  EDIT_COMPANY,
  DELETE_COMPANY
} from '../actions/types';

const initialState = {
  companies: [],
  company: {},
  loading: false
};

export const companyReducer = (state = initialState, action) => {
  let index = null;

  switch (action.type) {
    case LOADING_COMPANY:
      return {
        ...state,
        loading: true
      };
    case GET_COMPANIES:
      return {
        ...state,
        companies: action.payload,
        loading: false
      };
    case GET_COMPANY:
      return {
        ...state,
        company: action.payload,
        loading: false
      };
    case ADD_COMPANY:
      return {
        ...state,
        companies: state.companies.concat(action.payload)
      };
    case EDIT_COMPANY:
      index = state.companies.findIndex(v => v._id === action.payload._id);

      return {
        ...state,
        companies: [
          ...state.companies.slice(0, index),
          action.payload,
          ...state.companies.slice(index + 1)
        ]
      };
    case DELETE_COMPANY:
      index = state.companies.findIndex(v => v._id === action.payload);

      return {
        ...state,
        companies: [
          ...state.companies.slice(0, index),
          ...state.companies.slice(index + 1)
        ]
      };
    default:
      return state;
  }
};
