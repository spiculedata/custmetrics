const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const passport = require('passport');
const path = require('path');
const dotenv = require('dotenv');

// Init env
dotenv.config();

const users = require('./routes/api/users');
const companies = require('./routes/api/companies');
const tags = require('./routes/api/tags');
const painLabels = require('./routes/api/painLabels');
const relieverLabels = require('./routes/api/relieverLabels');
const gainLabels = require('./routes/api/gainLabels');
const notes = require('./routes/api/notes');
const notesTemplates = require('./routes/api/notesTemplates');
const links = require('./routes/api/links');
const valuePropositions = require('./routes/api/valuePropositions');
const profile = require('./routes/api/profile');

const app = express();

// Middleware
app.use(cors());
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// DB config
const db = require('./config/keys').mongoURI;

// Connect to MongoDB
mongoose
  .connect(db, { useNewUrlParser: true })
  .then(() => console.log('MongoDB Connected'))
  .catch(err => console.log(err));

// Passport middleware
app.use(passport.initialize());

// Passport config
require('./config/passport')(passport);

// Use routes
app.use('/api/users', users);
app.use('/api/companies', companies);
app.use('/api/tags', tags);
app.use('/api/label/pains', painLabels);
app.use('/api/label/relievers', relieverLabels);
app.use('/api/label/gains', gainLabels);
app.use('/api/notes', notes);
app.use('/api/notestemplates', notesTemplates);
app.use('/api/links', links);
app.use('/api/valuepropositions', valuePropositions);
app.use('/api/profile', profile);

// Server static assets if in production
if (process.env.NODE_ENV === 'production') {
  // Set static folder
  app.use(express.static('app/build'));

  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'app', 'build', 'index.html'));
  });
}

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`Server running on port ${port}`));
