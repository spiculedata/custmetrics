const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ValuePropositionSchema = new Schema({
  companyId: {
    type: Schema.Types.ObjectId,
    ref: 'companies'
  },
  valuePropositionId: {
    type: String
  },
  tagId: {
    type: Schema.Types.ObjectId,
    ref: 'tags'
  },
  title: {
    type: String,
    required: true
  },
  type: {
    type: String,
    default: 'vp'
  },
  wantsItems: {
    type: Array
  },
  wantsList: {
    type: [Schema.Types.Mixed]
  },
  wantsMindmap: {
    type: Array
  },
  wantsChart: {
    type: {}
  },
  needsItems: {
    type: Array
  },
  needsList: {
    type: [Schema.Types.Mixed]
  },
  needsMindmap: {
    type: Array
  },
  needsChart: {
    type: {}
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = ValueProposition = mongoose.model('valuePropositions', ValuePropositionSchema);
