const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const GainLabelSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  value: {
    type: String
  },
  label: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = GainLabel = mongoose.model('gainLabels', GainLabelSchema);
