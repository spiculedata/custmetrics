const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ResetPasswordSchema = new Schema({
  email: {
    type: String,
    required: true
  },
  token: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = ResetPassword = mongoose.model('resetPasswords', ResetPasswordSchema);
