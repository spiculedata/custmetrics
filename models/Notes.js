const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const NotesSchema = new Schema({
  companyId: {
    type: Schema.Types.ObjectId,
    ref: 'companies'
  },
  notesId: {
    type: String
  },
  tagId: {
    type: Schema.Types.ObjectId,
    ref: 'tags'
  },
  title: {
    type: String,
    required: true
  },
  template: {
    type: Boolean,
    default: false
  },
  type: {
    type: String,
    default: 'notes'
  },
  treeData: {
    type: [Schema.Types.Mixed]
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Notes = mongoose.model('notes', NotesSchema);
