const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PainLabelSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  value: {
    type: String
  },
  label: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = PainLabel = mongoose.model('painLabels', PainLabelSchema);
