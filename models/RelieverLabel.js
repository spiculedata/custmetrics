const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RelieverLabelSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  value: {
    type: String
  },
  label: {
    type: String
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = RelieverLabel = mongoose.model(
  'relieverLabels',
  RelieverLabelSchema
);
