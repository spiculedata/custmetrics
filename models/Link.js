const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LinkSchema = new Schema({
  companyId: {
    type: Schema.Types.ObjectId,
    ref: 'companies'
  },
  tagId: {
    type: String
  },
  title: {
    type: String,
    required: true
  },
  url: {
    type: String
  },
  type: {
    type: String,
    default: 'link'
  },
  date: {
    type: Date,
    default: Date.now
  }
});

module.exports = Link = mongoose.model('links', LinkSchema);
